require('dotenv').config();
require('./Auth/passport');
const express = require('express');
const passport = require('passport');
const cron = require('node-cron');
const session = require('express-session');
const userRoutes = require('./Routes/UserRoutes');
const healthCheckRoute = require('./Routes/HealthCheckRoute');
const alertRoutes = require('./Routes/AlertRoutes');
const eventRoutes = require('./Routes/EventRoutes');
const clubRoutes = require('./Routes/ClubRoutes');
const announcementRoutes = require('./Routes/AnnouncementRoutes');
const ticketRoutes = require('./Routes/TicketRoutes');
const imageRoutes = require('./Routes/ImageRoutes');
const slackRoutes = require('./Routes/SlackRoutes');
const socialRoutes = require('./Routes/SocialRoutes');
const authRoute = require('./Routes/AuthRoutes');
const config = require('./Config/config');
const database = require('./Database/Database');
const { EmailService } = require('./Services/EmailService');
const { FakeEmailService } = require('./Services/FakeEmailService');
const { scheduledJobs } = require('./Services/scheduledJobs');
const { discordLogin } = require('./Services/Discord/botLogin');

// Ensures that ReadableStream exists to prevent server crash
if (typeof global.ReadableStream === 'undefined') {
  // eslint-disable-next-line global-require
  global.ReadableStream = require('node:stream/web').ReadableStream;
}

const app = express();

// Express's session API requires defining
// using express-session before using passport.session()
app.use(session({
  secret: config.jwtSecret,
  resave: true,
  saveUninitialized: false,
}));
app.use(passport.initialize());
app.use(passport.session());

app.use(
  express.urlencoded({
    extended: true,
  }),
);
app.use(express.json());

app.use('/api/users', userRoutes);
app.use('/api/alerts', alertRoutes);
app.use('/api/events', eventRoutes);
app.use('/api/clubs', clubRoutes);
app.use('/api/announcements', announcementRoutes);
app.use('/api/tickets', ticketRoutes);
app.use('/api/images', imageRoutes);
app.use('/api/slack', slackRoutes);
app.use('/api/socials', socialRoutes);
app.use('/auth', authRoute);
app.use('/', healthCheckRoute);

database.connect();

global.emailService = config.email
  ? new EmailService()
  : new FakeEmailService();

if (config.discord) discordLogin();

const server = app.listen(config.port, () => {
  console.log(`Now listening on port ${config.port}`);
});

// Any jobs that need to be run daily (at midnight) go here
cron.schedule('0 0 0 * * *', () => {
  if (process.env.NODE_ENV === 'production') {
    scheduledJobs();
  }
});

function stop() {
  console.log('stopping');
  server.close();
  database.disconnect();
  process.exit(); // Make sure the app doesn't hang
}

module.exports = app;
module.exports.stop = stop;
