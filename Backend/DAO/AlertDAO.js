const Alert = require('../Model/Alert').Model;
const { NotFoundError } = require('../util/errors/errors');

exports.create = async (alertParams) => await new Alert(alertParams).save();

exports.getActiveAlerts = async () => {
  const currentDate = new Date();
  return await Alert.find({ dateToExpire: { $gt: currentDate } });
};

exports.get = async (id) => {
  const alert = await Alert.findById(id);
  if (!alert) throw NotFoundError('Alert not found');
  return alert;
};

exports.update = async (id, updatedData) => {
  const updatedAlert = await Alert.findByIdAndUpdate(id, updatedData, {
    new: true,
    useFindAndModify: false,
  });
  if (!updatedAlert) throw NotFoundError('Alert not found');
  return updatedAlert;
};

exports.delete = async (id) => {
  const alert = await Alert.findByIdAndDelete(id);
  if (!alert) throw NotFoundError('Alert not found');
  return alert;
};
