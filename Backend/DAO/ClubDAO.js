const Club = require('../Model/Club').Model;
const Announcement = require('../Model/Announcement').Model;
const Event = require('../Model/Event').Model;
const User = require('../Model/User').Model;
const Listener = require('../Model/Listener').Model;
const { NotFoundError, ConflictError } = require('../util/errors/errors');
const { limitedUserModelFields } = require('../util/userUtil');
const userDAO = require('./UserDAO');

exports.getAll = async () => await Club.find({})
  .populate({
    path: 'admins',
    model: 'User',
    select: limitedUserModelFields,
  })
  .populate({
    path: 'socialLinks.social',
    model: 'Social',
  })
  .exec();

exports.getPage = async (page, pageSize, filters) => {
  const skip = (page - 1) * pageSize;

  const query = filters || {};

  // Executes two queries in parallel (1 - get the "page" of clubs / 2 - get count of all "matches")
  const [clubs, totalClubs] = await Promise.all([
    Club.find(query)
      .sort({ name: 1 })
      .skip(skip)
      .limit(pageSize),
    Club.countDocuments(query),
  ]);

  // Calculate whether there are more pages (TRUE -> show "next"/"load more" buttons, etc...)
  const hasMore = skip + clubs.length < totalClubs;

  return {
    clubs,
    page,
    pageSize,
    totalClubs,
    hasMore,
  };
};

exports.get = async (id) => {
  const club = await Club.findById(id)
    .populate({
      path: 'admins',
      model: 'User',
      select: limitedUserModelFields,
    })
    .populate({
      path: 'socialLinks.social',
      model: 'Social',
    })
    .exec();

  if (!club) throw NotFoundError('Club not found');

  return club;
};

exports.getByName = async (clubName) => {
  // Fetch club by name (name is a unique identifier for a club)
  const club = await Club.findOne({
    name: clubName,
  });

  if (!club) throw NotFoundError('Club name not found');

  return club;
};

exports.getByAdminId = async (userId) => Club.find({ admins: userId })
  .populate({
    path: 'admins',
    model: 'User',
    select: limitedUserModelFields,
  })
  .populate({
    path: 'socialLinks.social',
    model: 'Social',
  });

exports.isAdmin = async (userId, clubId) => {
  const clubs = await Club.find({ _id: clubId, admins: userId });
  return clubs.length === 1;
};

exports.exists = async (id) => {
  try {
    return await Club.exists({ _id: id });
  } catch (error) {
    return false;
  }
};

exports.nameExists = async (name) => {
  try {
    return await Club.exists({ name });
  } catch (error) {
    return false;
  }
};

exports.create = async (clubParams) => {
  if (await Club.exists({ name: clubParams.name })) {
    throw ConflictError('Club name already exists');
  }

  const newClub = await new Club(clubParams).save();

  await Promise.all(clubParams.admins.map(async (admin) => {
    await userDAO.update(admin, { $addToSet: { clubs: newClub._id } });
  }));

  return newClub;
};

exports.update = async (id, updateData) => {
  await Club.findOneAndUpdate({ _id: id }, updateData, {
    useFindAndModify: false,
  });
  return exports.get(id);
};

exports.delete = async (id) => {
  try {
    const club = await Club.findById(id);
    if (!club) {
      throw new Error('Club not found');
    }

    await Announcement.deleteMany({ club: id });
    await Event.deleteMany({ club: id });

    await User.updateMany(
      { clubs: id },
      { $pull: { clubs: id } },
    );
    await Listener.deleteMany({ club: id });

    const deletedClub = await Club.findByIdAndRemove(id);

    return deletedClub;
  } catch (error) {
    throw new Error(`Failed to delete club: ${error.message}`);
  }
};

exports.deleteAll = async () => {
  await Club.deleteMany();
};

exports.search = async (input) => {
  if (input && input.length > 0) {
    // Simple search for matching club names (case insenstitive)
    const matches = await Club.find({ name: { $regex: input, $options: 'i' } })
      .populate({
        path: 'admins',
        model: 'User',
        select: limitedUserModelFields,
      })
      .populate({
        path: 'socialLinks.social',
        model: 'Social',
      })
      .limit(10);

    return matches;
  }

  // Returns a list of "default" clubs if no query input is given
  const defaults = await Club.find({})
    .populate({
      path: 'admins',
      model: 'User',
      select: limitedUserModelFields,
    })
    .populate({
      path: 'socialLinks.social',
      model: 'Social',
    })
    .limit(10);

  return defaults;
};
