const Social = require('../Model/Social').Model;
const { NotFoundError } = require('../util/errors/validationError');

exports.get = async (id) => {
  const socialLink = await Social.findById(id);
  if (!socialLink) throw new NotFoundError('Social not found.');

  return socialLink;
};

exports.getAll = async () => {
  const socials = await Social.find({});

  if (!socials) throw new NotFoundError('No socials found.');

  return socials;
};
