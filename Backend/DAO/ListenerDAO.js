const Listener = require('../Model/Listener').Model;
const { NotFoundError } = require('../util/errors/errors');
const clubDAO = require('./ClubDAO');

// Threshold used to track any listeners "left for dead" (server deleted, channel deleted, etc...)
// If backend fails to send info to a listener -> failures get incremented (3 in a row -> deleted)
const failureThreshold = 3;

exports.getClubListeners = async (type, clubId) => {
  const listeners = await Listener.find({
    type,
    club: clubId,
  });

  if (!listeners) throw NotFoundError('Listeners not found');

  return listeners;
};

exports.getServerListeners = async (type, platformId) => {
  // Fetch all listners for the given server id (used to manage listeners within integrations)
  const listeners = await Listener.find({
    type,
    platformId,
  }).populate('club');

  if (!listeners) return [];

  // Sort listeners so that those of the same club are grouped together
  return listeners.sort((a, b) => a.club.name.localeCompare(b.club.name, undefined, { sensitivity: 'base' }));
};

exports.addListener = async (clubName, type, platformId, channelId, roles) => {
  // Check whether given club name exists
  const club = await clubDAO.getByName(clubName);
  if (!club) {
    throw NotFoundError(`Club '${clubName}' not found`);
  }

  // Check if a listener with the same platformId and channelId already exists
  const existingListener = await Listener.findOne({
    type, club: club._id, platformId, channelId,
  });

  // Listener with the same platformId and channelId already exists
  if (existingListener) {
    existingListener.roles = roles;
    await existingListener.save(); // Save the changes to the existing listener

    return { listener: existingListener, isNew: false };
  }

  const newListener = new Listener({
    type, club: club._id, platformId, channelId, roles,
  });
  await newListener.save(); // Save the new listener to the database

  return { listener: newListener, isNew: true };
};

exports.removeListener = async (clubName, type, platformId, channelId) => {
  // Check whether given club name exists
  const club = await clubDAO.getByName(clubName);
  if (!club) {
    throw NotFoundError(`Club '${clubName}' not found`);
  }

  // Find the listener to remove by matching platformId and channelId
  const listenerToRemove = await Listener.findOneAndDelete({
    club: club._id,
    type,
    platformId,
    channelId,
  });

  if (!listenerToRemove) {
    throw NotFoundError('Listener to be removed not found');
  }

  return listenerToRemove;
};

exports.updateListenerFailures = async (listenerId, failureCount) => {
  // Function used to update a listener "failures" to dispatch
  // 3+ failures in a row -> listener is deleted (prune old listeners)
  // Could arrive due to server deleted, channel deleted, etc..
  try {
    if (failureCount >= failureThreshold) {
      await Listener.findByIdAndDelete(listenerId);
    } else {
      await Listener.findByIdAndUpdate({ _id: listenerId }, { failures: failureCount });
    }
  } catch (error) {
    console.error(`Error updating (failures) or deleting listener with id ${listenerId}: ${error}`);
  }
};

exports.getListenerById = async (listenerId) => {
  const listener = await Listener.findById(listenerId);

  if (!listener) {
    throw new Error(`Listener not found for id '${listenerId}'`);
  }

  return listener;
};

exports.removeListenerById = async (listenerId) => {
  const listenerToRemove = await Listener.findByIdAndDelete(listenerId);

  if (!listenerToRemove) {
    throw new Error(`Listener not found for id '${listenerId}'`);
  }

  return listenerToRemove; // Return the removed listener
};
