const IntegrationToken = require('../Model/IntegrationToken').Model;

exports.getIntegrationToken = async (platformId) => {
  const intToken = await IntegrationToken.findOne({
    platformId,
  });

  if (!intToken) throw Error('No token found for integration');

  return intToken.token;
};

exports.addIntegrationToken = async (platformId, token, type) => {
  const existingToken = await IntegrationToken.findOne({
    platformId,
  });

  if (existingToken) {
    existingToken.token = token;
    return existingToken.save();
  }

  const newToken = new IntegrationToken({
    platformId,
    token,
    type,
  });
  return newToken.save();
};
