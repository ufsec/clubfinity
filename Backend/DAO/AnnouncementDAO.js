const Announcement = require('../Model/Announcement').Model;
const { NotFoundError } = require('../util/errors/errors');
const clubDAO = require('./ClubDAO');

exports.get = async (id) => {
  const announcement = await Announcement.findById(id);

  if (!announcement) throw NotFoundError('Accouncement not found');

  return announcement;
};

exports.getByClubs = async (clubIds, page, pageSize) => {
  // Base Query
  const query = Announcement.find({ club: { $in: clubIds } })
    .sort({ date: -1 })
    .populate('club');

  // If page and pageSize given, add skip and limit query
  if (page && pageSize) {
    // Skip means which documents to skip in query
    const skip = (page - 1) * pageSize;
    query.limit(pageSize).skip(skip);
  }

  // Return announcements and total number of announcements
  const [announcements, totalAnnouncements] = await Promise.all([
    query.exec(),
    // Count number of total announcements in these club(s) to prevent
    // extra API calls when no more pages to query
    Announcement.countDocuments({ club: { $in: clubIds } }),
  ]);

  // Return object containing announcements and totalAnnouncements
  return { announcements, totalAnnouncements };
};

exports.create = async (params) => {
  if (!(await clubDAO.exists(params.club))) {
    throw NotFoundError('Club not found');
  }

  const updatedParams = params;
  updatedParams.club = (await clubDAO.get(updatedParams.club))._id;

  return new Announcement(updatedParams).save();
};

exports.update = async (id, params) => {
  const { title, description } = params;
  await Announcement.findOneAndUpdate({ _id: id }, { title, description }, {
    useFindAndModify: false,
  });

  return exports.get(id);
};

exports.delete = async (id) => {
  const announcement = await Announcement.findByIdAndDelete(id);
  if (!announcement) throw NotFoundError('Annoucement not found');

  return announcement;
};

exports.deleteAll = async () => {
  await Announcement.deleteMany();
};

exports.exists = async (id) => {
  try {
    return await Announcement.exists({ _id: id });
  } catch (error) {
    return false;
  }
};
