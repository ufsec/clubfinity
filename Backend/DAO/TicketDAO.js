const Ticket = require('../Model/Ticket').Model;
const { NotFoundError } = require('../util/errors/errors');

exports.get = async (id) => {
  const ticket = await Ticket.findById(id);
  if (!ticket) throw NotFoundError('Ticket not found');

  return ticket;
};

exports.delete = async (id) => {
  const ticket = await Ticket.findByIdAndDelete(id);
  if (!ticket) throw NotFoundError('Ticket not found');

  return ticket;
};

exports.create = async (params) => new Ticket(params).save();
