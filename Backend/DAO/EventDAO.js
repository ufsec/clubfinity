const Event = require('../Model/Event').Model;
const clubDAO = require('./ClubDAO');
const { NotFoundError } = require('../util/errors/errors');
const { limitedUserModelFields } = require('../util/userUtil');

const clubPopulateConfig = {
  path: 'club',
  model: 'Club',
  populate: [
    {
      path: 'admins',
      model: 'User',
      select: limitedUserModelFields,
    },
    {
      path: 'socialLinks.social',
      model: 'Social',
    },
  ],
};

const filterInMonth = (searchDate, events) => events.filter(
  (e) => e.date.month === searchDate.month && e.date.year === searchDate.year,
);

exports.create = async (eventParams) => {
  if (!(await clubDAO.exists(eventParams.club))) {
    throw NotFoundError('Club not found');
  }
  const updatedEventParams = eventParams;
  updatedEventParams.club = (await clubDAO.get(eventParams.club))._id;

  return await new Event(updatedEventParams).save();
};

exports.getAll = async () => await Event.find({}).populate(clubPopulateConfig).exec();

exports.getAllEventsInMonth = async (date) => {
  const allEvents = await exports.getAll();
  return filterInMonth(date, allEvents);
};

exports.getEventsFromFollowedClubsInMonth = async (date, user) => {
  if (user.clubs.length === 0) return [];

  const { events: followingEvents } = await exports.getByClubs(user.clubs);

  return filterInMonth(date, followingEvents);
};

exports.getGoingEventsInMonth = async (date, user) => {
  const goingEvents = await exports.getEventsUserIsGoingTo(user._id);
  return filterInMonth(date, goingEvents);
};

exports.get = async (id) => {
  const event = await Event.findById(id).populate(clubPopulateConfig);
  if (!event) throw NotFoundError('Event not found');

  return event;
};

exports.getByName = async (name) => {
  const event = await Event.findOne({ name });
  if (!event) throw NotFoundError('Event name not found');

  return event;
};

exports.getByClubs = async (clubs, page, pageSize, eventType) => {
  // Dates sorted by earliest date by default (1)
  // If true, sort events by latest date (-1)
  let invertSort = false;

  // "now" is current date
  const now = new Date();

  // Filter events to be ones in list of clubs
  const filterConditions = { club: { $in: clubs } };

  // Change which events to get based on type
  switch (eventType) {
    // If past, get events that occured before "now"
    case 'past':
      filterConditions.date = { $lt: now };
      invertSort = true;
      break;
    // If upcoming, get events that will occur after now
    case 'upcoming':
      filterConditions.date = { $gte: now };
      break;
    // Default case only for linting
    default:
      break;
  }

  // Base query
  const query = Event.find(filterConditions)
    .sort({ date: invertSort ? -1 : 1 })
    .populate(clubPopulateConfig);

  // If page and pageSize given, add skip and limit query
  if (page && pageSize) {
    const skip = (page - 1) * pageSize;
    query.limit(pageSize).skip(skip);
  }

  // Return events and total number of events
  const [events, totalEvents] = await Promise.all([
    query.exec(),
    // Count number of total announcement in these club(s) to prevent
    // extra API calls when no more pages to query
    Event.countDocuments(filterConditions),
  ]);

  // Return object containing announcements and totalAnnouncements
  return { events, totalEvents };
};

exports.exists = async (id) => {
  try {
    return await Event.exists({ _id: id });
  } catch (error) {
    return false;
  }
};

exports.getEventsUserIsGoingTo = async (userId) => Event.find({ goingUsers: userId })
  .populate(clubPopulateConfig)
  .exec();

exports.update = async (id, updatedData) => {
  await Event.findOneAndUpdate({ _id: id }, updatedData, {
    useFindAndModify: false,
  });
  return exports.get(id);
};

exports.delete = async (id) => {
  const event = await Event.findByIdAndDelete(id);
  if (!event) throw NotFoundError('Event not found');

  return event;
};

exports.deleteAll = async () => {
  await Event.deleteMany();
};

exports.search = async (input) => {
  if (input && input.length > 0) {
    // Simple search for matching event names (case insenstitive)
    const matches = await Event.find({ name: { $regex: input, $options: 'i' } })
      .populate(clubPopulateConfig)
      .limit(10);

    return matches;
  }

  // Returns a list of "default" events if no query input is given
  const defaults = await Event.find({})
    .populate(clubPopulateConfig)
    .limit(10);

  return defaults;
};
