import { DateTime } from 'luxon';
import { Types } from 'mongoose';

interface Event {
  name: String;
  location: String;
  majorOfInterest: String;
  facebookLink?: String;
  description: String;
  date: DateTime;
  endTime: DateTime;
  goingUsers: Types.ObjectId[];
  interestedUsers: Types.ObjectId[];
  uninterestedUsers: Types.ObjectId[];
  club: Types.ObjectId;
  _id: Types.ObjectId;
}

const eventsData: Event[] = [
  {
    name: 'CodeForChange',
    location: 'CSE 23A',
    majorOfInterest: 'Computer Science',
    facebookLink: 'https://facebook.com/facebook',
    description: 'Join us for mini hackathon',
    date: DateTime.local(2023, 4, 5, 13),
    endTime: DateTime.local(2023, 4, 5, 14),
    goingUsers: [new Types.ObjectId('16cb91bdc3464f14678934ca'), new Types.ObjectId('36cb91bdc3464f14678934ca'), new Types.ObjectId('86cb91bdc3464f14678934ca'), new Types.ObjectId('07cb91bdc3464f14678934ca')],
    uninterestedUsers: [new Types.ObjectId('46cb91bdc3464f14678934ca'), new Types.ObjectId('56cb91bdc3464f14678934ca'), new Types.ObjectId('76cb91bdc3464f14678934ca'), new Types.ObjectId('17cb91bdc3464f14678934ca')],
    interestedUsers: [new Types.ObjectId('26cb91bdc3464f14678934ca'), new Types.ObjectId('66cb91bdc3464f14678934ca'), new Types.ObjectId('96cb91bdc3464f14678934ca')],
    club: new Types.ObjectId('99cb91bdc3464f14678934ca'),
    _id: new Types.ObjectId('99cb91bdc3464f14678934ff'),
  },
  {
    name: 'Hash Code',
    location: 'Reitz Union Room 43',
    majorOfInterest: 'Computer Science',
    facebookLink: 'https://facebook.com/facebook',
    description: 'We will be hosting google hashcode competition',
    date: DateTime.local(2021, 4, 10, 14),
    endTime: DateTime.local(2021, 4, 10, 15),
    goingUsers: [new Types.ObjectId('16cb91bdc3464f14678934ca'), new Types.ObjectId('56cb91bdc3464f14678934ca'), new Types.ObjectId('96cb91bdc3464f14678934ca')],
    uninterestedUsers: [new Types.ObjectId('26cb91bdc3464f14678934ca'), new Types.ObjectId('36cb91bdc3464f14678934ca'), new Types.ObjectId('86cb91bdc3464f14678934ca'), new Types.ObjectId('07cb91bdc3464f14678934ca')],
    interestedUsers: [new Types.ObjectId('46cb91bdc3464f14678934ca'), new Types.ObjectId('66cb91bdc3464f14678934ca'), new Types.ObjectId('76cb91bdc3464f14678934ca'), new Types.ObjectId('17cb91bdc3464f14678934ca')],
    club: new Types.ObjectId('99cb91bdc3464f14678934ca'),
    _id: new Types.ObjectId(),
  },
  {
    name: 'CodeShop',
    location: 'LIT 101',
    majorOfInterest: 'Computer Science',
    facebookLink: 'https://facebook.com/facebook',
    description: 'Join us for mini hackathon',
    date: DateTime.local(2021, 4, 12, 15),
    endTime: DateTime.local(2021, 4, 12, 16),
    goingUsers: [new Types.ObjectId('16cb91bdc3464f14678934ca'), new Types.ObjectId('46cb91bdc3464f14678934ca'), new Types.ObjectId('76cb91bdc3464f14678934ca'), new Types.ObjectId('17cb91bdc3464f14678934ca')],
    uninterestedUsers: [new Types.ObjectId('26cb91bdc3464f14678934ca'), new Types.ObjectId('66cb91bdc3464f14678934ca'), new Types.ObjectId('96cb91bdc3464f14678934ca')],
    interestedUsers: [new Types.ObjectId('36cb91bdc3464f14678934ca'), new Types.ObjectId('56cb91bdc3464f14678934ca'), new Types.ObjectId('86cb91bdc3464f14678934ca'), new Types.ObjectId('07cb91bdc3464f14678934ca')],
    club: new Types.ObjectId('99cb91bdc3464f14678934ca'),
    _id: new Types.ObjectId(),
  },
  {
    name: 'Puppy Zoo',
    location: 'Plaza of Americas',
    majorOfInterest: 'Fun',
    description: "We'll have various puppers to cuddle with! Join us :)",
    date: DateTime.local(2021, 4, 5, 12),
    endTime: DateTime.local(2021, 4, 5, 13),
    goingUsers: [new Types.ObjectId('36cb91bdc3464f14678934ca'), new Types.ObjectId('56cb91bdc3464f14678934ca'), new Types.ObjectId('86cb91bdc3464f14678934ca'), new Types.ObjectId('07cb91bdc3464f14678934ca')],
    uninterestedUsers: [new Types.ObjectId('16cb91bdc3464f14678934ca'), new Types.ObjectId('46cb91bdc3464f14678934ca'), new Types.ObjectId('76cb91bdc3464f14678934ca'), new Types.ObjectId('17cb91bdc3464f14678934ca')],
    interestedUsers: [new Types.ObjectId('26cb91bdc3464f14678934ca'), new Types.ObjectId('66cb91bdc3464f14678934ca'), new Types.ObjectId('96cb91bdc3464f14678934ca')],
    club: new Types.ObjectId('99cd91bdc3464f14678934ca'),
    _id: new Types.ObjectId(),
  },
  {
    name: 'Potluck Wednesday',
    location: 'MAT 002',
    majorOfInterest: 'Fun',
    description: 'Join us for an evening of good pasta.',
    date: DateTime.local(2021, 4, 8, 18),
    endTime: DateTime.local(2021, 4, 8, 19),
    goingUsers: [new Types.ObjectId('26cb91bdc3464f14678934ca'), new Types.ObjectId('66cb91bdc3464f14678934ca'), new Types.ObjectId('96cb91bdc3464f14678934ca')],
    uninterestedUsers: [new Types.ObjectId('36cb91bdc3464f14678934ca'), new Types.ObjectId('56cb91bdc3464f14678934ca'), new Types.ObjectId('86cb91bdc3464f14678934ca'), new Types.ObjectId('07cb91bdc3464f14678934ca')],
    interestedUsers: [new Types.ObjectId('16cb91bdc3464f14678934ca'), new Types.ObjectId('46cb91bdc3464f14678934ca'), new Types.ObjectId('76cb91bdc3464f14678934ca'), new Types.ObjectId('17cb91bdc3464f14678934ca')],
    club: new Types.ObjectId('99ce91bdc3464f14678934ca'),
    _id: new Types.ObjectId(),
  },
];

export = eventsData;
