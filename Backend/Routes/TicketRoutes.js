const passport = require('passport');
const express = require('express');
const ticketController = require('../Controllers/TicketController');

const router = express.Router();

router.post(
  '/',
  passport.authenticate('loggedIn', { session: false }),
  ticketController.validate('validateTicketInfo'),
  ticketController.create,
);

module.exports = router;
