const passport = require('passport');
const express = require('express');
const config = require('../Config/config');
const alertController = require('../Controllers/AlertController');

const router = express.Router();

const validateApiKeyOrAuth = (requireKey = false) => (req, res, next) => {
  const apiKey = req.get('Authorization');
  const isValidApiKey = apiKey && apiKey === config.alertAPIkey;

  if (isValidApiKey) {
    return next();
  } if (requireKey) {
    return res.status(401).json({ error: 'Invalid API Key' });
  }

  passport.authenticate('loggedIn', { session: false }, (error, user) => {
    if (error || !user) {
      return res.status(401).json({ error: 'Unauthorized' });
    }
    return next();
  })(req, res, next);

  return null;
};

router.get('/', validateApiKeyOrAuth(), alertController.getActive);
router.get('/:id', validateApiKeyOrAuth(), alertController.get);
router.post('/', validateApiKeyOrAuth(true), alertController.validate('validateAlertInfo'), alertController.create);
router.put('/:id', validateApiKeyOrAuth(true), alertController.validate('validateAlertInfo'), alertController.update);
router.delete('/:id', validateApiKeyOrAuth(true), alertController.delete);

module.exports = router;
