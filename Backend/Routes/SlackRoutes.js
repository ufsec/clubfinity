const express = require('express');
const SlackController = require('../Controllers/SlackController');

const router = express.Router();

router.post('/', SlackController.slack); // General endpoint for requests
router.post('/select', SlackController.select); // Endpoint for Select Menus
router.get('/oauth', SlackController.oauth); // Redirect endpoint for OAuth

module.exports = router;
