const express = require('express');

const router = express.Router();
const LoginStrategy = require('../Auth/login');

router.post('/login', LoginStrategy.authenticate);

module.exports = router;
