const passport = require('passport');
const express = require('express');
const socialController = require('../Controllers/SocialController');

const router = express.Router();

router.get(
  '/',
  passport.authenticate('loggedIn', { session: false }),
  socialController.getAll,
);
router.get(
  '/:id',
  passport.authenticate('loggedIn', { session: false }),
  socialController.get,
);

module.exports = router;
