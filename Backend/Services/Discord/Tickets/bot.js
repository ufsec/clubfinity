const {
  Client, IntentsBitField,
} = require('discord.js');
const config = require('../../../Config/config');
const { resolveBugTicket, bugEmbed } = require('./bugReports');
const { resolveClubTicket, clubEmbed } = require('./clubSubmissions');

// Build discord client to interact with the Clubfinity tickets bot
const discordTicketClient = config.discord ? new Client({
  intents: [
    IntentsBitField.Flags.Guilds,
    IntentsBitField.Flags.GuildMembers,
    IntentsBitField.Flags.GuildMessages,
    IntentsBitField.Flags.MessageContent,
  ],
}) : undefined;

// Listener for any interactions with the Clubfinity tickets bot
if (discordTicketClient) {
  discordTicketClient.on('interactionCreate', async (interaction) => {
    if (
      interaction.message.author.id === discordTicketClient.user.id
      && (interaction.isButton() || interaction.isModalSubmit())
    ) {
      if (interaction.channelId === config.discord.tickets.bugChannelId) {
        resolveBugTicket(interaction);
      } else if (interaction.channelId === config.discord.tickets.clubChannelId) {
        resolveClubTicket(interaction);
      }
    }
  });
}

// Main ticket dispatch function -> sends tickets to the appropriate discord channel (in SEC server)
const sendTicket = async (ticket, type) => {
  if (!discordTicketClient) {
    console.log('Unable to process discord ticket request in dev envirionments');
  }

  try {
    if (type === 'bugReport') {
      const bugReportChannel = await discordTicketClient.channels.fetch(
        config.discord.tickets.bugChannelId,
      );
      bugReportChannel.send(bugEmbed(ticket));
    } else if (type === 'clubSubmission') {
      const clubSubmissionChannel = await discordTicketClient.channels.fetch(
        config.discord.tickets.clubChannelId,
      );
      clubSubmissionChannel.send(clubEmbed(ticket));
    }
  } catch (error) {
    console.error(`Failed to fetch, or send ticket to, '${type}' discord channel. Error:\n`, error);
  }
};

module.exports = {
  discordTicketClient,
  sendTicket,
};
