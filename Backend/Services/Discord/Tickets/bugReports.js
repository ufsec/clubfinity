const {
  EmbedBuilder,
  ActionRowBuilder,
  ButtonBuilder,
  ButtonStyle,
} = require('discord.js');
const {
  setEmbedToLoading,
  invalidTicket,
  BOT_AVATAR,
  CLUBFINITY_COLOR,
} = require('./util');
const ticketDAO = require('../../../DAO/TicketDAO');
const userDAO = require('../../../DAO/UserDAO');
const { discordEmailService } = require('./emailTemplates');
const { HttpError } = require('../../../util/errors/httpError');

// Discord embed template for bug tickets
const bugEmbed = (bug) => {
  const discordTimestamp = Math.floor(bug.timestamp.valueOf() / 1000);

  const embed = new EmbedBuilder()
    .setColor(CLUBFINITY_COLOR)
    .setTitle('New Bug Report!  🚨')
    .setFooter({ text: 'Powered by Clubfinity', iconURL: BOT_AVATAR })
    .setDescription(`Submitted at <t:${discordTimestamp}:f>`)
    .addFields(
      { name: 'Description', value: `${bug.bugData}` },
      { name: 'Ticket #', value: `${bug._id}` },
    );

  const buttons = new ActionRowBuilder()
    .addComponents(
      new ButtonBuilder()
        .setCustomId('resolve')
        .setLabel('Resolved')
        .setStyle(ButtonStyle.Primary),
      new ButtonBuilder()
        .setCustomId('discard')
        .setLabel('Discard')
        .setStyle(ButtonStyle.Secondary),
    );

  return { embeds: [embed], components: [buttons] };
};

// Discord embed template for bug tickets after being resolved
const resolvedBugEmbed = (interaction) => {
  const embed = {
    ...interaction.message.embeds[0].data,
    title: 'Bug Resolved! 🛠️',
    description: `Ticket was closed by <@${interaction.user.id}>`,
    fields: [interaction.message.embeds[0].data.fields[0]],
  };

  const buttons = new ActionRowBuilder()
    .addComponents(
      new ButtonBuilder()
        .setCustomId('fixed')
        .setLabel('Resolved')
        .setStyle(ButtonStyle.Success)
        .setDisabled(true)
        .setEmoji('✅'),
    );

  return { embeds: [embed], components: [buttons] };
};

// Method to control admin feedback on bug ticket
const resolveBugTicket = async (interaction) => {
  try {
    // Defer update (give ACK to discord) + set interaction button as "loading"
    await setEmbedToLoading(interaction);

    const ticketId = interaction.message.embeds[0].data.fields[
      interaction.message.embeds[0].data.fields.length - 1
    ].value;
    const { bugData, user: userId } = await ticketDAO.get(ticketId);
    const user = await userDAO.get(userId);
    await ticketDAO.delete(ticketId);

    // Admin has discarded the bug ticket -> remove ticket all together
    if (interaction.customId === 'discard') {
      interaction.message.delete();
      return;
    }

    // Send bug resolution email to user who raised the concern
    discordEmailService('ticketResolved', {
      name: `${user.name.first} ${user.name.last}`,
      email: user.email,
      bugDescription: bugData,
    });

    interaction.editReply(resolvedBugEmbed(interaction));
  } catch (error) {
    if (error instanceof HttpError) {
      // Todo: handle scenario where a user has deleted their account. Should ticket still be valid?
      // https://www.notion.so/ufsec/Fix-discord-tickets-regarding-deleted-users-a178be6842544060892dc4528f7c335d?pvs=4
      interaction.editReply(invalidTicket());
    }
  }
};

module.exports = {
  resolveBugTicket,
  bugEmbed,
};
