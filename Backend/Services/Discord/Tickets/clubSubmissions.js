const {
  EmbedBuilder,
  ActionRowBuilder,
  ButtonBuilder,
  ButtonStyle,
  ModalBuilder,
  TextInputBuilder,
  TextInputStyle,
} = require('discord.js');
const {
  setEmbedToLoading,
  invalidTicket,
  BOT_AVATAR,
  CLUBFINITY_COLOR,
} = require('./util');
const ticketDAO = require('../../../DAO/TicketDAO');
const userDAO = require('../../../DAO/UserDAO');
const clubDAO = require('../../../DAO/ClubDAO');
const { discordEmailService } = require('./emailTemplates');
const { htmlToMarkdownTranslator } = require('../../../util/htmlToMarkdown');
const { HttpError } = require('../../../util/errors/httpError');

// Discord embed template for club creation tickets
const clubEmbed = (clubTicket) => {
  const discordTimestamp = Math.floor(clubTicket.timestamp.valueOf() / 1000);

  const embed = new EmbedBuilder()
    .setColor(CLUBFINITY_COLOR)
    .setTitle('New Club Submission!  📬')
    .setFooter({ text: 'Powered by Clubfinity', iconURL: BOT_AVATAR })
    .setDescription(`Submitted at <t:${discordTimestamp}:f>`)
    .setThumbnail(clubTicket.clubData.thumbnailUrl)
    .addFields(
      { name: 'Club Name', value: `${clubTicket.clubData.name}`, inline: true },
      { name: 'Club Category', value: `${clubTicket.clubData.category}`, inline: true },
      { name: 'Description', value: `${htmlToMarkdownTranslator.translate(clubTicket.clubData.description)}` },
      { name: 'Ticket#', value: `${clubTicket._id}` },
    );

  if (clubTicket.clubData.slackLink) {
    embed.spliceFields(3, 0, { name: 'Slack', value: `[Link](${clubTicket.clubData.slackLink})`, inline: true });
  }
  if (clubTicket.clubData.instagramLink) {
    embed.spliceFields(3, 0, { name: 'Instagram', value: `[Link](${clubTicket.clubData.instagramLink})`, inline: true });
  }
  if (clubTicket.clubData.facebookLink) {
    embed.spliceFields(3, 0, { name: 'Facebook', value: `[Link](${clubTicket.clubData.facebookLink})`, inline: true });
  }
  if (clubTicket.clubData.googleCalLink) {
    embed.spliceFields(3, 0, { name: 'Google Calendar', value: `${clubTicket.clubData.googleCalLink}` });
  }

  const buttons = new ActionRowBuilder()
    .addComponents(
      new ButtonBuilder()
        .setCustomId('accept')
        .setLabel('Accept')
        .setStyle(ButtonStyle.Success),
      new ButtonBuilder()
        .setCustomId('deny')
        .setLabel('Deny')
        .setStyle(ButtonStyle.Danger),
    );

  return { embeds: [embed], components: [buttons] };
};

// Discord embed template for club creation tickets after being accepted
const acceptedClubEmbed = (interaction, err) => {
  let embed = null;
  let buttons = null;

  if (err) {
    embed = {
      ...interaction.message.embeds[0].data,
      title: `Error: ${err.message}  ⚠️`,
      description: `Ticket was closed by <@${interaction.user.id}>`,
      fields: [
        interaction.message.embeds[0].data.fields[0],
        interaction.message.embeds[0].data.fields[2],
      ],
      color: 0xFFD000,
    };

    buttons = new ActionRowBuilder()
      .addComponents(
        new ButtonBuilder()
          .setCustomId('error')
          .setLabel('User Alerted')
          .setStyle(ButtonStyle.Secondary)
          .setDisabled(true)
          .setEmoji('❕'),
      );
  } else {
    embed = {
      ...interaction.message.embeds[0].data,
      title: 'Submission Accepted!  👍',
      description: `Ticket was closed by <@${interaction.user.id}>`,
      fields: [
        interaction.message.embeds[0].data.fields[0],
        interaction.message.embeds[0].data.fields[2],
      ],
      color: 0x096D0B,
    };

    buttons = new ActionRowBuilder()
      .addComponents(
        new ButtonBuilder()
          .setCustomId('created')
          .setLabel('Created')
          .setStyle(ButtonStyle.Success)
          .setDisabled(true)
          .setEmoji('✅'),
      );
  }
  return { embeds: [embed], components: [buttons] };
};

// Discord embed template for club creation tickets after being rejected
const rejectedClubEmbed = (interaction, feedback) => {
  const embed = {
    ...interaction.message.embeds[0].data,
    title: 'Submission Denied!  👎',
    description: `Ticket was closed by <@${interaction.user.id}>`,
    fields: [
      interaction.message.embeds[0].data.fields[0],
      interaction.message.embeds[0].data.fields[2],
      feedback.length > 0 ? { name: 'Feedback', value: feedback } : null,
    ].filter(Boolean),
    color: 0xD32A2A,
  };

  const buttons = new ActionRowBuilder()
    .addComponents(
      new ButtonBuilder()
        .setCustomId('denied')
        .setLabel('Denied')
        .setStyle(ButtonStyle.Danger)
        .setDisabled(true)
        .setEmoji('⛔'),
    );

  return { embeds: [embed], components: [buttons] };
};

// Discord modal template for admin feedback after club creation refusal
const feedbackModal = () => {
  const modal = new ModalBuilder()
    .setCustomId('feedback')
    .setTitle('Ticket Feedback');

  const feedbackInput = new TextInputBuilder()
    .setCustomId('feedbackInput')
    .setLabel('Do you wish to provide any feedback?')
    .setStyle(TextInputStyle.Paragraph)
    .setMaxLength(500)
    .setPlaceholder('Leave empty for no feedback')
    .setRequired(false);

  const firstActionRow = new ActionRowBuilder().addComponents(feedbackInput);

  modal.addComponents(firstActionRow);
  return modal;
};

// Method to control admin feedback on club submission ticket (accept, deny, feedback)
const resolveClubTicket = async (interaction) => {
  try {
    // Admin has accepted club submission
    if (interaction.customId === 'accept') {
      // Defer update (give ACK to discord) + set interaction button as "loading"
      await setEmbedToLoading(interaction);

      let newMessage = null;
      const ticketId = interaction.message.embeds[0].data.fields[
        interaction.message.embeds[0].data.fields.length - 1
      ].value;
      const { clubData, user: userId } = await ticketDAO.get(ticketId);
      const user = await userDAO.get(userId);
      await ticketDAO.delete(ticketId);

      const clubInfo = {
        ...clubData.toObject(),
        admins: [userId],
        description: htmlToMarkdownTranslator.translate(clubData.description),
      };

      // Create new club
      await clubDAO.create(clubInfo)
        .then(() => {
          newMessage = acceptedClubEmbed(interaction);

          // Send club creation notice to user
          discordEmailService('clubCreated', {
            name: `${user.name.first} ${user.name.last}`,
            email: user.email,
            clubName: clubInfo.name,
          });
        })
        .catch((err) => {
          newMessage = acceptedClubEmbed(interaction, err);

          // Send club creation error notice to user (most likely due to club name already existing)
          discordEmailService('clubCreationError', {
            name: `${user.name.first} ${user.name.last}`,
            email: user.email,
            clubName: clubInfo.name,
            error: err.message,
          });
        });

      interaction.editReply(newMessage);
      // Admin has refused submission -> activate modal for optional feedback
    } else if (interaction.customId === 'deny') {
      interaction.showModal(feedbackModal());
      // Admin has finished giving feedback for club submission refusal
    } else if (interaction.customId === 'feedback') {
      // Defer update (give ACK to discord) + set interaction button as "loading"
      await setEmbedToLoading(interaction);

      const ticketId = interaction.message.embeds[0].data.fields[
        interaction.message.embeds[0].data.fields.length - 1
      ].value;
      const { clubData, user: userId } = await ticketDAO.get(ticketId);
      const user = await userDAO.get(userId);
      await ticketDAO.delete(ticketId);

      const feedback = interaction.fields.fields.get('feedbackInput').value;

      // Send feedback reasoning to user if given by Admin
      if (feedback.length > 0) {
        discordEmailService('clubRejected', {
          name: `${user.name.first} ${user.name.last}`,
          email: user.email,
          clubName: clubData.name,
          feedback,
        });
      }

      interaction.editReply(rejectedClubEmbed(interaction, feedback));
    }
  } catch (error) {
    if (error instanceof HttpError) {
      // Todo: handle scenario where a user has deleted their account. Should ticket still be valid?
      // https://www.notion.so/ufsec/Fix-discord-tickets-regarding-deleted-users-a178be6842544060892dc4528f7c335d?pvs=4
      interaction.editReply(invalidTicket());
    }
  }
};

module.exports = {
  resolveClubTicket,
  clubEmbed,
};
