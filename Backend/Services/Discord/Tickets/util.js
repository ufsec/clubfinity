const {
  ActionRowBuilder,
  ButtonBuilder,
  ButtonStyle,
} = require('discord.js');

const BOT_AVATAR = 'https://cdn.discordapp.com/avatars/1075609239428538440/6e59002204edc78140e9fd23f18cfaa7.png?size=256';
const CLUBFINITY_COLOR = 0x0099FF;

// Discord embed modifications for tickets in "loading" state
const setEmbedToLoading = async (interaction) => {
  const buttons = new ActionRowBuilder()
    .addComponents(
      new ButtonBuilder()
        .setCustomId('fixed')
        .setLabel('Updating ticket..')
        .setStyle(ButtonStyle.Secondary)
        .setDisabled(true),
    );

  await interaction.message.edit({ components: [buttons] });
  await interaction.deferUpdate();
};

// Discord embed template for invalid/expired tickets
const invalidTicket = () => ({
  content: 'This ticket has been removed as it is no longer valid. You may delete this message.',
  embeds: [],
  components: [],
});

module.exports = {
  setEmbedToLoading,
  invalidTicket,
  BOT_AVATAR,
  CLUBFINITY_COLOR,
};
