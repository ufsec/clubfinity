// Run this file when changes are made to the commands
// This is mostly taken from the discord.js guide

require('dotenv').config();
const { REST, Routes } = require('discord.js');
const fs = require('fs');
const path = require('path');
const config = require('../../../Config/config');

const commands = [];
// Fetch all commmand files within codebase (path is case sensitive in pipeline)
const commandsPath = path.join(__dirname, 'Commands');
const commandFiles = fs.readdirSync(commandsPath).filter((file) => file.endsWith('.js'));

// Read all command files within codebase
// eslint-disable-next-line no-restricted-syntax
for (const file of commandFiles) {
  const filePath = path.join(commandsPath, file);
  // eslint-disable-next-line import/no-dynamic-require, global-require
  const command = require(filePath);
  commands.push(command.data.toJSON());
}

const rest = new REST({ version: '10' }).setToken(config.discord.echo.botToken);

const registerCommands = async () => {
  await rest.put(
    // Global command registration - register commands for all servers
    Routes.applicationCommands(config.discord.echo.applicationId),
    { body: commands },
  );
};

registerCommands();
