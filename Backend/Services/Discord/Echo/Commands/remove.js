const {
  SlashCommandBuilder,
  ChannelType,
  EmbedBuilder,
} = require('discord.js');
const clubDAO = require('../../../../DAO/ClubDAO');
const listenerDAO = require('../../../../DAO/ListenerDAO');
const {
  BOT_AVATAR,
  CLUBFINITY_COLOR,
} = require('../util');

// Emebed response whenever a listener was sucessfully removed
const listenerRemovedMessage = async (interaction, listener, club) => {
  const channelText = `<#${listener.channelId}>`;

  const embed = new EmbedBuilder()
    .setColor(CLUBFINITY_COLOR)
    .setTitle('Listener Removed!  🗑️')
    .setFooter({ text: 'Powered by Clubfinity', iconURL: BOT_AVATAR })
    .setThumbnail(club.thumbnailUrl)
    .addFields(
      { name: 'Club', value: `${club.name}` },
      { name: 'Channel', value: channelText },
    );

  await interaction.reply({
    embeds: [embed],
  });
};

// Emebed response whenever a listener deletion encountered an error
const listenerErrorMessage = async (interaction, error) => {
  const message = `**Error:** ${error.message}

You may safely delete this message!`;

  await interaction.reply({
    content: message,
    components: [],
  });
};

// Remove listener via Listener DAO (into the DB) & Update bot response
const removeListener = async (interaction) => {
  const clubName = interaction.options.getString('club');
  const channelId = interaction.options.getChannel('channel').id;

  try {
    const listener = await listenerDAO.removeListener(clubName, 'discord', interaction.guildId, channelId);
    const club = await clubDAO.get(listener.club);

    await listenerRemovedMessage(interaction, listener, club);
  } catch (error) {
    await listenerErrorMessage(interaction, error);
  }
};

// Initialize Discord command
const remove = new SlashCommandBuilder()
  .setName('remove')
  .setDescription('Remove an existing club listener')
  .addStringOption((option) => option
    .setName('club')
    .setDescription('Name of club you wish to stop listening to')
    .setRequired(true)
    .setAutocomplete(true))
  .addChannelOption((option) => option
    .setName('channel')
    .setDescription('The channel where you wish to stop receiving the club\'s updates')
    .addChannelTypes(ChannelType.GuildText)
    .setRequired(true));

// function to work the "auto complete" club param in the command
const autocomplete = async (interaction) => {
  const focusedValue = interaction.options.getFocused();
  const matches = await clubDAO.search(focusedValue);
  await interaction.respond(
    matches.map((choice) => ({ name: choice.name, value: choice.name })),
  );
};

// Core logic of /remove command
const execute = async (interaction) => {
  await removeListener(interaction);
};

module.exports = {
  data: remove,
  autocomplete,
  execute,
};
