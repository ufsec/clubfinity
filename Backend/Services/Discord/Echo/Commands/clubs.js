const {
  ActionRowBuilder,
  EmbedBuilder,
  ButtonBuilder,
  SlashCommandBuilder,
  ButtonStyle,
  ComponentType,
} = require('discord.js');
const clubDAO = require('../../../../DAO/ClubDAO');
const {
  EMBED_TTL,
  EMBED_PAGE_SIZE,
  BOT_AVATAR,
  CLUBFINITY_COLOR,
  CLUB_CATEGORY_EMOJIS,
} = require('../util');

// Main embed used to display list of clubs
const clubPageEmbed = new EmbedBuilder()
  .setColor(CLUBFINITY_COLOR)
  .setThumbnail(BOT_AVATAR)
  .setFooter({ text: 'Powered by Clubfinity', iconURL: BOT_AVATAR });

// "previous" button to go back in pagination list
const prevButton = new ButtonBuilder()
  .setCustomId('prev')
  .setEmoji('⬅️')
  .setStyle(ButtonStyle.Primary)
  .setDisabled(true);

// "next" button to go forward in pagination list
const nextButton = new ButtonBuilder()
  .setCustomId('next')
  .setEmoji('➡️')
  .setStyle(ButtonStyle.Primary);

// Component to store all butons ("previous" & "next")
const buttonRow = new ActionRowBuilder().addComponents(prevButton, nextButton);

// Initialize Discord command
const clubs = new SlashCommandBuilder()
  .setName('clubs')
  .setDescription('Lists all clubs in clubfinity')
  .addStringOption((option) => option
    .setName('category')
    .setDescription('Filter clubs by a specific category')
    .setRequired(false)
    // Categories of clubs currently "hardcoded" -> TODO: Have one true source for club categories
    .addChoices(
      { name: 'Computer Science', value: 'Computer Science' },
      { name: 'Research', value: 'Research' },
      { name: 'Business', value: 'Business' },
      { name: 'Arts', value: 'Arts' },
      { name: 'Engineering', value: 'Engineering' },
      { name: 'Health', value: 'Health' },
      { name: 'Journalism', value: 'Journalism' },
      { name: 'Liberal Arts', value: 'Liberal Arts' },
      { name: 'Cultural', value: 'Cultural' },
      { name: 'Honor Society', value: 'Honor Society' },
      { name: 'Media', value: 'Media' },
      { name: 'Professional/Career', value: 'Professional/Career' },
      { name: 'Religious/Spiritual', value: 'Religious/Spiritual' },
      { name: 'Sport Clubs', value: 'Sport Clubs' },
      { name: 'Student Government', value: 'Student Government' },
    ));

// Updates the clubs embed (list of clubs) based on pagination results from DB
const updateClubsEmbed = async (page, pageSize, clubCategory) => {
  const filters = clubCategory ? { category: clubCategory } : {};
  const clubPageData = await clubDAO.getPage(page, pageSize, filters);

  // If given club category as no registeredl listeners
  if (clubPageData.totalClubs === 0) {
    return {
      empty: true,
    };
  }

  const lastPageNum = Math.ceil(clubPageData.totalClubs / clubPageData.pageSize);

  const mainTitle = clubCategory ? `${clubCategory} Clubs` : 'All Clubs';
  clubPageEmbed.setTitle(`${mainTitle}${lastPageNum > 1 ? ` - Page ${clubPageData.page}/${lastPageNum}` : ''}`);

  // Create the "fields" of the discord embed. For each -> (club, category emoji, socials)
  const fields = clubPageData.clubs.map((club) => {
    const socialLinks = [];

    if (club.instagramLink) {
      socialLinks.push(`[Instagram](${club.instagramLink})`);
    }
    if (club.facebookLink) {
      socialLinks.push(`[Facebook](${club.facebookLink})`);
    }
    if (club.slackLink) {
      socialLinks.push(`[Slack](${club.slackLink})`);
    }

    // TODO: Add any additional socials which are added to Clubfinity

    // Empty character after the socials is there on purpose (spacing)
    const clubDescription = `${socialLinks.length}` > 0 ? `${socialLinks.join(' | ')}\n ㅤ` : 'No links available\n';
    const clubName = `${CLUB_CATEGORY_EMOJIS[club.category]}  ${club.name}`;

    return { name: clubName, value: clubDescription };
  });

  clubPageEmbed.setFields(fields);

  if (page === 1) prevButton.setDisabled(true);
  else prevButton.setDisabled(false);

  if (page === lastPageNum) nextButton.setDisabled(true);
  else nextButton.setDisabled(false);

  return {
    empty: false,
    numPages: lastPageNum,
  };
};

// Core logic of /clubs command
const execute = async (interaction) => {
  // ACK user command -> allow more time for bot processing (needed for responses beyond 3s)
  await interaction.deferReply();

  let page = 1;

  const clubCategory = interaction.options.getString('category');
  const { empty, numPages } = await updateClubsEmbed(page, EMBED_PAGE_SIZE, clubCategory);

  // Given category has no registered clubs in DB
  if (empty) {
    await interaction.editReply({
      content: 'Sorry, there exists no clubs currently registered under this category!',
    });
    return;
  }

  const currentPage = await interaction.editReply({
    embeds: [clubPageEmbed],
    components: (numPages > 1) ? [buttonRow] : [],
    fetchReply: true,
  });

  // Create collector for "next" and "prev" buttons (pagination)
  const collector = await currentPage.createMessageComponentCollector({
    componentType: ComponentType.Button,
    time: EMBED_TTL,
  });

  // Collect any actions on the "next" & "prev" buttons
  collector.on('collect', async (i) => {
    // ACK user interaction -> allow more time for a bot update (needed for responses beyond 3s)
    await i.deferUpdate();

    if (i.user.id !== interaction.user.id) {
      return i.reply({
        content: "You can't use these buttons",
        ephemeral: true,
      });
    }

    // Update page number
    if (i.customId === 'prev') {
      if (page > 1) page -= 1;
    } else if (i.customId === 'next') {
      if (page < numPages) page += 1;
    }

    await updateClubsEmbed(page, EMBED_PAGE_SIZE, clubCategory);

    // Update embed with the next/prev page of clubs
    await i.editReply({
      embeds: [clubPageEmbed],
      components: [buttonRow],
    });

    // Reset collector timer if user interactions with embed
    return collector.resetTimer();
  });

  // Ending the collector (happens after "time" milliseconds) -> "time" is a constant
  collector.on('end', async () => {
    // Remove buttons from embed -> does not allow any further user interaction
    await currentPage.edit({
      embeds: [clubPageEmbed],
      components: [],
    });
  });
};

module.exports = {
  data: clubs,
  execute,
};
