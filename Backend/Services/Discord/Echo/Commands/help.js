const {
  EmbedBuilder,
  SlashCommandBuilder,
} = require('discord.js');
const {
  BOT_AVATAR,
  CLUBFINITY_COLOR,
} = require('../util');

// Embed used to display list of available commands (NEW COMMAND -> ADD INFO HERE!!)
const helpCommandEmbed = new EmbedBuilder()
  .setColor(CLUBFINITY_COLOR)
  .setThumbnail(BOT_AVATAR)
  .setTitle('Help Center')
  .setFooter({ text: 'Powered by Clubfinity', iconURL: BOT_AVATAR })
  .setFields([
    {
      name: '```/listen <club> <channel>```',
      value: 'Start listening to a club\'s events/announcements in your server',
    },
    {
      name: '```/remove <club> <channel>```',
      value: 'Delete a previously created listener',
    },
    {
      name: '```/listeners```',
      value: 'Display all currently registered listeners within your server',
    },
    {
      name: '```/clubs <category>```',
      value: 'Get a list of all clubs on Clubfinity',
    },
  ]);

// Initialize Discord command
const help = new SlashCommandBuilder()
  .setName('help')
  .setDescription('See all the commands available for Clubfinity Echo');

// Core logic of /help command
const execute = async (interaction) => {
  // ACK user command -> allow more time for bot processing (needed for responses beyond 3s)
  await interaction.deferReply();

  await interaction.editReply({
    embeds: [helpCommandEmbed],
  });
};

module.exports = {
  data: help,
  execute,
};
