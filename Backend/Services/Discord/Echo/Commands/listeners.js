const {
  ActionRowBuilder,
  EmbedBuilder,
  ButtonBuilder,
  SlashCommandBuilder,
  ButtonStyle,
  ComponentType,
} = require('discord.js');
const listenerDAO = require('../../../../DAO/ListenerDAO');
const {
  EMBED_TTL,
  EMBED_PAGE_SIZE,
  BOT_AVATAR,
  CLUBFINITY_COLOR,
  CLUB_CATEGORY_EMOJIS,
} = require('../util');

// Main embed used to display list of listeners
const listenersPageEmbed = new EmbedBuilder()
  .setColor(CLUBFINITY_COLOR)
  .setThumbnail(BOT_AVATAR)
  .setFooter({ text: 'Powered by Clubfinity', iconURL: BOT_AVATAR });

// "previous" button to go back in pagination list
const prevButton = new ButtonBuilder()
  .setCustomId('prev')
  .setEmoji('⬅️')
  .setStyle(ButtonStyle.Primary)
  .setDisabled(true);

// "next" button to go forward in pagination list
const nextButton = new ButtonBuilder()
  .setCustomId('next')
  .setEmoji('➡️')
  .setStyle(ButtonStyle.Primary);

// Component to store all butons ("previous" & "next")
const buttonRow = new ActionRowBuilder().addComponents(prevButton, nextButton);

// Initialize Discord command
const listeners = new SlashCommandBuilder()
  .setName('listeners')
  .setDescription('Lists all registered listeners in this server');

// Updates the listeners embed (list of listeners) based on current page & all listeners
const updateListenersEmbed = (registeredlisteners, page, totalPages) => {
  const startIndex = (page - 1) * EMBED_PAGE_SIZE;
  const endIndex = Math.min(startIndex + EMBED_PAGE_SIZE, registeredlisteners.length);
  const currentListeners = registeredlisteners.slice(startIndex, endIndex);

  // Create the "fields" of the discord embed. For each -> (club, channel, roles/mentions)
  const fields = currentListeners.map((listener) => {
    const rolesArray = listener.roles.map((roleId) => {
      if (roleId === 'everyone') {
        return '@everyone';
      }
      return `<@&${roleId}>`;
    });
    const rolesText = rolesArray.length > 0 ? rolesArray.join(', ') : 'N/A';

    const listenerDescription = `Channel: <#${listener.channelId}> | Roles: ${rolesText}`;
    return { name: `${CLUB_CATEGORY_EMOJIS[listener.club.category]}  ${listener.club.name}`, value: listenerDescription };
  });

  listenersPageEmbed.setFields(fields);
  listenersPageEmbed.setTitle(`Registered Listeners${totalPages > 1 ? ` - Page ${page}/${totalPages}` : ''}`);

  if (page === 1) prevButton.setDisabled(true);
  else prevButton.setDisabled(false);

  if (page === totalPages) nextButton.setDisabled(true);
  else nextButton.setDisabled(false);
};

// Core logic of /listeners command
const execute = async (interaction) => {
  // ACK user command -> allow more time for bot processing (needed for responses beyond 3s)
  await interaction.deferReply();

  const registeredlisteners = await listenerDAO.getServerListeners('discord', interaction.guildId);

  // If server has no listeners registered
  if (registeredlisteners.length === 0) {
    await interaction.editReply({
      content: 'No listeners are currently registred within this server! Use /listen to create new listeners.',
      ephemeral: true,
    });
    return;
  }

  let page = 1;
  const totalPages = Math.ceil(registeredlisteners.length / EMBED_PAGE_SIZE);

  updateListenersEmbed(registeredlisteners, page, totalPages);

  const currentPage = await interaction.editReply({
    embeds: [listenersPageEmbed],
    components: (totalPages > 1) ? [buttonRow] : [],
    fetchReply: true,
  });

  // Create collector for "next" and "prev" buttons (pagination)
  const collector = await currentPage.createMessageComponentCollector({
    componentType: ComponentType.Button,
    time: EMBED_TTL,
  });

  // Collect any actions on the "next" & "prev" buttons
  collector.on('collect', async (i) => {
    // ACK user interaction -> allow more time for a bot update (needed for responses beyond 3s)
    await i.deferUpdate();

    if (i.user.id !== interaction.user.id) {
      return i.reply({
        content: "You can't use these buttpons",
        ephemeral: true,
      });
    }

    // Update page number
    if (i.customId === 'prev') {
      if (page > 1) page -= 1;
    } else if (i.customId === 'next') {
      if (page < totalPages) page += 1;
    }

    updateListenersEmbed(registeredlisteners, page, totalPages);

    // Update embed with the next/prev page of listeners
    await i.editReply({
      embeds: [listenersPageEmbed],
      components: [buttonRow],
    });

    // Reset collector timer if user interactions with embed
    return collector.resetTimer();
  });

  // Ending the collector (happens after "time" milliseconds) -> "time" is a constant
  collector.on('end', async () => {
    // Remove buttons from embed -> does not allow any further user interaction
    await currentPage.edit({
      embeds: [listenersPageEmbed],
      components: [],
    });
  });
};

module.exports = {
  data: listeners,
  execute,
};
