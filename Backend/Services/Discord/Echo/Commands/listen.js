const {
  SlashCommandBuilder,
  ChannelType,
  RoleSelectMenuBuilder,
  ActionRowBuilder,
  ButtonBuilder,
  ComponentType,
  EmbedBuilder,
} = require('discord.js');
const clubDAO = require('../../../../DAO/ClubDAO');
const listenerDAO = require('../../../../DAO/ListenerDAO');
const {
  EMBED_TTL,
  BOT_AVATAR,
  CLUBFINITY_COLOR,
} = require('../util');

// Selector to choose which roles to ping
const selectRolesSelector = new RoleSelectMenuBuilder()
  .setCustomId('role_selector')
  .setPlaceholder('Choose your roles! (see below for default options)')
  .setMaxValues(5);

// "@everyone" button -> ping all server members
const everyoneRoleButton = new ButtonBuilder()
  .setCustomId('everyone')
  .setLabel('@everyone')
  .setStyle('Primary');

// "No Mentions" button -> no pings
const noMentionsButton = new ButtonBuilder()
  .setCustomId('no_mentions')
  .setLabel('No Mentions')
  .setStyle('Secondary');

// Cancel creation of listener
const cancelEveryoneButton = new ButtonBuilder()
  .setCustomId('cancel')
  .setLabel('Cancel')
  .setStyle('Danger');

// Component for role/mention selection for listener
const row = new ActionRowBuilder()
  .addComponents(selectRolesSelector);

// Component for predefined mention options (@everyone OR no mentions OR cancel)
const row2 = new ActionRowBuilder()
  .addComponents(everyoneRoleButton, noMentionsButton, cancelEveryoneButton);

// Emebed response whenever a listener was sucessfully registered
const listenerAddedMessage = async (interaction, listener, isNew, club) => {
  const rolesArray = listener.roles.map((roleId) => {
    if (roleId === 'everyone') {
      return '@everyone';
    }
    return `<@&${roleId}>`;
  });
  const rolesText = rolesArray.join(', ');

  const channelText = `<#${listener.channelId}>`;
  const listenerStatus = isNew ? 'Registered' : 'Updated';

  const embed = new EmbedBuilder()
    .setColor(CLUBFINITY_COLOR)
    .setTitle(`Listener ${listenerStatus}!  🎉`)
    .setFooter({ text: 'Powered by Clubfinity', iconURL: BOT_AVATAR })
    .setThumbnail(club.thumbnailUrl)
    .addFields(
      { name: 'Club', value: `${club.name}` },
      { name: 'Channel', value: channelText },
      { name: 'Roles', value: rolesText || 'N/A' },
    );

  await interaction.editReply({
    embeds: [embed],
    content: null,
    components: [],
  });
};

// Emebed response whenever a listener registration encountered an error
const listenerErrorMessage = async (interaction, error) => {
  const message = `**Error:** ${error.message}

You may safely delete this message!`;

  await interaction.editReply({
    content: message,
    components: [],
  });
};

// Add listener via Listener DAO (into the DB) & update bot response
const addListener = async (interaction, roles) => {
  const clubName = interaction.options.getString('club');
  const channelId = interaction.options.getChannel('channel').id;

  try {
    const res = await listenerDAO.addListener(clubName, 'discord', interaction.guildId, channelId, roles);
    const club = await clubDAO.get(res.listener.club);

    await listenerAddedMessage(interaction, res.listener, res.isNew, club);
  } catch (error) {
    await listenerErrorMessage(interaction, error);
  }
};

// Initialize Discord command
const listen = new SlashCommandBuilder()
  .setName('listen')
  .setDescription('Listen to a club\'s announcement and events')
  .addStringOption((option) => option
    .setName('club')
    .setDescription('Name of club you wish to listen to')
    .setRequired(true)
    .setAutocomplete(true))
  .addChannelOption((option) => option
    .setName('channel')
    .setDescription('The channel the announcement will be sent in.')
    .addChannelTypes(ChannelType.GuildText)
    .setRequired(true));

// function to work the "auto complete" club param in the command
const autocomplete = async (interaction) => {
  const focusedValue = interaction.options.getFocused();
  const matches = await clubDAO.search(focusedValue);
  await interaction.respond(
    matches.map((choice) => ({ name: choice.name, value: choice.name })),
  );
};

// Core logic of /listen command
const execute = async (interaction) => {
  const chooseRolesMessage = await interaction.reply({
    content: 'Please select any mentions for new club announcements/events:',
    components: [row, row2],
  });

  // Create collector for custom roles/mentions selection
  const roleSelectorCollector = await chooseRolesMessage.createMessageComponentCollector({
    componentType: ComponentType.RoleSelect,
    filter: (i) => i.user.id === interaction.user.id && i.customId === 'role_selector',
    time: EMBED_TTL,
  });

  // Create collector for predefined mention otpions (@everyone OR no mentions OR cancel)
  const alternateOptionsCollector = await chooseRolesMessage.createMessageComponentCollector({
    componentType: ComponentType.Button,
    filter: (i) => i.user.id === interaction.user.id && (
      i.customId === 'everyone' || i.customId === 'no_mentions' || i.customId === 'cancel'
    ),
    time: EMBED_TTL,
  });

  let messageHandled = false;

  // Collect any actions on custom role selector
  roleSelectorCollector.on('collect', async (i) => {
    // ACK user interaction -> allow more time for a bot update (needed for responses beyond 3s)
    await i.deferUpdate();
    messageHandled = true;

    if (i.values.length) {
      await addListener(interaction, i.values);
    }
  });

  // Collect any actions on the predefined options
  alternateOptionsCollector.on('collect', async (i) => {
    // ACK user interaction -> allow more time for a bot update (needed for responses beyond 3s)
    await i.deferUpdate();
    messageHandled = true;

    // Listener registration was cancelled
    if (i.customId !== 'cancel') {
      await addListener(interaction, i.customId === 'everyone' ? ['everyone'] : []);
    } else {
      try {
        await chooseRolesMessage.delete();
      } catch (error) {
        console.error('Error deleting message:', error);
      }
    }
  });

  roleSelectorCollector.on('end', async () => {
    // Check if message was already deleted (prevents deletion errors)
    if (!messageHandled) {
      try {
        await chooseRolesMessage.delete();
      } catch (error) {
        console.error(error);
      }
    }
  });

  return chooseRolesMessage;
};

module.exports = {
  data: listen,
  autocomplete,
  execute,
};
