const { EmbedBuilder } = require('discord.js');
const { discordEchoClient } = require('./bot');
const clubDAO = require('../../../DAO/ClubDAO');
const listenerDAO = require('../../../DAO/ListenerDAO');
const { htmlToMarkdownTranslator } = require('../../../util/htmlToMarkdown');

const botAvatar = 'http://clubfinity.assets.ufsec.org/clubfinity_logo';

// Main embed logic used for new events/announcements
const clubUpdateEmbed = async (update, type, club, listener) => {
  let content = '';

  // Building the proper mentions
  listener.roles.forEach((role, index) => {
    switch (role) {
      case 'everyone':
        content += '@everyone';
        break;
      default:
        content += `<@&${role}>`;
    }
    if (index !== listener.roles.length - 1) {
      content += ' ';
    }
  });

  // Transform the html description (as stored in DB) to markdown (what is used in Discord)
  const updateDescriptionMarkdown = htmlToMarkdownTranslator.translate(update.description);

  const embed = new EmbedBuilder()
    .setColor(0x0099FF)
    .setFooter({ text: 'Powered by Clubfinity', iconURL: botAvatar })
    .setThumbnail(club.thumbnailUrl);

  // Building update specific details (based on whether it is a event or announcement dispatch)
  if (type === 'announcement') {
    embed.setTitle(`📢  New Annoucement - ${club.name}`);
    embed.addFields(
      { name: 'Title', value: `${update.title}` },
      { name: 'Announcement', value: updateDescriptionMarkdown },
    );
  } else if (type === 'event') {
    const discordTimestamp = Math.floor(update.date.valueOf() / 1000);

    embed.setTitle(`📅  New Event - ${club.name}`);
    embed.addFields(
      { name: 'Name', value: `${update.name}` },
      { name: 'Date', value: `<t:${discordTimestamp}:f>` },
      { name: 'Announcement', value: updateDescriptionMarkdown },
    );

    if (update.facebookLink) {
      embed.spliceFields(3, 0, { name: 'Facebook', value: `[Link](${update.facebookLink})`, inline: true });
    }
  }

  return {
    content,
    embeds: [embed],
  };
};

// Dispatches club new events/annoucements to all registered listeners of that club
const notifyListeners = async (update, type) => {
  const club = await clubDAO.get(update.club);
  const listeners = await listenerDAO.getClubListeners('discord', update.club);

  if (discordEchoClient.isReady() && (type === 'announcement' || type === 'event')) {
    listeners.forEach(async (listener) => {
      try {
        const channel = await discordEchoClient.channels.fetch(listener.channelId);

        const embed = await clubUpdateEmbed(update, type, club, listener);
        channel.send(embed);

        // Reset failure count to 0 if it had previously failed (we only track failures "in a row")
        if (listener.failures > 0) {
          await listenerDAO.updateListenerFailures(listener._id, 0);
        }
      } catch (error) {
        console.error(`Failed to dispatch announcement from club '${club.name}' to channelId: ${listener.channelId}`);
        // Increemnt failure count (if 3+, the listener will be deleted)
        await listenerDAO.updateListenerFailures(listener._id, listener.failures + 1);
      }
    });
  }
};

module.exports = {
  notifyListeners,
};
