const {
  Client, IntentsBitField, Collection, Events,
} = require('discord.js');
const fs = require('fs');
const path = require('path');

// Create Discord client used for Echo bot
const discordEchoClient = new Client({
  intents: [
    IntentsBitField.Flags.Guilds,
    IntentsBitField.Flags.GuildMembers,
    IntentsBitField.Flags.GuildMessages,
    IntentsBitField.Flags.MessageContent,
  ],
});

discordEchoClient.commands = new Collection();

// Fetch all commmand files within codebase (path is case sensitive in pipeline)
const commandsPath = path.join(__dirname, 'Commands');
const commandFiles = fs.readdirSync(commandsPath).filter((file) => file.endsWith('js'));

// Read all command files within codebase
// eslint-disable-next-line no-restricted-syntax
for (const file of commandFiles) {
  const filePath = path.join(commandsPath, file);
  // eslint-disable-next-line import/no-dynamic-require, global-require
  const command = require(filePath);

  if ('data' in command && 'execute' in command) {
    discordEchoClient.commands.set(command.data.name, command);
  } else {
    console.log(`[WARNING] The command at ${filePath} is missing a required "data" or "execute" property.`);
  }
}

// Main collector of events for the Echo bot -> dispatch logic to respective command
discordEchoClient.on(Events.InteractionCreate, async (interaction) => {
  const command = interaction.client.commands.get(interaction.commandName);

  // Ingore other intereactions (such as buttons, selectors, etc..) -> handled by command files
  if (!command) return;

  try {
    // Autocomplete commands are seperated here, altough logic is basically the game
    // Commands w/ autocomplete -> /listen & /remove
    if (interaction.isChatInputCommand()) {
      await command.execute(interaction);
    } else if (interaction.isAutocomplete()) {
      await command.autocomplete(interaction);
    }
  } catch (error) {
    console.error(error);
    const errorMessage = 'There was an error while executing this command!';
    if (interaction.replied || interaction.deferred) {
      await interaction.followUp({ content: errorMessage, ephemeral: true });
    } else {
      await interaction.reply({ content: errorMessage, ephemeral: true });
    }
  }
});

module.exports = {
  discordEchoClient,
};
