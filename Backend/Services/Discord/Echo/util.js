/* eslint-disable quote-props */
const EMBED_TTL = 30000;
const EMBED_PAGE_SIZE = 5;
const BOT_AVATAR = 'https://cdn.discordapp.com/avatars/1075609239428538440/6e59002204edc78140e9fd23f18cfaa7.png?size=256';
const CLUBFINITY_COLOR = 0x0099FF;
const CLUB_CATEGORY_EMOJIS = {
  'Computer Science': '💻',
  'Research': '🔬',
  'Business': '💼',
  'Arts': '🎨',
  'Engineering': '🛠️',
  'Health': '🏥',
  'Journalism': '📰',
  'Liberal Arts': '📘',
  'Cultural': '🎭',
  'Honor Society': '🏅',
  'Media': '🎥',
  'Professional/Career': '🎓',
  'Religious/Spiritual': '🌄',
  'Sport Clubs': '🏀',
  'Student Government': '🏛️',
};

module.exports = {
  EMBED_TTL,
  EMBED_PAGE_SIZE,
  BOT_AVATAR,
  CLUBFINITY_COLOR,
  CLUB_CATEGORY_EMOJIS,
};
