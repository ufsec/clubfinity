const config = require('../../Config/config');
const { discordTicketClient } = require('./Tickets/bot');
const { discordEchoClient } = require('./Echo/bot');

// Function used to login/launch given bot (via client & token parameters)
const launchBot = (client, token, botName) => {
  if (token) {
    try {
      client.login(token);
    } catch (error) {
      console.error(`Failed to launch Discord ${botName} bot. Error:\n`, error);
    }
  }
};

/*
Launch all Discord bots (aka. turn them "on")

Tickets bot - internal tool for bug reports & club creation requests
(Create ticket on the app -> SEC Discord server - "Clubfinity Tickets" channel group)

Echo bot - tool for users/communities to "listen" to club updates
(Make event/ann. on app -> discord bot notifies the relevant discord server(s))
*/
const discordLogin = () => {
  launchBot(discordTicketClient, config.discord?.tickets?.botToken, 'Tickets');
  launchBot(discordEchoClient, config.discord?.echo?.botToken, 'Echo');
};

module.exports = {
  discordLogin,
};
