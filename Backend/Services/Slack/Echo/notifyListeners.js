const { WebClient } = require('@slack/web-api');
const slackifyMarkdown = require('slackify-markdown');
const clubDAO = require('../../../DAO/ClubDAO');
const listenerDAO = require('../../../DAO/ListenerDAO');
const IntegrationTokenDAO = require('../../../DAO/IntegrationTokenDAO');
const { htmlToMarkdownTranslator } = require('../../../util/htmlToMarkdown');

const botAvatar = 'http://clubfinity.assets.ufsec.org/clubfinity_logo';

// Creates and formats message to be sent in Slack channels
const createMessage = async (type, message, club, listener) => {
  let mentions = '';

  // Building the proper mentiosn for the message
  listener.roles.forEach((role, index) => {
    switch (role) {
      case 'channel':
        mentions += '<!channel>';
        break;
      default:
        mentions += `<!${role}>`;
    }
    if (index !== listener.roles.length - 1) {
      mentions += ' ';
    }
  });

  // Converts message to markdown then converts markdown to Slack markdown
  const messageInMarkdown = htmlToMarkdownTranslator.translate(message.description);
  const messageInSlackMarkdown = slackifyMarkdown(messageInMarkdown);
  const clubObject = await clubDAO.get(message.club);

  let blocks = [];

  // Builds blocks for Slack API based on type of message
  if (type === 'announcement') {
    blocks = [
      {
        type: 'header',
        text: {
          type: 'plain_text',
          text: `📢 New Announcement - ${clubObject.name}`,
        },
      },
      {
        type: 'section',
        text: {
          type: 'mrkdwn',
          text: `*${message.title}* ${mentions}\n\n${messageInSlackMarkdown}`,
        },
        accessory: {
          type: 'image',
          image_url: clubObject.thumbnailUrl || botAvatar,
          alt_text: 'Club Logo',
        },
      },
      {
        type: 'context',
        elements: [
          {
            type: 'image',
            image_url: botAvatar,
            alt_text: 'Clubfinity Logo',
          },
          {
            type: 'mrkdwn',
            text: 'Powered by Clubfinity',
          },
        ],
      },
    ];
  } else if (type === 'event') {
    const slackTimestamp = Math.floor(message.date.valueOf() / 1000);
    blocks = [
      {
        type: 'header',
        text: {
          type: 'plain_text',
          text: `📅 New Event - ${clubObject.name}`,
        },
      },
      {
        type: 'section',
        text: {
          type: 'mrkdwn',
          text: `*Name:* ${message.name}\n*Date:* <!date^${slackTimestamp}^{date} at {time}|Error Loading Date>\n*Location:* ${message.location}\n*Description*\n${messageInSlackMarkdown}`,
        },
        accessory: {
          type: 'image',
          image_url: clubObject.thumbnailUrl || botAvatar,
          alt_text: 'Club Logo',
        },
      },
      {
        type: 'section',
        text: {
          type: 'mrkdwn',
          text: `\n${mentions}`,
        },
      },
      {
        type: 'context',
        elements: [
          {
            type: 'image',
            image_url: botAvatar,
            alt_text: 'Clubfinity Logo',
          },
          {
            type: 'mrkdwn',
            text: 'Powered by Clubfinity',
          },
        ],
      },
    ];
  }

  return {
    text: `New ${type} from ${club.name}! `,
    blocks,
  };
};

// Dispatches message to Slack channels
const notifySlackListeners = async (message, type) => {
  if (type !== 'announcement' && type !== 'event') return;

  const club = await clubDAO.get(message.club);
  const listeners = await listenerDAO.getClubListeners('slack', message.club);

  // For each listener, send message to Slack channel
  listeners.forEach(async (listener) => {
    try {
      const botToken = await IntegrationTokenDAO.getIntegrationToken(listener.platformId);
      const slackClient = new WebClient(botToken);

      const text = await createMessage(type, message, club, listener);

      await slackClient.chat.postMessage({
        channel: listener.channelId,
        ...text,
      });

      // Reset failure count to 0 if it had previously failed (we only track failures "in a row")
      if (listener.failures > 0) {
        await listenerDAO.updateListenerFailures(listener._id, 0);
      }
    } catch (error) {
      // Increment failure count (if 3+, the listener will be deleted)
      await listenerDAO.updateListenerFailures(listener._id, listener.failures + 1);
    }
  });
};

module.exports = {
  notifySlackListeners,
};
