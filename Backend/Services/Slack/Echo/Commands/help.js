const { WebClient } = require('@slack/web-api');

// Handler for the Help shortcut
exports.help = async (payload, token) => {
  const triggerId = payload.trigger_id;
  const slackClient = new WebClient(token);

  // List of commands that will be displayed
  const commands = [
    { command: 'Add a Listener', description: 'Allows you to create a club listener in a specified channel.' },
    { command: 'Remove a Listener', description: 'Allows you to removes a club listener from a specified channel.' },
    { command: 'View All Listeners', description: 'Displays all club listeners in the current workspace.' },
  ];

  // Constructs the blocks for the modal
  const blocks = commands.map((command, _) => ({
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: `*${command.command}*\n${command.description}`,
    },
  }));

  // Opens the modal with the constructed blocks
  (async () => {
    await slackClient.views.open({
      trigger_id: triggerId,
      view: {
        type: 'modal',
        title: {
          type: 'plain_text',
          text: 'Help Center',
        },
        callback_id: 'help',
        blocks,
      },
    });
  })();
};
