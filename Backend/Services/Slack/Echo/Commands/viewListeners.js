const { WebClient } = require('@slack/web-api');
const listenerDAO = require('../../../../DAO/ListenerDAO');

const defaultClubImage = 'http://clubfinity.assets.ufsec.org/default_club_image';

// Handler for the View Listeners shortcut
exports.viewListeners = async (payload, token) => {
  const triggerId = payload.trigger_id;
  const slackClient = new WebClient(token); // Connects to Slack with the token for the workspace

  const listeners = await listenerDAO.getServerListeners('slack', payload.team.id); // Gets all Slack listeners for the workspace

  let blocks;
  // If there are no listeners, constructs a message saying so
  if (listeners.length === 0) {
    blocks = [{
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: 'There are currently no listeners for this workspace.',
      },
    }];
  } else { // Otherwise, construct blocks for the modal with each listener as a section
    blocks = await Promise.all(listeners.map(async (listener, index) => {
      // Gets channel name from channel ID
      const res = await slackClient.conversations.info({ channel: listener.channelId });
      const channelName = res.channel.name;
      const tagChannel = listener.roles && listener.roles.length > 0 ? 'Yes' : 'No'; // If there are roles, indicates that the channel is tagged

      return {
        type: 'section',
        text: {
          type: 'mrkdwn',
          text: `*Listener ${index + 1}*\nClub: ${listener.club.name}\nChannel: #${channelName}\nTag channel for each message: ${tagChannel}`,
        },
        accessory: {
          type: 'image',
          image_url: listener.club.thumbnailUrl || defaultClubImage,
          alt_text: 'Icon',
        },
      };
    }));
  }

  // Opens the modal with the constructed blocks
  (async () => {
    await slackClient.views.open({
      trigger_id: triggerId,
      view: {
        type: 'modal',
        title: {
          type: 'plain_text',
          text: 'View Listeners',
        },
        callback_id: 'view-listeners',
        blocks,
      },
    });
  })();
};
