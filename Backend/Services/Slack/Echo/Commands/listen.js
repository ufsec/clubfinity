const { WebClient } = require('@slack/web-api');
const clubDAO = require('../../../../DAO/ClubDAO');
const listenerDAO = require('../../../../DAO/ListenerDAO');
const IntegrationTokenDAO = require('../../../../DAO/IntegrationTokenDAO');

const botAvatar = 'http://clubfinity.assets.ufsec.org/clubfinity_logo';

// Handler for the Add Listener shortcut
exports.listen = async (payload, token) => {
  const triggerId = payload.trigger_id;
  const slackClient = new WebClient(token); // Connects to Slack with the token for the workspace
  (async () => {
    await slackClient.views.open({
      trigger_id: triggerId,
      view: {
        type: 'modal',
        title: {
          type: 'plain_text',
          text: 'Clubfinity Listener',
        },
        callback_id: 'add-listener',
        submit: {
          type: 'plain_text',
          text: 'Submit',
        },
        blocks: [
          {
            type: 'input',
            block_id: 'choose-club',
            label: {
              type: 'plain_text',
              text: 'Choose a Club',
            },
            element: {
              type: 'external_select',
              min_query_length: 2,
              action_id: 'choose-club-action',
            },
          },
          {
            type: 'input',
            block_id: 'choose-channel',
            label: {
              type: 'plain_text',
              text: 'Choose a Channel',
            },
            element: {
              type: 'conversations_select',
              action_id: 'choose-channel-action',
              filter: {
                include: ['public'],
                exclude_external_shared_channels: true,
                exclude_bot_users: true,
              },
            },
          },
          {
            type: 'input',
            block_id: 'tag-channel',
            label: {
              type: 'plain_text',
              text: 'Choose Alerts',
            },
            element: {
              type: 'radio_buttons',
              action_id: 'tag-channel-action',
              options: [
                {
                  text: {
                    type: 'plain_text',
                    text: 'Tag @channel for each message.',
                  },
                  value: 'tag',
                },
                {
                  text: {
                    type: 'plain_text',
                    text: 'Do not tag @channel',
                  },
                  value: 'do_not_tag',
                },
              ],
            },
          },
        ],
      },
    });
  })();
};

// Returns list of clubs to be displayed in search box
exports.chooseClubs = async (payload) => {
  const typedText = payload.value;
  const matchingClubs = await clubDAO.search(typedText);

  // Constructs the options for the search box
  const options = matchingClubs.map((club) => ({
    text: {
      type: 'plain_text',
      text: club.name,
    },
    value: club.name,
  }));

  return options;
};

// Handler for the Add Listener modal submission
exports.viewSubmission = async (payload) => {
  const selectedClub = payload.view.state.values['choose-club']['choose-club-action'].selected_option.value;
  const workspaceId = payload.team.id;
  const channelId = payload.view.state.values['choose-channel']['choose-channel-action'].selected_conversation;
  const tagChannel = payload.view.state.values['tag-channel']['tag-channel-action'].selected_option.value === 'tag';

  // Sends requerst to create new Slack listener in the database
  const listener = await listenerDAO.addListener(selectedClub, 'slack', workspaceId, channelId, tagChannel ? ['channel'] : []);
  if (!listener) { return null; } // If response is null, there was an error adding the listener

  // Send a message to the channel confirming that the listener has been added or updated
  const botToken = await IntegrationTokenDAO.getIntegrationToken(workspaceId);
  const slackClient = new WebClient(botToken);
  const club = await clubDAO.getByName(selectedClub);

  let message = '';
  if (listener.isNew) {
    message = `A new listener has been added to this channel by Clubfinity for *${selectedClub}*.`;
  } else {
    message = `The listener for *${selectedClub}* has been updated by Clubfinity.`;
  }

  await slackClient.chat.postMessage({
    channel: channelId,
    text: `${message} The listener is set to ${tagChannel ? 'tag @channel' : 'not tag @channel'} for each message.`,
    blocks: [
      {
        type: 'section',
        text: {
          type: 'mrkdwn',
          text: `${message} The listener will ${tagChannel ? 'tag the channel' : '*not* tag the channel'} for each message.`,
        },
        accessory: {
          type: 'image',
          image_url: club.thumbnailUrl || botAvatar,
          alt_text: 'Icon',
        },
      },
      {
        type: 'context',
        elements: [
          {
            type: 'image',
            image_url: botAvatar,
            alt_text: 'Clubfinity Logo',
          },
          {
            type: 'mrkdwn',
            text: 'Powered by Clubfinity',
          },
        ],
      },
    ],
  });

  // Exits the modal
  return {
    response_action: 'clear',
  };
};
