const { WebClient } = require('@slack/web-api');
const listenerDAO = require('../../../../DAO/ListenerDAO');
const clubDAO = require('../../../../DAO/ClubDAO');
const IntegrationTokenDAO = require('../../../../DAO/IntegrationTokenDAO');

const botAvatar = 'http://clubfinity.assets.ufsec.org/clubfinity_logo';

// Handler for the Remove Listener shortcut
exports.remove = async (payload, token) => {
  const triggerId = payload.trigger_id;
  const slackClient = new WebClient(token); // Connects to Slack with the token for the workspace
  const listeners = await listenerDAO.getServerListeners('slack', payload.team.id); // Gets all Slack listeners for the workspace

  let blocks = [];

  // Constructs the blocks for the modal with each listener as an option
  if (listeners.length > 0) {
    const options = await Promise.all(listeners.map(async (listener) => {
      // Gets channel name from channel ID
      const res = await slackClient.conversations.info({ channel: listener.channelId });
      const channelName = res.channel.name;
      return {
        text: {
          type: 'plain_text',
          text: `${listener.club.name} - #${channelName}`, // Club name and channel name separated by a dash
        },
        value: listener.id,
      };
    }));

    blocks = [
      {
        type: 'input',
        block_id: 'remove-listeners',
        label: {
          type: 'plain_text',
          text: 'Select Listener(s)',
        },
        element: {
          type: 'checkboxes',
          action_id: 'remove-listeners-action',
          options,
        },
      },
    ];
  } else {
    blocks = [
      {
        type: 'section',
        text: {
          type: 'mrkdwn',
          text: 'There are currently no listeners for this workspace.',
        },
      },
    ];
  }

  // Opens the modal with the constructed blocks
  (async () => {
    const view = {
      type: 'modal',
      title: {
        type: 'plain_text',
        text: 'Remove Listener',
      },
      callback_id: 'remove-listener',
      blocks,
    };

    if (listeners.length > 0) {
      view.submit = {
        type: 'plain_text',
        text: 'Submit',
      };
    }

    await slackClient.views.open({
      trigger_id: triggerId,
      view,
    });
  })();
};

// Handler for the Remove Listener modal submission
exports.viewSubmission = async (payload) => {
  const selectedListeners = payload.view.state.values['remove-listeners']['remove-listeners-action'].selected_options;

  // Removes each selected listener from the database
  selectedListeners.forEach(async (listener) => {
    // Removes listener by ID and stores listener object
    const listenerObj = await listenerDAO.removeListenerById(listener.value);
    if (!listenerObj) { return; } // If response is null, there was an error removing the listener

    const botToken = await IntegrationTokenDAO.getIntegrationToken(listenerObj.platformId);
    const clubObj = await clubDAO.get(listenerObj.club);
    const slackClient = new WebClient(botToken);

    // Sends a message to the channel confirming that the listener has been removed
    await slackClient.chat.postMessage({
      channel: listenerObj.channelId,
      text: `The listener for ${clubObj.name} has been removed from this channel by Clubfinity.`,
      blocks: [
        {
          type: 'section',
          text: {
            type: 'mrkdwn',
            text: `The listener for ${clubObj.name} has been removed from this channel by Clubfinity.`,
          },
          accessory: {
            type: 'image',
            image_url: clubObj.thumbnailUrl || botAvatar,
            alt_text: 'Icon',
          },
        },
        {
          type: 'context',
          elements: [
            {
              type: 'image',
              image_url: botAvatar,
              alt_text: 'Clubfinity Logo',
            },
            {
              type: 'mrkdwn',
              text: 'Powered by Clubfinity',
            },
          ],
        },
      ],
    });
  });

  // Exits the modal
  return {
    response_action: 'clear',
  };
};
