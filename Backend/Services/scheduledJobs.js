const { CalendarScrapperService } = require('./googleCalendarService');
const clubDAO = require('../DAO/ClubDAO');

exports.scheduledJobs = async () => {
  console.log('running every minute');
  this.updateGoogleCalendars();
};

exports.updateGoogleCalendars = async () => {
  const clubs = await clubDAO.getAll();
  clubs.map((club) => {
    if (club.googleCalendarId) {
      console.log(`updating from ${club.name}'s google calendar`);
      return CalendarScrapperService(club._id, club.googleCalendarId);
    }
    return false;
  });
};
