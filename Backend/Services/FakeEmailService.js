exports.FakeEmailService = class {
  // eslint-disable-next-line class-methods-use-this
  send(_toAddress, _subject, _body) {
    console.warn('EmailService.send was called, but no emails were sent due to lack of configuration');
    return Promise.resolve();
  }
};
