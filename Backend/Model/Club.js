const mongoose = require('mongoose');

/*
This schema represents a club.
Each club has a name, admins, social links, description, category, thumbnail, Google calendar id,
and tags.

The admins and tags fields are lists of users and strings respectively.
The admins field is a reference to User schemas, indicating which users are admins of the
given club.
*/

const Schema = new mongoose.Schema({
  name: String,
  isPrivate: Boolean,
  admins: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  }],
  facebookLink: String,
  instagramLink: String,
  slackLink: String,
  description: String,
  category: String,
  thumbnailUrl: String,
  googleCalendarId: String,
  tags: [{
    type: String,
  }],
  socialLinks: [{
    link: String,
    social: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Social',
    },
  }],
});

exports.Model = mongoose.model('Club', Schema);
