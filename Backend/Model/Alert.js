const { DateTime } = require('luxon');
const mongoose = require('mongoose');

const Schema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  body: {
    type: String,
    required: true,
  },
  dateToExpire: {
    type: Date,
    required: true,
    set: (dt) => {
      if (dt instanceof DateTime) {
        return dt.toJSDate();
      }
      return new Date(dt);
    },
    get: (d) => DateTime.fromJSDate(d),
  },
  type: {
    type: String,
    // info -> for general information
    // warning -> for warnings
    // urgent -> for critical alerts requiring immediate attention
    enum: ['info', 'warning', 'urgent'],
    required: true,
  },
});

exports.Model = mongoose.model('Alert', Schema);
