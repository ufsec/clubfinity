const { DateTime } = require('luxon');
const mongoose = require('mongoose');

/*
This schema represents an event made by a club.
Each event has a name, location, social links, description, image, date, going users,
interested users, uninterested users, and a club.

Going users, interested users and uninterested users are all lists of users.
They also are a reference to User schemas, indicating which users are going,
interested, or uninterested.

The club field is a reference to the Club schema, indicating which club is making the event.
*/

const Schema = new mongoose.Schema({
  name: String,
  location: {
    placeId: String,
    description: String,
    room: String,
    googleMapsUrl: String,
  },
  links: [{
    name: String,
    link: String,
  }],
  description: String,
  image: String,
  date: {
    type: Date,
    set: (dt) => {
      if (dt instanceof DateTime) {
        return dt.toJSDate();
      }

      return new Date(dt);
    },
    get: (d) => DateTime.fromJSDate(d),
  },
  endTime: {
    type: Date,
    set: (dt) => {
      if (dt instanceof DateTime) {
        return dt.toJSDate();
      }

      return new Date(dt);
    },
    get: (d) => DateTime.fromJSDate(d),
  },
  goingUsers: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  }],
  uninterestedUsers: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  }],
  interestedUsers: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  }],
  club: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Club',
  },
});

exports.Model = mongoose.model('Event', Schema);
