const mongoose = require('mongoose');
const { DateTime } = require('luxon');

/*
This schema represents a ticket which a user submits to report a bug or submit a club.
Each ticket has a user, type, timestamp, club data OR bug data.

The user field is a reference to the User schema, indicating which user created the ticket.

The type can be either a bug report or a club submission.

If it is a club submission, the new club's information is required.
If it is a bug, then bug data is required.
*/

const Schema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  type: {
    type: String,
    enum: ['bug', 'club-submission'],
    required: true,
  },
  timestamp: {
    type: Date,
    set: (dt) => {
      if (dt instanceof DateTime) {
        return dt.toJSDate();
      }

      return new Date(dt);
    },
    get: (d) => DateTime.fromJSDate(d),
    required: true,
  },
  clubData: {
    type: new mongoose.Schema(
      {
        name: String,
        facebookLink: String,
        instagramLink: String,
        slackLink: String,
        googleCalLink: String,
        description: String,
        category: String,
        thumbnailUrl: String,
        isPrivate: Boolean,
        tags: [{
          type: String,
        }],
      },
      { _id: false },
    ),
    required() { return this.type === 'club-submission'; },
  },
  bugData: {
    type: String,
    required() { return this.type === 'bug'; },
  },
});
exports.Model = mongoose.model('Ticket', Schema);
