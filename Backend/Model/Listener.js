const mongoose = require('mongoose');

/*
This schema represents a listener for the slack and discord integrations.
It listens for a specified club's announcement & events, redirecting them to the chosen
channels in discord or slack.

Each listener has a type, club, serverId, channelId, roles (not required), and failures.

The club field is a reference to the Club schema, indicating which club it is listening to.

The failures field counts the number of failed redirects to discord or slack. Once it reaches
three consecutive failures, the listener is deleted (resets to 0 on success).
This is designed for scenarios such as when the chosen channel is deleted.
*/

const Schema = new mongoose.Schema({
  type: {
    type: String,
    required: true,
  },
  club: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Club',
    required: true,
  },
  platformId: {
    type: String,
    required: true,
  },
  channelId: {
    type: String,
    required: true,
  },
  roles: [{
    type: String,
    required: false,
  }],
  failures: {
    type: Number,
    required: true,
    default: 0,
  },
});

exports.Model = mongoose.model('Listener', Schema);
