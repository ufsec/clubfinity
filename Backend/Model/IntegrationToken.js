const mongoose = require('mongoose');

const Schema = new mongoose.Schema({
  platformId: {
    type: String,
    required: true,
  },
  token: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: true,
  },
});

exports.Model = mongoose.model('IntegrationToken', Schema);
