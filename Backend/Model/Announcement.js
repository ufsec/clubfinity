const { DateTime } = require('luxon');
const mongoose = require('mongoose');

/*
This schema represents an announcement made by a club.
Each announcement has a title, description, and date.

The club field is a reference to the Club schema, indicating which club made the announcement.
*/

const Schema = new mongoose.Schema({
  title: String,
  description: String,
  date: {
    type: Date,
    set: (dt) => {
      if (dt instanceof DateTime) {
        return dt.toJSDate();
      }

      return new Date(dt);
    },
    get: (d) => DateTime.fromJSDate(d),
  },
  club: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Club',
  },
});

exports.Model = mongoose.model('Announcement', Schema);
