const { DateTime } = require('luxon');
const mongoose = require('mongoose');

/*
This schema represents a verification code sent to a new user (via email).
Each new verification code has a user to which the email is sent (user id), the code itself,
and expiration time.

The user field is a reference to the User schema, indicating which user is being sent the
verification code.
*/

const Schema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  code: String,
  expirationTimestamp: {
    type: Date,
    set: (dt) => {
      if (dt instanceof DateTime) {
        return dt.toJSDate();
      }

      return new Date(dt);
    },
    get: (d) => DateTime.fromJSDate(d),
  },
});

exports.Model = mongoose.model('EmailVerificationCode', Schema);
