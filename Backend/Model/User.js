const mongoose = require('mongoose');

/*
This schema represents a user.
Each user has a name, major, year, profileImage, email, push token, password, clubs, settings,
and active indicator.

The clubs field is a list of clubs and a reference to Club schemas, indicating which clubs
the user is in.

Settings include options for announcement, event, and event reminder notifications.
Announcement notifications and Event notifications can be either enabled or disabled.
Event reminder notifications is a number, representing the desired time at which to notify
before an event.
*/

const Schema = new mongoose.Schema({
  name: { first: String, last: String },
  major: String,
  year: Number,
  profileImage: String,
  email: String,
  pushToken: String,
  password: { hash: String, salt: String },
  clubs: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Club',
  }],
  settings: {
    announcementNotifications: {
      type: String,
      enum: ['enabled', 'disabled'],
      default: 'enabled',
    },
    eventNotifications: {
      type: String,
      enum: ['enabled', 'disabled'],
      default: 'enabled',
    },
    eventReminderNotifications: {
      type: String,
      enum: ['never', '24', '12', '6', '3', '1'],
      default: '1',
    },
  },
  active: { type: Boolean, default: false },
});

exports.Model = mongoose.model('User', Schema);
