const { validationResult, body, param } = require('express-validator');
const { DateTime } = require('luxon');
const eventDAO = require('../DAO/EventDAO');
const clubDAO = require('../DAO/ClubDAO');
const userDAO = require('../DAO/UserDAO');
const {
  ValidationError, ForbiddenError, BadRequestError, NotFoundError,
} = require('../util/errors/errors');
const { catchErrors } = require('../util/httpUtil');
const { sendNotifications } = require('../util/notificationUtil');
const { notifySlackListeners } = require('../Services/Slack/Echo/notifyListeners');
const { notifyListeners } = require('../Services/Discord/Echo/notifyListeners');
const config = require('../Config/config');

const validateEventData = (req) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) throw ValidationError(errors.array());
};

const getInMonth = async (date, user, filter) => {
  const searchDate = DateTime.fromISO(date);
  switch (filter) {
    case undefined:
    case 'all':
    case '':
      return await eventDAO.getAllEventsInMonth(searchDate);
    case 'following':
      return await eventDAO.getEventsFromFollowedClubsInMonth(searchDate, user);
    case 'going':
      return await eventDAO.getGoingEventsInMonth(searchDate, user);
    default:
      throw BadRequestError(`Invalid filter type ${filter}`);
  }
};

exports.getMultiple = async (req, res) => catchErrors(res, async () => {
  const {
    filterBy, page, pageSize, eventType,
  } = req.query;
  switch (filterBy) {
    case 'userId': {
      const user = await userDAO.get(req.userId);
      return eventDAO.getByClubs(user.clubs, page, pageSize, eventType);
    }
    case 'month': {
      const user = await userDAO.get(req.userId);
      const { date, filter } = req.query;
      return getInMonth(date, user, filter);
    }
    case 'club': {
      const { clubId } = req.query;
      return eventDAO.getByClubs(clubId, page, pageSize, eventType);
    }
    case 'search': {
      const { input } = req.query;
      return eventDAO.search(input);
    }
    case 'all': {
      return eventDAO.getAll();
    }
    default:
      throw BadRequestError(`Invalid filter type ${filterBy}`);
  }
});

exports.get = async (req, res) => catchErrors(res, async () => {
  const { id } = req.params;
  return await eventDAO.get(id);
});

exports.update = async (req, res) => catchErrors(res, async () => {
  validateEventData(req);

  if (!(await eventDAO.exists(req.params.id))) {
    throw NotFoundError('Event not found');
  }

  const event = await eventDAO.get(req.params.id);
  if (!(await clubDAO.isAdmin(req.userId, event.club))) {
    throw ForbiddenError('Only admins of this club can update the event.');
  }

  return eventDAO.update(req.params.id, req.body);
});

exports.create = async (req, res) => catchErrors(res, async () => {
  validateEventData(req);
  if (!(await clubDAO.exists(req.body.club))) {
    throw NotFoundError('Club not found');
  }

  if (!(await clubDAO.isAdmin(req.userId, req.body.club))) {
    throw ForbiddenError('Only admins of this club can create an event');
  }

  req.body.date = DateTime.fromISO(req.body.date);
  req.body.endTime = DateTime.fromISO(req.body.endTime);

  const newEvent = await eventDAO.create(req.body);
  if (newEvent) {
    sendNotifications(req.body.club, req.body.name);
    notifySlackListeners(newEvent, 'event');
    notifyListeners(newEvent, 'event');
  }
  return newEvent;
});

exports.updateGoingUsers = async (req, res) => catchErrors(res, async () => {
  validateEventData(req);

  const { op } = req.query;
  switch (op) {
    case 'add':
      eventDAO.update(req.params.id, {
        $pull: { interestedUsers: req.userId },
      });
      eventDAO.update(req.params.id, {
        $pull: { uninterestedUsers: req.userId },
      });
      await eventDAO.update(req.params.id, {
        $addToSet: { goingUsers: req.userId },
      });
      break;
    case 'remove':
      await eventDAO.update(req.params.id, {
        $pull: { goingUsers: req.userId },
      });
      break;
    default:
      throw BadRequestError(`Invalid query parameter ${op}`);
  }
});

exports.updateInterestedUsers = async (req, res) => catchErrors(res, async () => {
  validateEventData(req);

  const { op } = req.query;
  switch (op) {
    case 'add':
      eventDAO.update(req.params.id, {
        $pull: { uninterestedUsers: req.userId },
      });
      eventDAO.update(req.params.id, { $pull: { goingUsers: req.userId } });
      await eventDAO.update(req.params.id, {
        $addToSet: { interestedUsers: req.userId },
      });
      break;
    case 'remove':
      await eventDAO.update(req.params.id, {
        $pull: { interestedUsers: req.userId },
      });
      break;
    default:
      throw BadRequestError(`Invalid query parameter ${op}`);
  }
});

exports.updateUninterestedUsers = async (req, res) => catchErrors(res, async () => {
  validateEventData(req);

  const { op } = req.query;
  switch (op) {
    case 'add':
      eventDAO.update(req.params.id, {
        $pull: { interestedUsers: req.userId },
      });
      eventDAO.update(req.params.id, { $pull: { goingUsers: req.userId } });
      await eventDAO.update(req.params.id, {
        $addToSet: { uninterestedUsers: req.userId },
      });
      break;
    case 'remove':
      await eventDAO.update(req.params.id, {
        $pull: { uninterestedUsers: req.userId },
      });
      break;
    default:
      throw BadRequestError(`Invalid query parameter ${op}`);
  }
});

exports.delete = async (req, res) => catchErrors(res, async () => {
  validateEventData(req);

  if (!(await eventDAO.exists(req.params.id))) {
    throw NotFoundError('Event not found');
  }

  const event = await eventDAO.get(req.params.id);
  if (!(await clubDAO.isAdmin(req.userId, event.club))) {
    throw ForbiddenError('Only admins of this club can delete an event');
  }

  return eventDAO.delete(req.params.id);
});

exports.retrieveGoogleApiKey = async (req, res) => catchErrors(res, async () => (
  { googleApiKey: config.googleCalAPIKEY }
));

async function validateEvent(id) {
  try {
    await eventDAO.get(id);
  } catch (error) {
    throw NotFoundError('Event not found');
  }
}

async function validateEndTime(endTime, date) {
  if (DateTime.fromISO(endTime) < DateTime.fromISO(date)) {
    throw ValidationError('End time must be after start date.');
  }
}

exports.validate = (type) => {
  switch (type) {
    case 'validateEventInfo': {
      return [
        body('name', 'Event name does not exist').exists(),
        body('location', 'Location does not exist').exists(),
        body('description', 'Description does not exist').exists(),
        body('date', 'Date does not exist').exists(),
        body('endTime', 'End time does not exist or invalid')
          .exists()
          .custom((endTime, { req }) => validateEndTime(endTime, req.body.date)),
        body('club', 'Club id does not exist').exists(),
      ];
    }
    case 'validateUpdateEventInfo': {
      return [
        body('name', 'Event name does not exist').exists(),
        body('location', 'Location does not exist').exists(),
        body('description', 'Description does not exist').exists(),
        body('date', 'Date does not exist').exists(),
        body('endTime', 'End time does not exist or invalid')
          .exists()
          .custom((endTime, { req }) => validateEndTime(endTime, req.body.date)),
      ];
    }
    case 'validateExistingEvent': {
      return [
        param('id', 'Event id does not exist or invalid')
          .exists()
          .custom((date) => validateEvent(date)),
      ];
    }
    default: {
      throw BadRequestError(`Invalid validator type ${type}`);
    }
  }
};
