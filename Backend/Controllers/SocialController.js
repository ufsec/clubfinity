const socialDAO = require('../DAO/SocialDAO');
const { catchErrors } = require('../util/httpUtil');

exports.get = async (req, res) => catchErrors(res, async () => socialDAO.get(req.params.id));
exports.getAll = async (req, res) => catchErrors(res, async () => socialDAO.getAll());
