const { validationResult, body, param } = require('express-validator');
const { DateTime } = require('luxon');
const { catchErrors } = require('../util/httpUtil');
const {
  ValidationError, BadRequestError, ForbiddenError, NotFoundError,
} = require('../util/errors/errors');
const announcementDAO = require('../DAO/AnnouncementDAO');
const clubDAO = require('../DAO/ClubDAO');
const { sendNotifications } = require('../util/notificationUtil');
const { notifySlackListeners } = require('../Services/Slack/Echo/notifyListeners');
const { notifyListeners } = require('../Services/Discord/Echo/notifyListeners');

const validateAnnouncementData = (req) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) throw ValidationError(errors.array());
};

exports.get = async (req, res) => catchErrors(res, async () => {
  validateAnnouncementData(req);

  return announcementDAO.get(req.params.id);
});

exports.getMultiple = async (req, res) => catchErrors(res, async () => {
  const { filterBy } = req.query;
  if (filterBy === 'club') {
    const { clubId, page, pageSize } = req.query;
    return announcementDAO.getByClubs([clubId], page, pageSize);
  }
  throw BadRequestError(`Invalid query parameter ${filterBy}`);
});

exports.create = async (req, res) => catchErrors(res, async () => {
  validateAnnouncementData(req);

  if (!(await clubDAO.exists(req.body.club))) {
    throw NotFoundError('Club not found');
  }
  if (!(await clubDAO.isAdmin(req.userId, req.body.club))) {
    throw ForbiddenError('Only admins of this club can create an announcement');
  }
  req.body.date = DateTime.fromISO(req.body.date);
  const newAnnouncement = await announcementDAO.create(req.body);
  if (newAnnouncement) {
    sendNotifications(req.body.club, req.body.title);
    notifySlackListeners(newAnnouncement, 'announcement');
    notifyListeners(newAnnouncement, 'announcement');
  }
  return newAnnouncement;
});

exports.update = async (req, res) => catchErrors(res, async () => {
  validateAnnouncementData(req);

  if (!(await announcementDAO.exists(req.params.id))) {
    throw NotFoundError('Announcement not found');
  }

  const announcement = await announcementDAO.get(req.params.id);
  if (!(await clubDAO.isAdmin(req.userId, announcement.club))) {
    throw ForbiddenError('Only admins of this club can edit an announcement');
  }

  req.body.date = DateTime.fromISO(req.body.date);

  return announcementDAO.update(req.params.id, req.body);
});

exports.delete = async (req, res) => catchErrors(res, async () => {
  validateAnnouncementData(req);

  if (!(await announcementDAO.exists(req.params.id))) {
    throw NotFoundError('Announcement not found');
  }

  const announcement = await announcementDAO.get(req.params.id);
  if (!(await clubDAO.isAdmin(req.userId, announcement.club))) {
    throw ForbiddenError('Only admins of this club can delete an announcement');
  }

  return announcementDAO.delete(req.params.id);
});

exports.validate = (type) => {
  switch (type) {
    case 'announcementBody': {
      return [
        body('title', 'title does not exist').exists(),
        body('description', 'description does not exist').exists(),
        body('date', 'date does not exist').exists(),
        body('club', 'club id does not exist').exists(),
      ];
    }
    case 'announcementUpdate': {
      return [
        body('title', 'title does not exist').exists(),
        body('description', 'description does not exist').exists(),
      ];
    }
    case 'idParam': {
      return [param('id', 'id not given').exists()];
    }
    case 'clubIdParam': {
      return [param('clubId', 'club id not given').exists()];
    }
    default: {
      throw BadRequestError(`Invalid validator type ${type}`);
    }
  }
};
