const { validationResult, body } = require('express-validator');
const alertDAO = require('../DAO/AlertDAO');
const {
  ValidationError,
  BadRequestError,
} = require('../util/errors/errors');
const { catchErrors } = require('../util/httpUtil');

const validateAlertData = (req) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) throw ValidationError(errors.array());
};

exports.create = async (req, res) => {
  catchErrors(res, async () => {
    validateAlertData(req);
    return await alertDAO.create(req.body);
  });
};

exports.getActive = async (req, res) => {
  catchErrors(res, async () => await alertDAO.getActiveAlerts());
};

exports.get = async (req, res) => {
  catchErrors(res, async () => await alertDAO.get(req.params.id));
};

exports.update = async (req, res) => {
  catchErrors(res, async () => {
    validateAlertData(req);
    return await alertDAO.update(req.params.id, req.body);
  });
};

exports.delete = async (req, res) => {
  catchErrors(res, async () => await alertDAO.delete(req.params.id));
};

exports.validate = (type) => {
  switch (type) {
    case 'validateAlertInfo': {
      return [
        body('title', 'Title does not exist').exists(),
        body('body', 'Body does not exist').exists(),
        body('dateToExpire', 'Date to expire does not exist').exists(),
        body('type', 'Type does not exist')
          .exists()
          .isIn(['info', 'warning', 'urgent']),
      ];
    }
    default: {
      throw BadRequestError(`Invalid validator type ${type}`);
    }
  }
};
