const axios = require('axios');
const config = require('../Config/config');
const SlackListen = require('../Services/Slack/Echo/Commands/listen');
const SlackViewListeners = require('../Services/Slack/Echo/Commands/viewListeners');
const SlackRemoveListeners = require('../Services/Slack/Echo/Commands/removeListeners');
const SlackHelp = require('../Services/Slack/Echo/Commands/help');
const IntegrationTokenDAO = require('../DAO/IntegrationTokenDAO');

// Handler for each Slack Shortcut command
exports.slack = async (req, res) => {
  const payload = JSON.parse(req.body.payload);
  // Gets token for workspace sending the request
  const token = await IntegrationTokenDAO.getIntegrationToken(payload.team.id);
  if (token === null) return res.status(200).send();

  // Switch case for each command type, which calls the appropriate function
  switch (payload.callback_id) {
    case 'listen':
      SlackListen.listen(payload, token);
      break;
    case 'remove':
      SlackRemoveListeners.remove(payload, token);
      break;
    case 'view_listeners':
      SlackViewListeners.viewListeners(payload, token);
      break;
    case 'help':
      SlackHelp.help(payload, token);
      break;
    default:
      // If the callback_id is not recognized, we verify it is
      // a submission recevied from a modal then call the appropriate function
      if (payload.type === 'view_submission') {
        if (payload.view.callback_id === 'add-listener') {
          const result = await SlackListen.viewSubmission(payload);
          res.json(result);
        } else if (payload.view.callback_id === 'remove-listener') {
          const result = await SlackRemoveListeners.viewSubmission(payload);
          res.json(result);
        }
      }
      break;
  }
  return res.status(200).send();
};

// Handler for each Slack Interactive Message
exports.select = async (req, res) => {
  const payload = JSON.parse(req.body.payload);

  // Loads the list of clubs for the user to choose from
  if (payload.action_id === 'choose-club-action') {
    const options = await SlackListen.chooseClubs(payload);
    return res.json({
      options,
    });
  }
  return res.status(200).send();
};

// Handler for OAuth flow when installing the app
exports.oauth = async (req, res) => {
  const { code } = req.query;

  if (!code) {
    return res.json({ message: 'Error occurred while installing the Clubfinity App. Please try again.' });
  }

  const { clientId, clientSecret } = config.slack.echo;
  if (!clientId || !clientSecret) {
    return res.status(400).json({ message: 'Missing Slack API keys.' });
  }

  try {
    // Sends request to Slack to get required access token
    const response = await axios.post('https://slack.com/api/oauth.v2.access', null, {
      params: {
        client_id: clientId,
        client_secret: clientSecret,
        code,
      },
    });

    if (!response.data.ok) {
      return res.json({ message: 'Error occurred while installing the Clubfinity App. Please try again.' });
    }

    // eslint-disable-next-line camelcase
    const { access_token, app_id } = response.data;
    const teamId = response.data.team.id;

    // Saves workspace token and checks for error
    const token = await IntegrationTokenDAO.addIntegrationToken(teamId, access_token, 'slack');
    if (token === null) throw Error('Error saving token to database');

    // eslint-disable-next-line camelcase
    return res.redirect(`https://slack.com/apps/${app_id}`); // Sends user to Clubfinity App page
  } catch (error) {
    return res.json({ message: 'Error occurred while installing the Clubfinity App. Please try again.' });
  }
};
