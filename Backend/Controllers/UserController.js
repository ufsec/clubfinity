const {
  validationResult,
  body,
  param,
  query,
} = require('express-validator');
const { DateTime } = require('luxon');
const userDAO = require('../DAO/UserDAO');
const clubDAO = require('../DAO/ClubDAO');
const emailVerificationCodeDAO = require('../DAO/EmailVerificationCodeDAO');
const { generateRandomCode } = require('../util/authUtil');
const {
  ValidationError, NotFoundError, UnauthorizedError, BadRequestError, ConflictError,
} = require('../util/errors/errors');
const { catchErrors } = require('../util/httpUtil');
const { getLimitedUserData } = require('../util/userUtil');
const {
  validateName,
  validatePassword,
  validateYear,
} = require('../util/Validations/Validations');

const validateData = (req) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) throw ValidationError(errors.array());
};

async function validateEmailIsUnique(email) {
  if (await userDAO.existsByEmail(email)) {
    return false;
  }

  return true;
}

async function sendEmailVerificationEmail(user, verificationCode) {
  await global.emailService.send(
    user.email,
    'Clubfinity Email Verification',
    `
Hello ${user.name.first} ${user.name.last},

Thanks for joining Clubfinity!

Here is your email verification code:

${verificationCode}
    `,
  );
}

async function sendEmailForgotPassword(user, verificationCode) {
  await global.emailService.send(
    user.email,
    'Clubfinity Forget Password',
    `
Hello ${user.name.first} ${user.name.last},

Here is your verification code:

${verificationCode}
    `,
  );
}

exports.forgotPasswordEmail = (req, res) => catchErrors(res, async () => {
  const { email } = req.body;
  const exists = await userDAO.existsByEmail(email);
  if (!exists) {
    return false;
  }

  const user = await userDAO.getByEmail(email);
  const code = generateRandomCode();
  await emailVerificationCodeDAO.delete(user._id);
  await emailVerificationCodeDAO.create({
    user: user._id,
    code,
    expirationTimestamp: DateTime.local().plus({ minutes: 15 }),
  });
  await sendEmailForgotPassword(user, code);
  return user;
});

exports.resetPassword = (req, res) => catchErrors(res, async () => {
  const { code, newPass, userId } = req.body;
  const databaseCodeRecord = await emailVerificationCodeDAO.get(userId);
  if (databaseCodeRecord.expirationTimestamp < DateTime.local()) {
    throw NotFoundError('Verification code expired');
  }
  if (code !== databaseCodeRecord.code) {
    throw UnauthorizedError('Invalid verification code');
  }
  await emailVerificationCodeDAO.delete(userId);
  const Newuser = await userDAO.changePassword(userId, newPass);
  return Newuser;
});

exports.register = (req, res) => catchErrors(res, async () => {
  const { email } = req.body;

  if (!await validateEmailIsUnique(email)) {
    throw ConflictError('Email already in use');
  }

  validateData(req);

  if (await userDAO.inactiveUserExistsByEmail(email)) {
    const user = await userDAO.getByEmail(email);

    await userDAO.delete(user._id);
    await emailVerificationCodeDAO.delete(user._id);
  }

  const user = await userDAO.create(req.body);

  const code = generateRandomCode();
  await emailVerificationCodeDAO.create({
    user: user._id,
    code,
    expirationTimestamp: DateTime.local().plus({ minutes: 15 }),
  });

  await sendEmailVerificationEmail(user, code);

  return getLimitedUserData(user);
});

exports.resendEmailVerificationCode = (req, res) => catchErrors(res, async () => {
  const { userId } = req.body;

  const user = await userDAO.get(userId);
  const emailVerificationCode = await emailVerificationCodeDAO.get(userId);

  await sendEmailVerificationEmail(user, emailVerificationCode.code);
});

exports.verifyEmailCode = (req, res) => catchErrors(res, async () => {
  const { code: codeAttempt, userId } = req.body;

  const databaseCodeRecord = await emailVerificationCodeDAO.get(userId);

  if (databaseCodeRecord.expirationTimestamp < DateTime.local()) {
    throw NotFoundError('Verification code expired');
  }

  if (codeAttempt !== databaseCodeRecord.code) {
    throw UnauthorizedError('Invalid verification code');
  }

  await emailVerificationCodeDAO.delete(userId);

  return await userDAO.update(userId, { active: true });
});

exports.get = async (req, res) => catchErrors(res, async () => {
  const user = await userDAO.get(req.userId);
  return getLimitedUserData(user);
});

exports.getByEmail = async (req, res) => catchErrors(res, async () => {
  const user = await userDAO.getByEmail(req.params.email);
  return getLimitedUserData(user);
});

exports.update = async (req, res) => catchErrors(res, async () => {
  validateData(req);

  const user = await userDAO.update(req.userId, req.body);
  return getLimitedUserData(user);
});

exports.updateUserSettings = async (req, res) => catchErrors(res, async () => {
  validateData(req);

  if (Object.keys(req.query).length === 0) {
    throw new ValidationError([{
      value: '',
      param: '',
      msg: 'No query parameters given',
      location: 'query',
    }]);
  }

  const userData = await userDAO.get(req.userId);

  userData.settings = { ...userData.settings, ...req.query };

  const user = await userDAO.update(req.userId, userData);
  return getLimitedUserData(user);
});

exports.updatePushToken = async (req, res) => catchErrors(res, async () => {
  validateData(req);

  await userDAO.update(req.userId, req.query);
});

exports.updateClubFollowingState = async (req, res) => catchErrors(res, async () => {
  validateData(req);

  const { id: clubId, userId } = req.params;
  const { follow } = req.query;
  const targetUserId = userId || req.userId;
  const user = await userDAO.get(targetUserId);
  switch (follow) {
    case 'true':
      if (!user.clubs.some((club) => club._id.toString() === clubId)) {
        user.clubs.push(clubId);
      }
      return getLimitedUserData(await userDAO.update(targetUserId, user));
    case 'false':
      user.clubs.forEach((club, index, clubs) => {
        if (club._id.toString() === clubId) clubs.splice(index, 1);
      });
      return getLimitedUserData(await userDAO.update(targetUserId, user));
    default:
      throw BadRequestError(`Invalid query parameter: ${follow}`);
  }
});

async function validateClubId(clubId) {
  const clubExists = await clubDAO.exists(clubId);
  if (!clubExists) {
    throw NotFoundError('Club not found');
  }
  return clubExists;
}

exports.validate = (type) => {
  const baseUserInfo = [
    body('name.first', 'First name does not exist')
      .exists()
      .custom(validateName),
    body('name.last', 'Last name does not exist').exists().custom(validateName),
    body('major', 'Major does not exist or is invalid').exists(),
    body('year', 'Year does not exist or is invalid')
      .exists()
      .custom((year) => validateYear(year)),
    // This is required to prevent users from modifying their own active status
    body('active', 'Cannot set active flag')
      .not()
      .exists(),
  ];
  switch (type) {
    case 'validateBaseUserInfo': {
      return baseUserInfo;
    }
    case 'validateFullUserInfo': {
      return [
        ...baseUserInfo,
        body('email', 'Email not given, invalid, or already exists')
          .exists()
          .isEmail()
          .contains('@ufl.edu'),
        body('password', 'Password not given')
          .exists()
          .custom((password) => validatePassword(password)),
      ];
    }
    case 'validateUserSettings': {
      return [
        query('eventNotifications', 'Invalid event notifications setting').optional().isIn(['enabled', 'disabled']),
        query('announcementNotifications', 'Invalid announcement notifications setting').optional().isIn(['enabled', 'disabled']),
        query('eventReminderNotifications', 'Invalid event reminder notifications setting').optional().isIn(['never', '24', '12', '6', '3', '1']),
      ];
    }
    case 'validatePushToken': {
      return [query('pushToken', 'push token is missing').exists()];
    }
    case 'validateClubId': {
      return [
        param('id', 'Club id missing')
          .exists()
          .custom(async (clubId) => {
            await validateClubId(clubId);
          }),
      ];
    }
    default: {
      throw BadRequestError(`Invalid validator type ${type}`);
    }
  }
};

exports.delete = (req, res) => {
  catchErrors(res, async () => {
    const { userId } = req.params;
    const deletedUser = await userDAO.delete(userId);
    const clubsUserIsAdmin = await clubDAO.getByAdminId(userId);

    if (clubsUserIsAdmin) {
      clubsUserIsAdmin.forEach(async (club) => {
        const adminIndex = club.admins.findIndex((admin) => admin._id.toString() === userId);
        if (adminIndex !== -1) {
          club.admins.splice(adminIndex, 1);
          await clubDAO.update(club._id, club);
        }
      });
    }

    return deletedUser;
  });
};
