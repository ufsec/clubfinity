require('dotenv').config();

const env = process.env.NODE_ENV || 'development';
const DEV_DB_PATH = process.env.DB_TEST_URL
  || 'mongodb+srv://clubfinity_developer:BM7AghucQCH2IWHP@cluster0.ezebi.mongodb.net/?retryWrites=true&w=majority';

const config = {
  development: {
    port: process.env.PORT || 8080,
    jwtSecret: '4686E7A784E4176F122F7F00D5742225421',
    alertAPIkey: '20c312e2-fbb0-4b71-897e-b8d23160d5d8',
    database: DEV_DB_PATH,
    users: {
      collection: 'users',
    },
  },
  test: {
    port: process.env.PORT || 8080,
    database: DEV_DB_PATH,
    jwtSecret: '4686E7A784E4176F122F7F00D5742225421',
    alertAPIkey: '20c312e2-fbb0-4b71-897e-b8d23160d5d8',
  },
  staging: {
    port: process.env.PORT || 443,
    jwtSecret: process.env.JWT_SECRET,
    database: process.env.DATABASE_URL,
    alertAPIkey: process.env.ALERT_API_KEY,
    users: {
      collection: 'users',
    },
    discord: {
      tickets: {
        botToken: process.env.DISCORD_TICKETS_TOKEN,
        bugChannelId: process.env.DISCORD_TICKETS_BUG_CHANNEL,
        clubChannelId: process.env.DISCORD_TICKETS_CLUB_CHANNEL,
      },
      echo: {
        botToken: process.env.DISCORD_ECHO_TOKEN,
        applicationId: process.env.DISCORD_ECHO_APP_ID,
      },
    },
    slack: {
      echo: {
        clientId: process.env.SLACK_CLIENT_ID,
        clientSecret: process.env.SLACK_CLIENT_SECRET,
      },
    },
    slackToken: process.env.SLACK_TOKEN,
    googleCalAPIKEY: process.env.GOOGLE_CAL_API_KEY,
    s3: {
      bucketName: process.env.STAGING_AWS_BUCKET_NAME,
      bucketRegion: process.env.STAGING_AWS_BUCKET_REGION,
      accessKey: process.env.STAGING_AWS_ACCESS_KEY,
      secretKey: process.env.STAGING_AWS_SECRET_ACCESS_KEY,
    },
  },
  production: {
    port: process.env.PORT || 443,
    jwtSecret: process.env.JWT_SECRET,
    database: process.env.DATABASE_URL,
    alertAPIkey: process.env.ALERT_API_KEY,
    users: {
      collection: 'users',
    },
    email: {
      domain: 'pvanderlaat.com',
      from: 'Clubfinity <secatuf@gmail.com>',
      apiKey: process.env.MAILGUN_API_KEY,
      publicKey: process.env.MAILGUN_PUBLIC_KEY,
    },
    discord: {
      tickets: {
        botToken: process.env.DISCORD_TICKETS_TOKEN,
        bugChannelId: process.env.DISCORD_TICKETS_BUG_CHANNEL,
        clubChannelId: process.env.DISCORD_TICKETS_CLUB_CHANNEL,
      },
      echo: {
        botToken: process.env.DISCORD_ECHO_TOKEN,
        applicationId: process.env.DISCORD_ECHO_APP_ID,
      },
    },
    slack: {
      echo: {
        clientId: process.env.SLACK_CLIENT_ID,
        clientSecret: process.env.SLACK_CLIENT_SECRET,
      },
    },
    slackToken: process.env.SLACK_TOKEN,
    googleCalAPIKEY: process.env.GOOGLE_CAL_API_KEY,
    s3: {
      bucketName: process.env.AWS_BUCKET_NAME,
      bucketRegion: process.env.AWS_BUCKET_REGION,
      accessKey: process.env.AWS_ACCESS_KEY,
      secretKey: process.env.AWS_SECRET_ACCESS_KEY,
    },
  },
  ci: {
    port: process.env.PORT || 8080,
    jwtSecret: 'testSecret',
    database: 'mongodb://mongo:27017/clubfinity',
  },
};

module.exports = config[env];
module.exports.ENV = env;
module.exports.DEV_DB_PATH = DEV_DB_PATH;
