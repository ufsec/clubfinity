const { HttpError } = require('./errors/httpError');
const { ValidationEntityError } = require('./errors/validationError');

function sendOkResponse(res, result) {
  res.send({ ok: true, data: result });
}

function sendErrorResponse(res, error) {
  if (error instanceof ValidationEntityError) {
    res.status(error.code).send({
      ok: false,
      error: error.message,
      validationErrors: error.validationErrors,
    });
  } else if (error instanceof HttpError) {
    res.status(error.code).send({ ok: false, error: error.message });
  } else {
    // If an error is thrown that is not a predefined http error with an error code,
    // then respond with it as an Internal Server Error (500)
    res.status(500).send({ ok: false, error: error.message });
  }
}

exports.catchErrors = async (res, f) => {
  try {
    const result = await f();
    sendOkResponse(res, result);
  } catch (error) {
    sendErrorResponse(res, error);
  }
};
