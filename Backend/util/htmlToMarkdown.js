const { NodeHtmlMarkdown } = require('node-html-markdown');

// Additional custom ranslator for <u> tags -> underlined elements (not present in base package)
const customTranslator = {
  u: {
    prefix: '__',
    postfix: '__',
    spaceIfRepeatingChar: true,
    postprocess: ({ cc }) => cc,
  },
};

// Util function to transform HTML to Markdown (used in Discord & Slack)
const htmlToMarkdownTranslator = new NodeHtmlMarkdown(
  /* options (optional) */
  {
    bulletMarker: '* ',
  },
  /* customTranslators (optional) */
  customTranslator,
  /* customCodeBlockTranslators (optional) */
  undefined,
);

module.exports = {
  htmlToMarkdownTranslator,
};
