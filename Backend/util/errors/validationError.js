const { HttpError } = require('./httpError');

exports.ValidationEntityError = class extends HttpError {
  constructor(message, code, validationErrors) {
    super(message, code);
    this.validationErrors = validationErrors;
  }
};
