const { HttpError } = require('./httpError');
const { ValidationEntityError } = require('./validationError');

// 4xx Error Codes
exports.BadRequestError = function BadRequestError(message = 'Bad request') {
  return new HttpError(message, 400);
};

exports.UnauthorizedError = function UnauthorizedError(
  message = 'User is unathorized',
) {
  return new HttpError(message, 401);
};

exports.ForbiddenError = function ForbiddenError(
  message = 'Requested resource is forbidden',
) {
  return new HttpError(message, 403);
};

exports.NotFoundError = function NotFoundError(message = 'Not found') {
  return new HttpError(message, 404);
};

exports.MethodNotAllowedError = function MethodNotAllowedError(
  message = 'Request method not allowed',
) {
  return new HttpError(message, 405);
};

exports.ConflictError = function ConflictError(
  message = 'Conflict with request occured',
) {
  return new HttpError(message, 409);
};

exports.ValidationError = function ValidationError(
  validationErrors = null,
  message = 'Request contains validation errors',
) {
  return new ValidationEntityError(message, 422, validationErrors);
};

// 5xx Error Codes
exports.InternalServerError = function InternalServerError(
  message = 'Internal server error',
) {
  return new HttpError(message, 500);
};
