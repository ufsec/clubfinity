import React, { useEffect, createContext, useState } from 'react';
import {
  View, StyleSheet, TouchableOpacity, Image, ActivityIndicator, BackHandler,
} from 'react-native';
import PropTypes from 'prop-types';
import { useVideoPlayer, VideoView } from 'expo-video';
import { useEventListener } from 'expo';
import { Ionicons } from '@expo/vector-icons';

const styles = StyleSheet.create({
  overlay: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(0, 0, 0, 0.97)',
    zIndex: 1,
  },
  video: {
    width: '100%',
    height: '100%',
    opacity: 1,
  },
  image: {
    width: '100%',
    height: '100%',
    opacity: 1,
  },
  closeButton: {
    position: 'absolute',
    top: '8%',
    left: '5%',
    zIndex: 2,
  },
});

export const FullscreenMediaContext = createContext();

export const FullscreenMediaProvider = ({ children }) => {
  const [media, setMedia] = useState(null);
  const [originalPlayer, setOriginalPlayer] = useState(null);
  const [mediaType, setMediaType] = useState(false);
  const [loadingMedia, setLoadingMedia] = useState(true);

  const closeMedia = () => {
    if (mediaType === 'video') {
      try { // prevents error if ExpandableVideo somehow closes
        if (originalPlayer && !originalPlayer._released) {
          originalPlayer.play();
        }
      } catch (error) {
        console.warn('Video player released');
      }
      setOriginalPlayer(null);
    }

    setMedia(null);
    setLoadingMedia(true);
  };

  useEffect(() => { // allows user to exit fullscreen via their phone's back button
    const backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      if (media) {
        closeMedia();
        return true;
      }
      return false;
    });

    return () => backHandler.remove();
  }, [media]);

  const fullscreenPlayer = useVideoPlayer(media || null, (player) => {
    if (player && mediaType === 'video') {
      player.loop = true;
      player.play();
    }
  });

  // detects when the video is ready to play
  useEventListener(fullscreenPlayer, 'statusChange', ({ status }) => {
    if (status === 'readyToPlay') {
      setLoadingMedia(false);
      if (!fullscreenPlayer.playing) {
        fullscreenPlayer.play();
      }
    } else {
      setLoadingMedia(true);
    }
  });

  const expandVideo = (video, player) => {
    player.pause();
    setOriginalPlayer(player);
    setMediaType('video');
    setMedia(video);
  };

  const expandImage = (image) => {
    setMediaType('image');
    setMedia(image);
  };

  return (
    <FullscreenMediaContext.Provider
      value={{
        expandVideo,
        expandImage,
      }}
    >
      <View style={{ flex: 1 }}>
        {children}
        {media && (
        <View style={styles.overlay}>
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            {mediaType === 'video' ? (
              <VideoView
                style={styles.video}
                player={fullscreenPlayer}
                resizeMode="contain"
                nativeControls={false}
              />
            ) : mediaType === 'image' ? (
              <Image
                src={media}
                style={styles.image}
                onLoad={() => setLoadingMedia(false)}
                resizeMode="contain"
              />
            ) : <></>}
            {loadingMedia && (
            <View style={{ position: 'absolute' }}>
              <ActivityIndicator color="white" />
            </View>
            )}
          </View>
          <TouchableOpacity
            style={styles.closeButton}
            onPress={closeMedia}
          >
            <Ionicons name="close" size={35} color="lightgray" />
          </TouchableOpacity>
        </View>
        )}
      </View>
    </FullscreenMediaContext.Provider>
  );
};

FullscreenMediaProvider.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]).isRequired,
};
