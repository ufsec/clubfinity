import * as MediaLibrary from 'expo-media-library';
import * as FileSystem from 'expo-file-system';

// request permissions
async function requestPermissions() {
  const { status } = await MediaLibrary.requestPermissionsAsync();
  if (status !== 'granted') {
    alert('Please give Clubfinity Camera Roll Permisions to Download.');
    return false;
  }
  return true;
}

// convert base64 qr Code data to png file
async function convertBase64ToFile(base64Data, fileName) {
  const filePath = `${FileSystem.documentDirectory}${fileName}.png`;
  const base64Image = base64Data.replace(/^data:image\/png;base64,/, '');

  await FileSystem.writeAsStringAsync(filePath, base64Image, {
    encoding: FileSystem.EncodingType.Base64,
  });

  return filePath;
}
// can create a new function if you already have png data, this is for QR code
export default async function saveImage(base64Data) {
  if (await requestPermissions()) {
    try {
      const fileUri = await convertBase64ToFile(base64Data, 'qr-code');

      const asset = await MediaLibrary.createAssetAsync(fileUri);

      const album = await MediaLibrary.getAlbumAsync('Clubfinity');
      if (!album) {
        await MediaLibrary.createAlbumAsync('Clubfinity', asset, false);
      } else {
        await MediaLibrary.addAssetsToAlbumAsync([asset], album, false);
      }
      alert('Photo saved to Photos!');
    } catch (error) {
      console.error('Error saving photo:', error);
      alert('We encountered an error saving your photo. Please try again later.');
    }
  }
}
