import Constants from 'expo-constants';
import { URL } from 'react-native-url-polyfill';

// Checks if custom API URL is localhost
// -> replaces "localhost" w/ local IP (allows connection to local backend from all devices)
export const resolveCustomApiUrl = () => {
  const apiUrl = new URL(process.env.EXPO_PUBLIC_API_URL);

  if (apiUrl.hostname === 'localhost') {
    // Get local IP (uses Expo host IP)
    const expoHost = Constants?.expoConfig?.hostUri;
    if (expoHost) {
      const [expoHostname] = expoHost.split(':');

      // Replace localhost with local IP
      apiUrl.host = expoHostname;
    }
  }

  return apiUrl.toString();
};

// Remove if additional util functions are added
export default resolveCustomApiUrl;
