import React, {
  createContext, useState,
} from 'react';
import * as Linking from 'expo-linking';
import PropTypes from 'prop-types';
import ClubAPI from '../api/ClubsApi';
import EventsApi from '../api/EventsApi';
import { resetRoot } from '../navigation/helpers/resetNavigationHelper';

// We need context to track the linking url in case a user signs out and
// then scans a url. In this case, we need to store the url for later.
export const DeepLinkingContext = createContext();
export function DeepLinkingProvider({ children }) {
  const [currentUrl, setCurrentUrl] = useState(null);
  const [initialLoad, setInitialLoad] = useState(true);

  // If the params are empty, then this is the default clubfinity url (return null).
  // If the params are not empty, return the custom url parameters.
  function getQueryParams(unParsedUrl) {
    if (unParsedUrl === null) return null;

    const urlSections = unParsedUrl.split('/').filter(Boolean);
    if (urlSections.length < 2) return null;

    // Get the last two elements in the url, which should be type and name/id
    // End of url for clubs: /club/club_name, end of url for events: /event/event_id
    // Make sure this parsing still works with links created after deployment
    const type = urlSections[urlSections.length - 2];
    const idOrName = urlSections[urlSections.length - 1];

    // Check if the type is valid and if the idOrName is not empty
    if (!['club', 'event'].includes(type) || !idOrName) return null;

    return { type, idOrName };
  }

  // Open screen specified by linking URL (via type and name/id)
  const openScreen = async (navigation, params) => {
    const { type, idOrName } = params;
    let screen;
    let navigationParams;

    if (type === 'club') {
      screen = 'Club';
      const allClubs = await ClubAPI.getAllClubs();
      const club = allClubs.find((c) => c.name.toLowerCase().replace(/ /g, '_') === idOrName);
      navigationParams = { club };
    }

    if (type === 'event') {
      screen = 'Event';
      navigationParams = { event: await EventsApi.getEvent(idOrName) };
    }

    if (!screen) return;

    // This syntax is needed so that we navigate to the
    // correct "parent" page before going deeper into
    // the stack. This way, users can back out to the Discover Screen.
    navigation.navigate('AppStack', {
      screen: 'DiscoverStack',
      params: { screen: 'Discovery' },
    });
    navigation.navigate('AppStack', {
      screen: 'DiscoverStack',
      params: {
        screen,
        params: navigationParams,
      },
    });
  };

  // There are two possible scenario:
  // 1 - a valid linking URL is available, navigate to its destination
  // 2 - navigate to default "home" screen
  async function navigateToInitialLinkingUrlIfPresent(navigation, routeName) {
    let initialUrl = null;

    // If this navigation call is being made on the app's first render,
    // then get the inital url from expo's API.
    if (initialLoad === true) {
      initialUrl = await Linking.getInitialURL();
      setInitialLoad(false);
    }

    // Check if a more a recent URL was scanned
    if (currentUrl !== null) {
      // "Wipe" the url so we don't re-use it in the future. If the user
      // explicitly scans another qr code we will set this value again.
      initialUrl = currentUrl;
      setCurrentUrl(null);
    }

    const params = getQueryParams(initialUrl);
    if (params === null) {
      navigation.navigate(routeName);
    } else {
      openScreen(navigation, params);
    }
  }

  // Navigate to linking URL destination (only if already logged in)
  const navigateToNewLinkingUrl = (navigation, url) => {
    const params = getQueryParams(url);
    if (params !== null) {
      // We need to reset the stack in order to traverse it.
      resetRoot('AppStack');
      openScreen(navigation, params);
    }
  };

  // Creates unique url using type and name/id
  const createUrl = (type, idOrName) => Linking.createURL(`/${type}/${idOrName}`);

  const contextValue = {
    setCurrentUrl,
    createUrl,
    navigateToInitialLinkingUrlIfPresent,
    navigateToNewLinkingUrl,
  };

  return <DeepLinkingContext.Provider value={contextValue}>{children}</DeepLinkingContext.Provider>;
}

DeepLinkingProvider.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]).isRequired,
};
