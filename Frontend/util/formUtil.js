// eslint-disable-next-line import/prefer-default-export
export function updateStateAndClearErrors(component, field, value) {
  const { errors } = component.state;

  const stateUpdate = {};

  stateUpdate[field] = value;

  errors[field] = null;
  stateUpdate.errors = errors;

  component.setState(stateUpdate);
}
