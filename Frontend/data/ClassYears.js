const classYear = new Date().getFullYear();

// display the current year and next 4 years for possible graduation dates
const ClassYears = [
  { label: (classYear + 4).toString(), value: (classYear + 4).toString() },
  { label: (classYear + 3).toString(), value: (classYear + 3).toString() },
  { label: (classYear + 2).toString(), value: (classYear + 2).toString() },
  { label: (classYear + 1).toString(), value: (classYear + 1).toString() },
  { label: classYear.toString(), value: classYear.toString() },
];

export default ClassYears;
