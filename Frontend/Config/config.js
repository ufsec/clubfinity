import { resolveCustomApiUrl } from '../util/setup';

// Check for custom API URL from .env
const devUrl = process.env.EXPO_PUBLIC_API_URL ? resolveCustomApiUrl() : 'https://clubfinity.staging.ufsec.org/';

const config = {
  test: {
    url: 'http://mock/',
  },
  development: {
    url: devUrl,
  },
  production: {
    url: 'https://clubfinity.prod.ufsec.org/',
  },
};

export default config[process.env.NODE_ENV];
