import React, { Component } from 'react';
import {
  ActivityIndicator,
  FlatList,
  Text,
  View,
  Platform,
  StyleSheet,
  ScrollView,
} from 'react-native';
import Constants from 'expo-constants';
import * as Notifications from 'expo-notifications';
import * as SecureStore from 'expo-secure-store';
import AlertCard from '../components/AlertCard';
import EventCard from '../components/EventCard';
import CustomRefresh from '../components/CustomRefresh';
import EventsApi from '../api/EventsApi';
import AlertsApi from '../api/AlertsApi';
import UserApi from '../api/UserApi';
import DiscoverButton from '../components/DiscoverButton';
import { UserContext } from '../util/UserContext';
import colors from '../util/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10,
    backgroundColor: '#F2F2F7',
  },
  bodyText: {
    color: colors.text,
    fontSize: 13,
  },
  headerText: {
    fontSize: 15,
    marginTop: 15,
    marginBottom: 5,
    alignSelf: 'center',
    color: colors.grayScale9,
  },
  emptyContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyContainerText: {
    fontSize: 25,
    margin: 10,
    color: colors.grayScale8,
  },
  emptyContainerSubtext: {
    fontSize: 20,
    margin: 5,
    color: colors.grayScale8,
  },
});

class HomeScr extends Component {
  static contextType = UserContext;

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      isFollowingClubs: true,
      isLoadingEvents: false,
      page: 1,
      pageSize: 10,
      eventType: 'upcoming',
      events: [],
      alerts: [],
    };
  }

  async componentDidMount() {
    this.registerForPushNotificationsAsync();
    await this.prepareAlerts();
    const { user } = this.context;
    const { clubs } = user;
    const { navigation } = this.props;

    if (clubs.length === 0) {
      this.setState({
        events: [],
        alerts: [],
        isFollowingClubs: false,
        isLoading: false,
      });

      return;
    }

    navigation.addListener('willFocus', this.onFocus);
    this.onFocus();
  }

  changeHandler = (newState) => {
    this.setState(newState);
  };

  onFocus = async () => {
    // Get first page of events and add to state
    const { page, pageSize, eventType } = this.state;
    const { events, totalEvents } = await EventsApi.getFollowing(
      page,
      pageSize,
      eventType,
    );
    this.setState({
      events,
      totalEvents,
      isLoading: false,
    });
  };

  registerForPushNotificationsAsync = async () => {
    if (Constants.isDevice) {
      const { status: existingStatus } = await Notifications.getPermissionsAsync();
      let finalStatus = existingStatus;
      if (existingStatus !== 'granted') {
        const { status } = await Notifications.requestPermissionsAsync();
        finalStatus = status;
      }
      if (finalStatus !== 'granted') {
        // eslint-disable-next-line no-console
        console.log('Failed to get push token for push notification!');
        return;
      }
      // TODO: The following two lines are giving warnings. Fix them.
      const token = (await Notifications.getExpoPushTokenAsync()).data;
      UserApi.updatePushToken(token);
    } else {
      // eslint-disable-next-line no-console
      console.log('Must use physical device for Push Notifications');
    }

    if (Platform.OS === 'android') {
      Notifications.setNotificationChannelAsync('default', {
        name: 'default',
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: '#FF231F7C',
      });
    }
  };

  navigateToDiscover = () => {
    const { navigation } = this.props;
    navigation.navigate('DiscoverStack', { screen: 'Discovery' });
  };

  loadingView = () => (
    <View style={{ flex: 1, padding: 20 }}>
      <ActivityIndicator />
    </View>
  );

  notFollowingClubsView = () => {
    const { emptyContainer, emptyContainerText } = styles;
    const { user } = this.context;
    const { eventType, pageSize } = this.state;
    return (
      <ScrollView
        refreshControl={(
          <CustomRefresh
            changeHandler={this.changeHandler}
            reqs={{
              screen: 'HomeScr',
              clubs: user.clubs,
              eventType,
              pageSize,
            }}
          />
        )}
        contentContainerStyle={{ flex: 1 }}
      >
        <View style={emptyContainer}>
          <Text style={emptyContainerText}>
            You are not following any clubs
          </Text>
          <DiscoverButton onPress={this.navigateToDiscover} />
        </View>
      </ScrollView>
    );
  };

  noUpcomingEventsView = () => {
    const { emptyContainer, emptyContainerText, emptyContainerSubtext } = styles;
    const { user } = this.context;
    const { eventType, pageSize } = this.state;
    return (
      <ScrollView
        refreshControl={(
          <CustomRefresh
            changeHandler={this.changeHandler}
            reqs={{
              screen: 'HomeScr',
              clubs: user.clubs,
              eventType,
              pageSize,
            }}
          />
        )}
        contentContainerStyle={{ flex: 1 }}
      >
        <View style={emptyContainer}>
          <Text style={emptyContainerText}>No upcoming events</Text>
          <Text style={emptyContainerSubtext}>Find new clubs!</Text>
        </View>
      </ScrollView>
    );
  };

  filterAlerts = async (alerts) => {
    const alertTypeOrder = ['urgent', 'warning', 'info'];

    const includeAlertPromises = alerts.map(async (alert) => {
      const dismissed = await SecureStore.getItemAsync(
        `alert_dismissed_${alert._id}`,
      );
      return !dismissed;
    });

    const includeAlerts = await Promise.all(includeAlertPromises);

    return alerts
      .filter((_, index) => includeAlerts[index])
      .sort((alertOne, alertTwo) => {
        const alertOneIndex = alertTypeOrder.indexOf(
          alertOne.type.toLowerCase(),
        );
        const alertTwoIndex = alertTypeOrder.indexOf(
          alertTwo.type.toLowerCase(),
        );
        return alertOneIndex - alertTwoIndex;
      });
  };

  prepareAlerts = async () => {
    const alerts = await AlertsApi.getAlerts();
    const filteredAlerts = await this.filterAlerts(alerts);
    this.setState({ alerts: filteredAlerts });
  };

  dismissAlert = async (alertId) => {
    await SecureStore.setItemAsync(`alert_dismissed_${alertId}`, 'true');
    const { alerts } = this.state;
    this.setState({
      alerts: alerts.filter((alert) => alert._id !== alertId),
    });
  };

  eventListView = () => {
    const {
      events, eventType, pageSize, isLoadingEvents, alerts,
    } = this.state;
    const { user } = this.context;
    const { container, bodyText, headerText } = styles;
    const { navigation } = this.props;

    return (
      <View style={[container, bodyText]}>
        {alerts.length > 0 && (
          <View style={{ marginTop: 10 }}>
            {alerts.slice(0, 3).map((alert) => (
              <AlertCard
                key={alert._id}
                alert={alert}
                dismissAlert={this.dismissAlert}
              />
            ))}
          </View>
        )}
        <Text style={headerText}>Upcoming Events</Text>
        <FlatList
          // When close to end, load more events
          onEndReached={() => this.loadMoreEvents()}
          onEndReachedThreshold={0.7}
          refreshControl={(
            <CustomRefresh
              changeHandler={this.changeHandler}
              reqs={{
                screen: 'HomeScr',
                clubs: user.clubs,
                eventType,
                pageSize,
              }}
            />
          )}
          data={events}
          renderItem={({ item }) => (
            <EventCard
              key={item._id}
              event={item}
              updateStatus={this.onFocus}
              userId={user._id}
              navigation={navigation}
            />
          )}
          contentContainerStyle={{ paddingBottom: 5 }}
          keyExtractor={(item) => item._id.toString()}
        />
        {isLoadingEvents && (
          <ActivityIndicator style={{ paddingVertical: 20 }} />
        )}
      </View>
    );
  };

  async loadMoreEvents() {
    // Load next page of events and add to events list
    const {
      events, totalEvents, isLoadingEvents, page, pageSize, eventType,
    } = this.state;
    // Only proceed with API call if none already in process
    // And if there are more events to load
    if (isLoadingEvents === false && events.length < totalEvents) {
      this.setState({ isLoadingEvents: true });
      const { events: eventsToAdd, totalEvents: totalEve } = await EventsApi.getFollowing(
        page + 1, pageSize, eventType,
      );
      events.push(...eventsToAdd);

      // Update events and increment page number
      this.setState({
        events,
        totalEvents: totalEve,
        page: page + 1,
        isLoadingEvents: false,
      });
    }
  }

  render() {
    const { isLoading, isFollowingClubs, events } = this.state;

    if (isLoading) return this.loadingView();
    if (!isFollowingClubs) return this.notFollowingClubsView();
    if (events.length === 0) return this.noUpcomingEventsView();
    return this.eventListView();
  }
}

export default HomeScr;
