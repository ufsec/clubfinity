import React, {
  useCallback, useRef, useState,
} from 'react';
import {
  View, Text, Image, TouchableOpacity, ScrollView,
} from 'react-native';
import { BottomSheetModal, BottomSheetBackdrop } from '@gorhom/bottom-sheet';
import { Ionicons } from '@expo/vector-icons';
import discordLogo from '../assets/images/discord.png';
import slackLogo from '../assets/images/slack.png';
import EchoDiscord from './integrations/EchoDiscord';

// Array with all integration services (and their platforms). Allows consistent formatting across all integrations
const integrations = [
  {
    name: 'Echo',
    description: 'Real-time club updates on third-party platforms',
    platforms: [
      {
        logo: discordLogo,
        name: 'Discord',
        description: 'Setup notifications within your Discord server',
        // Put to TRUE if onboarding modal screen isn't setup yet (navigation button is then deactivated)
        disabled: false,
        // Function to be displayed within the pop-up modal (where the step-by-step instructions are put)
        // A seperate screen for each platform -> insert it inside the "integrations" folder (within "/screens")
        modalContent: () => (
          <EchoDiscord />
        ),
      },
      {
        logo: slackLogo,
        name: 'Slack',
        description: 'Club notifications sent right to your workspace',
        disabled: true,
      },
    ],
  },
];

const integrationCardStyle = {
  width: '85%',
  height: 85,
  borderWidth: 0.5,
  borderRadius: 10,
  borderColor: '#d3d3d3',
  marginVertical: '3%',
  elevation: 3,
  backgroundColor: 'white',
  shadowColor: '#000',
  shadowOffset: { width: 0, height: 2 },
  shadowOpacity: 0.1,
  shadowRadius: 2,
  flexDirection: 'row',
};

const disabledIntegrationCardStyle = {
  ...integrationCardStyle,
  backgroundColor: '#f9f9f9',
};

const IntegrationsScr = () => {
  const bottomSheetModalRef = useRef(null);
  const [selectedPlatform, setSelectedPlatform] = useState(null);

  // Opens the modal with the desired platform content
  const handlePresentModalPress = useCallback((platform) => {
    setSelectedPlatform(platform);
    bottomSheetModalRef.current?.present();
  }, []);

  // Background whenever the modal is open
  const renderBackdrop = useCallback(
    (props) => (
      <BottomSheetBackdrop
        disappearsOnIndex={-1}
        appearsOnIndex={0}
        opacity={0.5}
        /* eslint-disable react/jsx-props-no-spreading */
        {...props}
      />
    ),
    [],
  );

  return (
    <ScrollView contentContainerStyle={{
      flex: 1,
      flexDirection: 'column',
      alignItems: 'center',
      paddingVertical: '8%',
    }}
    >
      {/* Render each integration type (e.g. Echo) */}
      {integrations.map((integration) => (
        <React.Fragment key={integration.name}>
          <View style={{ width: '85%' }}>
            <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{integration.name}</Text>
            <Text style={{
              fontSize: 15, fontWeight: 'normal', color: 'dimgray', marginTop: '1%',
            }}
            >
              {integration.description}
            </Text>
          </View>
          <View style={{
            flex: 1, flexDirection: 'column', alignItems: 'center', marginVertical: '3%',
          }}
          >
            {/* Render each platform for a specific integration (e.g. Discord & Slack for Echo) */}
            {integration.platforms.map((platform) => (
              <TouchableOpacity
                key={platform.name}
                style={platform.disabled ? disabledIntegrationCardStyle : integrationCardStyle}
                disabled={platform.disabled}
                onPress={() => handlePresentModalPress(platform)}
              >
                <View style={{
                  width: '20%', height: '100%', justifyContent: 'center', alignItems: 'center',
                }}
                >
                  <Image
                    source={platform.logo}
                    style={{ width: 35, height: 35, resizeMode: 'contain' }}
                  />
                </View>
                <View style={{
                  width: (!platform.disabled ? '65%' : '80%'),
                  justifyContent: 'top',
                  alignItems: 'left',
                  paddingVertical: '3%',
                  paddingHorizontal: '3%',
                }}
                >
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontWeight: '600', fontSize: 16, paddingVertical: '1%' }}>{platform.name}</Text>
                    {platform.disabled && (
                      <View style={{
                        backgroundColor: 'rgba(17, 142, 255, 0.15)',
                        paddingHorizontal: '4%',
                        borderRadius: 30,
                        marginLeft: '4%',
                        justifyContent: 'center',
                      }}
                      >
                        <Text style={{ fontSize: 11, color: 'midnightblue' }}>Coming Soon</Text>
                      </View>
                    )}
                  </View>
                  <Text style={{ fontSize: 13, paddingTop: '2%', color: 'gray' }}>{platform.description}</Text>
                </View>
                <View style={{
                  width: (!platform.disabled ? '15%' : '0%'),
                  height: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                >
                  <Ionicons
                    name="chevron-forward"
                    size={20}
                    style={{ paddingRight: '2%' }}
                  />
                </View>
              </TouchableOpacity>
            ))}
          </View>
        </React.Fragment>
      ))}
      <BottomSheetModal
        ref={bottomSheetModalRef}
        index={1}
        snapPoints={['15%', '90%']}
        enableDismissOnClose
        backdropComponent={renderBackdrop}
      >
        {/* Opens the platform's modal content (according to the integrations array object (top of file) */}
        {selectedPlatform && selectedPlatform.modalContent && selectedPlatform.modalContent()}
      </BottomSheetModal>
    </ScrollView>
  );
};

export default IntegrationsScr;
