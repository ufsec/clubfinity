import React, { useState, useContext } from 'react';
import {
  View,
  Dimensions,
  TouchableOpacity,
  Image,
  StyleSheet,
  Text,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ClubfinityLogo from '../assets/images/ClubfinityLogo.png';

import AuthApi from '../api/AuthApi';
import ErrorText from '../components/ErrorText';
import { UserContext } from '../util/UserContext';
import colors from '../util/colors';
import { DeepLinkingContext } from '../util/DeepLinkingContext';
import Form from '../components/Form';

const MAX_FIELD_WIDTH = (Dimensions.get('screen').width * 4) / 5;

const styles = StyleSheet.create({
  mainContainer: {
    paddingTop: '12%',
    paddingBottom: '5%',
    paddingHorizontal: 20,
    flex: 1,
    display: 'flex',
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 34,
    letterSpacing: 1,
    marginVertical: 30,
  },
  field: {
    borderBottomColor: colors.grayScale1,
    borderBottomWidth: 2,
    minWidth: MAX_FIELD_WIDTH,
    borderColor: colors.grayScale1,
    margin: 8,
    paddingVertical: 9,
  },
  loginButton: {
    padding: 10,
    minWidth: MAX_FIELD_WIDTH,
    backgroundColor: colors.primary0,
    borderColor: colors.primary1,
    borderRadius: 8,
    marginHorizontal: 12,
    marginVertical: 12,
    minHeight: 42,
    elevation: 3,
  },
  loginButtonText: {
    fontSize: 15,
    alignSelf: 'center',
    color: colors.grayScale0,

  },

  signupLink: {
    borderBottomColor: colors.primary0,
    borderBottomWidth: 1,
    top: 5,
  },
});

const SigninScr = ({ navigation }) => {
  const { setUser } = useContext(UserContext);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [showError, setShowError] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const { navigateToInitialLinkingUrlIfPresent } = useContext(DeepLinkingContext);
  const [processingRequest, setProcessingRequest] = useState(false);

  const signIn = async () => {
    setProcessingRequest(true);
    try {
      const authResponse = await AuthApi.authenticate(email, password);

      if (authResponse.token) {
        setShowError(false);
        setErrorMessage('');
        if (authResponse.user) {
          setUser(authResponse.user);

          // If user opened app via QR Code -> navigate to URL destination
          // If not -> navigate to default "home"
          navigateToInitialLinkingUrlIfPresent(navigation, 'AppStack');
        }
      } else {
        setShowError(true);
        setErrorMessage(authResponse.error);
      }
    } catch (error) {
      console.error(error);
    }
    setProcessingRequest(false);
  };

  const forgotPassword = () => {
    navigation.navigate('ForgotPassword');
  };

  const signUp = () => {
    navigation.navigate('SignUp');
  };

  return (
    <KeyboardAwareScrollView
      resetScrollToCoords={{ x: 0, y: 0 }}
      contentContainerStyle={{ flex: 1 }}
      scrollEnabled={false}
    >
      <View style={styles.mainContainer}>
        <View
          style={{
            flex: 1,
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <Image
            style={{
              width: 200,
              height: 200,
              margin: 30,
              marginBottom: 80,
            }}
            source={ClubfinityLogo}
          />
          <View style={{ flex: 1, minWidth: MAX_FIELD_WIDTH }}>
            <Form style={{ padding: 0 }}>
              <Form.Text
                placeholder="Email"
                value={email}
                onChange={setEmail}
                autoCapitalize="none"
                returnKeyType="next"
                keyboardType="email-address"
              />
              <Form.Text
                placeholder="Password"
                value={password}
                onChange={setPassword}
                autoCapitalize="none"
                returnKeyType="next"
                secureTextEntry
              />
              {showError && <ErrorText errorMessage={errorMessage} />}
              <Form.Button
                text={processingRequest ? 'Logging in...' : 'Login'}
                onPress={signIn}
                isLoading={processingRequest}
              />
            </Form>
            <View
              style={{
                flex: 1,
              }}
            >
              <Text style={{ color: colors.text, alignSelf: 'center', fontSize: 13 }}>
                <TouchableOpacity
                  onPress={forgotPassword}
                  style={styles.signupLink}
                >
                  <Text style={{ color: colors.info, fontSize: 13, top: 2 }}>Forgot your password?</Text>
                </TouchableOpacity>
              </Text>
              <Text style={{ color: colors.text, alignSelf: 'center', fontSize: 13 }}>
                <Text>Don&apos;t have an account yet? </Text>
                <TouchableOpacity
                  onPress={signUp}
                  style={styles.signupLink}
                >
                  <Text style={{ color: colors.info, fontSize: 13, top: 2 }}>Sign up</Text>
                </TouchableOpacity>
              </Text>
            </View>
          </View>
        </View>
      </View>
    </KeyboardAwareScrollView>
  );
};

SigninScr.navigationOptions = {
  header: null,
};

export default SigninScr;
