import React, { Component } from 'react';
import { View, SectionList, Text } from 'react-native';
import CustomRefresh from '../components/CustomRefresh';
import EventsApi from '../api/EventsApi';
import Row from '../components/Row';
import SegmentedControl from '../components/SegmentedControl';

class EventList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pastEvents: [],
      upcomingEvents: [],
      pastPage: 1,
      upcomingPage: 1,
      pageSize: 15,
      eventType: 'upcoming',
      loading: false,
    };
    this.changeEventType = this.changeEventType.bind(this);
  }

  async componentDidMount() {
    const { pastPage, upcomingPage, pageSize } = this.state;
    const { route } = this.props;
    const { clubId } = route.params;

    this.setState({ loading: true });

    const { events: pastEvents, totalEvents: totalPastEvents } = await EventsApi.getForClub(
      clubId, pastPage, pageSize, 'past',
    );

    const {
      events: upcomingEvents, totalEvents: totalUpcomingEvents,
    } = await EventsApi.getForClub(clubId, upcomingPage, pageSize, 'upcoming');

    this.setState({
      // eslint-disable-next-line react/no-unused-state
      pastEvents, upcomingEvents, totalPastEvents, totalUpcomingEvents, loading: false,
    });
  }

  changeHandler = (newState) => {
    const { eventType } = this.state;
    const { events, totalEvents, page } = newState;

    // Pick which event type variables to update in state
    const eventsToUpdate = eventType === 'past' ? 'pastEvents' : 'upcomingEvents';
    const totalEventsToUpdate = eventType === 'past' ? 'totalPastEvents' : 'totalUpcomingEvents';
    const pageToUpdate = eventType === 'past' ? 'pastPage' : 'upcomingPage';

    this.setState({
      [eventsToUpdate]: events,
      [totalEventsToUpdate]: totalEvents,
      [pageToUpdate]: page,
    });
  };

  renderSectionHeader = (section) => (
    <Text
      style={{
        fontWeight: 'bold',
        fontSize: 18,
        marginBottom: 15,
        backgroundColor: 'white',
      }}
    >
      {section.title}
    </Text>
  );

  renderEmpty = () => (
    <Text style={{ color: 'rgba(0, 0, 0, 0.50)', textAlign: 'center' }}>
      No Events
    </Text>
  );

  renderLoading = () => (
    <View style={{ flex: 1, paddingVertical: 20, paddingHorizontal: 30 }}>
      <Text style={{ color: 'rgba(0, 0, 0, 0.50)', textAlign: 'center' }}>
        Loading Events...
      </Text>
    </View>
  );

  // Load next page of events and append to current list
  async loadMoreEvents(clubId) {
    const { eventType } = this.state;

    const eventsToUpdate = eventType === 'past' ? 'pastEvents' : 'upcomingEvents';
    const totalEventsToUpdate = eventType === 'past' ? 'totalPastEvents' : 'totalUpcomingEvents';
    const pageToUpdate = eventType === 'past' ? 'pastPage' : 'upcomingPage';

    const {
      [eventsToUpdate]: events,
      [totalEventsToUpdate]: totalEvents,
      [pageToUpdate]: page,
      loading,
      pageSize,
    } = this.state;

    // Only call API if none are already in process and if there are more events to pull
    if (loading === false && events.length < totalEvents) {
      this.setState({ loading: true });

      const { events: eventsToAdd, totalEvents: totalEventsUpdated } = await EventsApi.getForClub(
        clubId, page + 1, pageSize, eventType,
      );
      events.push(...eventsToAdd);

      // Increment current page and update events, set loading to false for next call
      this.setState({
        [eventsToUpdate]: events,
        [totalEventsToUpdate]: totalEventsUpdated,
        [pageToUpdate]: page + 1,
        loading: false,
      });
    }
  }

  async changeEventType(active) {
    // 0 => upcoming, 1 => past
    let { eventType } = this.state;
    if (active === 0 && eventType === 'past') {
      // Switch events to upcoming
      eventType = 'upcoming';
    } else if (active === 1 && eventType === 'upcoming') {
      // Switch events to past
      eventType = 'past';
    }

    this.setState({ eventType });
  }

  render() {
    const { navigation, route } = this.props;
    const {
      pastEvents, upcomingEvents, eventType, pageSize, loading,
    } = this.state;
    const { clubId } = route.params;

    // Display whether past or upcoming events
    const listData = [];
    if (pastEvents.length > 0 && eventType === 'past') {
      listData.push({ title: 'Past Events', data: pastEvents });
    } else if (upcomingEvents.length > 0 && eventType === 'upcoming') {
      listData.push({ title: 'Upcoming Events', data: upcomingEvents });
    }

    return (
      <View
        style={{
          flex: 1,
          paddingVertical: 10,
          paddingHorizontal: 30,
        }}
      >
        <SegmentedControl
          parentHandler={this.changeEventType}
          optionOne="Upcoming"
          optionTwo="Past"
        />
        <SectionList
          style={{ marginTop: 10 }}
          onEndReached={() => this.loadMoreEvents(clubId)}
          onEndReachedThreshold={0.7}
          refreshControl={(
            <CustomRefresh
              changeHandler={this.changeHandler}
              reqs={{
                screen: 'EventList',
                clubId,
                pageSize,
                eventType,
              }}
            />
          )}
          sections={listData}
          keyExtractor={(event) => event._id}
          renderItem={({ item }) => (
            <Row
              date={item.date.toFormat('MMM dd yyyy')}
              text={item.name}
              handler={() => {
                navigation.navigate('Event', { event: item });
              }}
            />
          )}
          renderSectionHeader={({ section }) => this.renderSectionHeader(section)}
          ListEmptyComponent={!loading && this.renderEmpty}
        />
        {loading && this.renderLoading()}
      </View>
    );
  }
}

export default EventList;
