import React, { Component } from 'react';
import {
  View, SectionList, Text,
} from 'react-native';
import CustomRefresh from '../components/CustomRefresh';
import AnnouncementsApi from '../api/AnnouncementsApi';
import Row from '../components/Row';

class AnnouncementList extends Component {
  constructor(props) {
    super(props);
    this.isAdmin = false;
    this.state = {
      announcements: [],
      loading: false,
      page: 1,
      pageSize: 15,
    };
  }

  async componentDidMount() {
    const { page, pageSize } = this.state;
    const { route } = this.props;
    const { clubId, isAdmin } = route.params;
    this.setState({ loading: true });
    // Get first (1st) page of announcements with given page size
    const { announcements, totalAnnouncements } = await AnnouncementsApi.getForClub(clubId, page, pageSize);
    this.isAdmin = isAdmin;
    this.setState({
      announcements,
      totalAnnouncements,
      loading: false,
    });
  }

  changeHandler = (newState) => {
    this.setState(newState);
  };

  renderSectionHeader = (section) => (
    <Text
      style={{
        fontWeight: 'bold',
        fontSize: 18,
        marginBottom: 15,
        backgroundColor: 'white',
      }}
    >
      {section.title}
    </Text>
  );

  renderEmpty = () => (
    <Text style={{ color: 'rgba(0, 0, 0, 0.50)', textAlign: 'center' }}>
      No Announcements
    </Text>
  );

  renderLoading = () => (
    <View style={{ flex: 1, paddingVertical: 20, paddingHorizontal: 30 }}>
      <Text style={{ color: 'rgba(0, 0, 0, 0.50)', textAlign: 'center' }}>
        Loading Announcements...
      </Text>
    </View>
  );

  // Function to append next page of announcements to current announcements
  async loadMoreAnnouncements(clubId) {
    const {
      announcements, totalAnnouncements, loading, page, pageSize,
    } = this.state;

    // Only runs if this function is not already still running and if there
    // are more announcements to pull
    if (loading === false && announcements.length < totalAnnouncements) {
      this.setState({ loading: true });

      // Get announcements to add
      const {
        announcements: annToAdd,
        totalAnnouncements: totalAnn,
      } = await AnnouncementsApi.getForClub(clubId, page + 1, pageSize);

      // Push new announcements to current and update announcements in state
      announcements.push(...annToAdd);
      this.setState({
        announcements,
        totalAnnouncements: totalAnn,
        page: page + 1,
        loading: false,
      });
    }
  }

  render() {
    const { navigation, route } = this.props;
    const { announcements, pageSize, loading } = this.state;
    const { clubId } = route.params;
    const listData = [{ title: 'Announcements', data: announcements }];

    return (
      <View style={{ flex: 1, paddingVertical: 20, paddingHorizontal: 30 }}>
        <SectionList
          // Load more announcements when reaching end of list,
          // Threshold of 0.7 means end of list is within length of
          // 70% of what is visible
          onEndReached={() => this.loadMoreAnnouncements(clubId)}
          onEndReachedThreshold={0.7}
          refreshControl={(
            <CustomRefresh
              changeHandler={this.changeHandler}
              reqs={{
                screen: 'AnnouncementList',
                clubId,
                pageSize,
              }}
            />
          )}
          sections={listData}
          keyExtractor={(announcement) => announcement._id}
          renderItem={({ item }) => (
            <Row
              date={item.date.toFormat('MMM dd yyyy')}
              text={item.title}
              handler={() => {
                navigation.navigate('Announcement', {
                  announcement: item,
                  isAdmin: this.isAdmin,
                });
              }}
            />
          )}
          renderSectionHeader={({ section }) => this.renderSectionHeader(section)}
          ListEmptyComponent={this.renderEmpty}
        />
        {loading && this.renderLoading()}
      </View>
    );
  }
}

export default AnnouncementList;
