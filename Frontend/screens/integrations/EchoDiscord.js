import {
  View, Text, Image, TouchableOpacity, Linking, useWindowDimensions,
} from 'react-native';
import React from 'react';
import { Ionicons } from '@expo/vector-icons';
import PropTypes from 'prop-types';
import discordLogo from '../../assets/images/discord.png';
import Carousel from '../../components/Carousel';
import ExpandableVideo from '../../components/ExpandableVideo';

export default function EchoDiscord() {
  // Data for each step of the Discord onboarding. To add a new step, simply append a new object to the array
  // Note: video URLs are using the new large-asset storage mechanism (via AWS S3)
  // Inside the bucket - clubfinity.assets.ufsec.org
  const onboardingSteps = [
    {
      id: 1,
      title: 'Step 1',
      description: 'Invite the Clubfinity Echo bot to your Discord server. Click the button above!',
      video: 'http://clubfinity.assets.ufsec.org/discord_onboarding_step_1',
    },
    {
      id: 2,
      title: 'Step 2',
      description: 'Use the /help command to see what Echo can do within your Discord server.',
      video: 'http://clubfinity.assets.ufsec.org/discord_onboarding_step_2',
    },
    {
      id: 3,
      title: 'Step 3',
      description: 'Create your first listener with the /listen command.',
      video: 'http://clubfinity.assets.ufsec.org/discord_onboarding_step_3',
    },
    {
      id: 4,
      title: 'Step 4',
      description: 'And that\'s it! You will never miss another club update, everything right in Discord!',
      video: 'http://clubfinity.assets.ufsec.org/discord_onboarding_step_4',
    },
  ];

  // Component/View for the Discord steps. Passed along to the Carousel component
  const ItemView = ({ item, shouldPlay }) => {
    const { width } = useWindowDimensions();

    // Function used to "bold" certain words (any "/commands" - i.e. "/help", "/listen", etc...)
    const formatDescription = (description) => {
      const regex = /\/\w+\b/g;
      const words = description.split(/\s+/);

      return words.map((word, index) => (
        // eslint-disable-next-line react/no-array-index-key
        <Text key={index.toString()} style={word.match(regex) ? { color: 'black', fontWeight: '500' } : {}}>
          {word}
          {index !== words.length - 1 && ' '}
        </Text>
      ));
    };

    return (
      <View style={{
        flex: 1, justifyContent: 'center', alignItems: 'center', width,
      }}
      >
        <View style={{
          flex: 0.65, alignItems: 'center', justifyContent: 'center',
        }}
        >
          {/* Displays the step's video, which can also be viewed in "fullscreen" mode via <ExpandableVideo/> */}
          <ExpandableVideo video={item.video} shouldPlay={shouldPlay} />
        </View>
        <View style={{ flex: 0.3, marginTop: '5%' }}>
          <Text style={{
            fontWeight: '800', fontSize: 20, marginBottom: 10, color: '#015ed8', textAlign: 'center',
          }}
          >
            {item.title}
          </Text>
          <Text style={{
            fontWeight: '300', paddingHorizontal: 64, color: '#62656b', textAlign: 'center',
          }}
          >
            {formatDescription(item.description)}
          </Text>
        </View>
      </View>
    );
  };

  ItemView.propTypes = {
    item: PropTypes.shape({
      id: PropTypes.number,
      title: PropTypes.string,
      description: PropTypes.string,
      video: PropTypes.string,
    }).isRequired,
    shouldPlay: PropTypes.bool.isRequired,
  };

  return (
    <View style={{
      flex: 1, justifyContent: 'center', alignItems: 'center', marginVertical: '3%',
    }}
    >
      <View style={{
        width: '100%',
        justifyContent: 'space-between',
        paddingHorizontal: '5%',
        flexDirection: 'row',
        alignItems: 'center',
      }}
      >
        <View style={{ flexDirection: 'column', justifyContent: 'left' }}>
          <Text style={{
            fontSize: 25, fontWeight: 'bold', marginLeft: '2%', marginTop: '2%',
          }}
          >
            Onboarding
          </Text>
          <View style={{
            flexDirection: 'row', alignItems: 'center', marginLeft: '2%', marginTop: '1%',
          }}
          >
            <Image
              source={discordLogo}
              style={{
                width: 30, height: 30, resizeMode: 'contain',
              }}
            />
            <Text style={{ fontSize: 15, fontWeight: 600, marginLeft: '4%' }}>Discord</Text>
          </View>
        </View>
        <TouchableOpacity
          style={{
            paddingVertical: '1.5%',
            paddingHorizontal: '4%',
            flexDirection: 'row',
            alignItems: 'center',
            borderWidth: 1,
            borderRadius: 10,
            borderColor: '#ababab',
          }}
          onPress={() => Linking.openURL(
            // The invite link for the Discord Echo bot. When pressed, it allows users to invite the bot to a server
            // eslint-disable-next-line max-len
            'https://discord.com/api/oauth2/authorize?client_id=1170934132939952219&permissions=551903479840&scope=applications.commands%20bot',
          )}
        >
          <Ionicons name="add" size={24} color="black" />
          <Text>Add Bot</Text>
        </TouchableOpacity>
      </View>
      {/* Uses the Carousel component to display all onboarding steps */}
      <Carousel data={onboardingSteps} ItemView={ItemView} />
    </View>
  );
}
