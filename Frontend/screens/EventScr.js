import React, { useState, useEffect, useContext } from 'react';
import {
  Button, ScrollView, VStack, Image,
} from 'native-base';
import * as Clipboard from 'expo-clipboard';
import { useNavigation } from '@react-navigation/native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { DateTime } from 'luxon';
import {
  StyleSheet, View, Text, Dimensions, Linking,
} from 'react-native';
import RenderHTML from 'react-native-render-html';
import QRCode from 'react-native-qrcode-svg';
import colors from '../util/colors';
import standardColors from '../constants/Colors';
import { UserContext } from '../util/UserContext';
import { DeepLinkingContext } from '../util/DeepLinkingContext';
import Layout from '../constants/Layout';
import ResponseButton from '../components/ResponseButton';
import EventsApi from '../api/EventsApi';
import StatisticsCard from '../components/StatisticsCard';

const style = StyleSheet.create({
  eventsPage: {
    width: '85%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: '5%',
    marginRight: '7.5%',
    marginLeft: '7.5%',
    marginTop: '8%',
    marginBottom: '8%',
  },
  detailListItem: {
    width: '100%',
    height: 45,
    flexDirection: 'row',
    alignItems: 'center',
  },
  editButton: {
    alignSelf: 'center',
    backgroundColor: colors.secondary0,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: '1%',
    marginBottom: '5%',
    marginTop: 20,
    borderRadius: 5,
  },
  statisticsCardContainer: {
    flexDirection: 'row',
    marginTop: 6,
    justifyContent: 'space-around',
    width: '100%',
    display: 'flex',
    flexWrap: 'wrap',
  },
  responseButtonContainer: {
    flexDirection: 'row',
    marginTop: 5,
    width: '100%',
  },
  label: {
    fontSize: 18,
    fontWeight: 'bold',
    width: '100%',
    marginBottom: 12,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  fullWidth: {
    width: '100%',
  },
  horizontalLine: {
    height: 1,
    width: '100%',
    backgroundColor: standardColors.grayFade,
    marginTop: 22,
    marginBottom: 18,
  },
  location: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    width: '75%',
  },
  horizontalLine2: {
    height: 1,
    width: '100%',
    backgroundColor: standardColors.grayFade,
    marginTop: 18,
    marginBottom: 16,
  },
  dayNumber: {
    color: standardColors.black,
    fontSize: 24,
    textAlign: 'center',
  },
  month: {
    color: standardColors.red,
    fontSize: 14,
  },
  shortDateContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    marginRight: 24,
    marginLeft: 10,
  },
  eventHeader: {
    width: '100%',
    height: 85,
    flexDirection: 'row',
    alignItems: 'center',
  },
  titleContainer: {
    height: 45,
    marginBottom: '5%',
    display: 'flex',
  },
  icon: {
    marginRight: '5%',
  },
  linkName: {
    width: '100%',
    fontWeight: 'bold',
  },
  linkURL: {
    width: '100%',
    color: colors.primary0,
    textDecorationLine: 'underline',
  },
});

const EventScr = ({ route }) => {
  const navigation = useNavigation();
  const { event: eventParams } = route.params;

  const { user } = useContext(UserContext);
  const [going, setGoing] = useState(false);
  const [uninterested, setUninterested] = useState(false);
  const [interested, setInterested] = useState(false);
  const [isPrivate, setIsPrivate] = useState(false);
  const [numGoing, setNumGoing] = useState(0);
  const [numUninterested, setNumUninterested] = useState(0);
  const [numInterested, setNumInterested] = useState(0);
  const [isAdmin, setIsAdmin] = useState(false);
  const [event, setEvent] = useState(eventParams);

  useEffect(() => {
    const {
      goingUsers, uninterestedUsers, interestedUsers, club,
    } = event;
    const { clubs } = user;
    const isGoing = goingUsers.includes(user._id);
    const isUninterested = uninterestedUsers.includes(user._id);
    const isInterested = interestedUsers.includes(user._id);
    const userClubs = clubs.map((c) => c._id);
    setIsPrivate(club.isPrivate && !userClubs.includes(club._id));
    setGoing(isGoing);
    setUninterested(isUninterested);
    setInterested(isInterested);
    setNumGoing(goingUsers.length - isGoing);
    setNumUninterested(uninterestedUsers.length - isUninterested);
    setNumInterested(interestedUsers.length - isInterested);
    setIsAdmin(event.club.admins
      .map((admin) => admin._id)
      .includes(user._id));
    setEvent(event);
  }, []);

  const goingHandler = async () => {
    const { updateStatus } = route.params;
    const { going: previouslyGoing } = event;

    if (previouslyGoing) {
      setGoing(false);
      await EventsApi.removeGoingUser(event._id);
    } else {
      setGoing(true);
      setInterested(false);
      setUninterested(false);

      await EventsApi.addGoingUser(event._id);
    }
    updateStatus();
  };

  const uninterestedHandler = async () => {
    const { updateStatus } = route.params;
    const { uninterested: previouslyUninterested } = event;

    if (previouslyUninterested) {
      setUninterested(false);

      await EventsApi.removeUninterestedUser(event._id);
    } else {
      setUninterested(true);
      setInterested(false);
      setGoing(false);

      await EventsApi.addUninterestedUser(event._id);
    }
    updateStatus();
  };

  const interestedHandler = async () => {
    const { updateStatus } = route.params;
    const { interested: previouslyInterested } = event;

    if (previouslyInterested) {
      setInterested(false);
      await EventsApi.removeInterestedUser(event._id);
    } else {
      setInterested(true);
      setUninterested(false);
      setGoing(false);

      await EventsApi.addInterestedUser(event._id);
    }
    updateStatus();
  };

  const datetime = DateTime.fromISO(event.date);
  let month = '';
  if (datetime.monthShort != null) {
    month = datetime.monthShort.toString().toUpperCase();
  }
  const { createUrl } = useContext(DeepLinkingContext);
  const redirectUrl = createUrl('event', event._id);

  return (
    <ScrollView>
      {!isPrivate ? (
        <View style={style.eventsPage}>
          {event.image && (
            <Image
              source={{ uri: event.image }}
              style={{
                height: Dimensions.get('window').height * 0.35,
                width: '100%',
                resizeMode: 'cover',
                borderRadius: 10,
              }}
              alt="event image"
            />
          )}
          <View style={style.eventHeader}>
            <View style={style.shortDateContainer}>
              <Text style={style.dayNumber}>{datetime.day}</Text>
              <Text style={style.month}>{month}</Text>
            </View>
            <View style={{ height: '100%', justifyContent: 'center' }}>
              <Text style={style.title}>{event.name}</Text>
            </View>
          </View>
          <View style={style.responseButtonContainer}>
            <ResponseButton
              clickHandler={goingHandler}
              selected={going}
              icon="checkmark-circle-outline"
              label="Going"
            />
            <View style={{ marginRight: 33, marginLeft: 33 }}>
              <ResponseButton
                clickHandler={interestedHandler}
                selected={interested}
                icon="star-outline"
                label="Interested"
              />
            </View>
            <ResponseButton
              clickHandler={uninterestedHandler}
              selected={uninterested}
              icon="close"
              label="Not Going"
            />
          </View>
          {isAdmin && (
            <Button
              style={style.editButton}
              onPress={() => navigation.navigate('Edit Event', { event })}
            >
              <Text style={{
                alignSelf: 'center',
                color: 'white',
                fontWeight: '600',
                padding: 3,
              }}
              >
                EDIT
              </Text>
            </Button>
          )}
          <View style={style.horizontalLine} />
          <View style={style.detailListItem}>
            <MaterialCommunityIcons
              name="account-group"
              size={24}
              style={style.icon}
            />
            <Text>{event.club.name}</Text>
          </View>
          <View style={style.detailListItem}>
            <MaterialCommunityIcons
              name="map-marker"
              size={24}
              style={style.icon}
            />
            <View style={style.location}>
              <Text>{event.location.description}</Text>
              {event.location.room && (
              <Text>
                Room:
                {event.location.room}
              </Text>
              )}
            </View>
          </View>
          <View style={style.detailListItem}>
            <MaterialCommunityIcons name="clock" size={24} style={style.icon} />
            <Text>{datetime.toLocaleString(DateTime.DATETIME_MED)}</Text>
          </View>
          <Button
            style={{ width: '90%', marginVertical: '4%' }}
            onPress={async () => {
              await Clipboard.setStringAsync(redirectUrl);
            }}
          >
            Copy
          </Button>
          <QRCode value={redirectUrl} />
          <View style={style.horizontalLine2} />
          <VStack width="100%">
            <Text style={style.label}>Description</Text>
            {event?.description && (
              <RenderHTML
                source={{ html: event.description }}
                contentWidth={Layout.window.width}
              />
            )}
          </VStack>
          <View style={style.horizontalLine} />

          {event?.links?.length > 0 && (
            <>
              <Text style={style.label}>Resources</Text>
              {event.links.map((externalLink, index) => (
                <VStack
                  key={externalLink._id}
                  width="100%"
                >
                  {index !== 0 && <Text />}
                  <Text style={style.linkName}>{externalLink.name}</Text>
                  <Text
                    style={style.linkURL}
                    numberOfLines={1}
                    onPress={() => {
                      Linking.openURL(
                        externalLink.link.startsWith('https://') || externalLink.link.startsWith('http://')
                          ? externalLink.link
                          : `https://${externalLink.link}`,
                      );
                    }}
                  >
                    {externalLink.link}
                  </Text>
                </VStack>
              ))}
            </>
          )}

          <View style={style.horizontalLine} />
          <Text style={style.label}>Responses</Text>
          <View style={style.statisticsCardContainer}>
            <StatisticsCard
              icon="checkmark-circle-outline"
              number={numGoing + going}
              label="Going"
            />
            <StatisticsCard
              icon="star-outline"
              number={numInterested + interested}
              label="Interested"
            />
            <StatisticsCard
              icon="close-circle-outline"
              number={numUninterested + uninterested}
              label="Not Going"
            />
          </View>
        </View>
      ) : (
        <Text fontSize="lg">
          This event belongs to a private club. Please ask a club admin to add you as a member.
        </Text>
      )}
    </ScrollView>
  );
};

export default EventScr;
