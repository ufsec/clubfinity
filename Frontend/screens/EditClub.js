import React, { Component } from 'react';
import {
  View, Alert, TextInput, Pressable,
} from 'react-native';
import { Heading, Text } from 'native-base';
import { Ionicons } from '@expo/vector-icons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CheckBox from 'expo-checkbox';
import { UserContext } from '../util/UserContext';
import ClubsApi from '../api/ClubsApi';
import SocialsApi from '../api/SocialsApi';
import ImageUploadAPI from '../api/ImageUploadApi';
import UserApi from '../api/UserApi';
import Form from '../components/Form';
import Modal from '../components/Modal';
import { updateStateAndClearErrors } from '../util/formUtil';
import Validator from '../components/form/validation/Validator';
import Colors from '../constants/Colors';
import colors from '../util/colors';

export default class EditClub extends Component {
  static contextType = UserContext;

  validator = new Validator({
    name: [Validator.required(), Validator.minLength(3)],
    description: [Validator.required(), Validator.maxLength(280)],
    facebookLink: [Validator.required(), Validator.validFacebookUrl()],
    instagramLink: [Validator.required(), Validator.validInstagramUrl()],
    slackLink: [Validator.required(), Validator.validSlackUrl()],
    socialLinks: [Validator.required(), Validator.validLinks()],
    thumbnailUrl: [],
  });

  emailValidator = new Validator({
    email: [
      Validator.required(),
      Validator.email(),
      Validator.endsWith('@ufl.edu', 'Must be a ufl email'),
    ],
  });

  constructor(props) {
    super(props);

    this.state = {
      name: '',
      description: '',
      facebookLink: '',
      instagramLink: '',
      slackLink: '',
      thumbnailUrl: '',
      socialLinks: [],
      socials: [],
      processingRequest: false,
      processingDelete: false,
      deleteModalOpen: false,
      deletionEnteredName: '',
      errors: {},
      isPrivate: false,
      email: '',
    };
  }

  async componentDidMount() {
    const { route } = this.props;
    const { club } = route.params;
    const socials = (await SocialsApi.getAllSocials()).map((social) => ({
      label: social.name,
      value: social._id,
    }));

    this.setState({
      name: club.name,
      description: club.description,
      facebookLink: club.facebookLink,
      instagramLink: club.instagramLink,
      slackLink: club.slackLink,
      thumbnailUrl: club.thumbnailUrl,
      socialLinks: club?.socialLinks || [],
      socials,
      id: club._id,
      isPrivate: club.isPrivate,
    });
  }

  updateSocialLinks = (socialId, link, index) => {
    const { socialLinks, socials } = this.state;

    // Need to find the corresponding social to the ID and reformat the data
    // to match the way the values are stored in state
    const newSocialLinks = [...socialLinks];
    const newSocial = socials.filter((social) => social.value === socialId)[0];
    newSocialLinks[index] = {
      social: {
        _id: newSocial.value,
        name: newSocial.label,
      },
      link,
    };

    updateStateAndClearErrors(this, 'socialLinks', newSocialLinks);
  };

  addSocialLink = () => {
    const { socialLinks, socials } = this.state;

    const unselectedSocials = socials.filter(
      (social) => !socialLinks
        .map((socialLink) => socialLink.social._id)
        .includes(social.value),
    );
    if (!unselectedSocials) return;

    updateStateAndClearErrors(this, 'socialLinks', [
      ...socialLinks,
      {
        link: '',
        social: {
          _id: unselectedSocials[0].value,
          name: unselectedSocials[0].label,
        },
      },
    ]);
  };

  removeSocialLink = (social) => {
    const { socialLinks } = this.state;

    updateStateAndClearErrors(
      this,
      'socialLinks',
      socialLinks.filter((socialLink) => socialLink.social._id !== social),
    );
  };

  // Handles adding new members to private club
  AddMemberHandler = async () => {
    const { route } = this.props;
    const { club } = route.params;
    const validationResults = this.emailValidator.validate(this.state);
    if (!validationResults.valid) {
      this.setState({
        processingRequest: false,
        errors: validationResults.errors,
      });
      return;
    }

    const { email } = this.state;
    const res = await UserApi.getUserByEmail(email);
    let user;
    if (res) {
      user = res.data;
    } else {
      this.setState({
        errors: { email: 'User does not exist' },
      });
      return;
    }
    user.clubs.forEach((c) => {
      if (c._id === club._id) {
        this.setState({
          errors: { email: 'User is already a member' },
        });
      }
    });
    await UserApi.forceAddUser(user._id, club._id, true);

    // TODO: send specified user a notification where they can choose if they want to be added.
    this.setState({ email: '' });
  };

  editClub = async () => {
    const validationResults = this.validator.validate(this.state);

    if (!validationResults.valid) {
      this.setState({
        processingRequest: false,
        errors: validationResults.errors,
      });

      return;
    }
    this.setState({ processingRequest: true });

    const { route, navigation } = this.props;
    const { club, updateClubState } = route.params;
    const { setMessage } = this.context;

    const {
      name,
      description,
      slackLink,
      facebookLink,
      instagramLink,
      thumbnailUrl,
      socialLinks,
      isPrivate,
    } = this.state;

    let uploadedImage;
    if (thumbnailUrl !== club.thumbnailUrl) {
      uploadedImage = await ImageUploadAPI.upload('club', thumbnailUrl);
    }

    const updatedClubValues = {
      ...club,
      name,
      description,
      slackLink,
      facebookLink,
      instagramLink,
      thumbnailUrl: uploadedImage !== null ? uploadedImage : club.thumbnailUrl,
      socialLinks,
      isPrivate,
    };

    const editedClubResponse = await ClubsApi.updateClub(
      club._id,
      updatedClubValues,
    );
    if (editedClubResponse.successfulRequest) {
      this.setState({ processingRequest: false });
      setMessage('Club updated.');

      updateClubState(editedClubResponse.data);

      navigation.pop();
    } else {
      setMessage('Unable to update club.');
      console.log(editedClubResponse.error);
      this.setState({ processingRequest: false });
    }
  };

  handleDelete = () => {
    const { route } = this.props;
    const { club } = route.params;

    this.setState({ deleteModalOpen: false });
    const { deletionEnteredName } = this.state;

    if (deletionEnteredName === club.name) {
      this.deleteClub();
    } else {
      Alert.alert(
        'Could not delete club',
        'The text entered does not match the club\'s name',
      );
    }
    this.setState({ deletionEnteredName: '' });
  };

  // Deletes a club and updates the user's club list.
  deleteClub = async () => {
    const { route, navigation } = this.props;
    const { club } = route.params;
    const { setMessage, user, setUser } = this.context;
    this.setState({ processingDelete: true });

    // Try to delete club
    const deletedClubResponse = await ClubsApi.deleteClub(club._id, club);

    if (deletedClubResponse.successfulRequest) {
      setMessage('Club deleted.');

      // Club successfully deleted, remove club from user's club list
      const updatedUserClubs = user.clubs.filter((c) => c._id !== club._id);

      setUser({ ...user, clubs: updatedUserClubs });
      navigation.popToTop();
    } else {
      setMessage('Unable to delete club.');
      Alert.alert('Unable to delete club', 'An error occurred while deleting this club');
    }
    this.setState({ processingDelete: false });
  };

  openModal = () => {
    this.setState({ deleteModalOpen: true });
  }

  closeModal = () => {
    this.setState({ deleteModalOpen: false });
  }

  render() {
    const { navigation, route } = this.props;
    const { club } = route.params;
    const {
      name,
      description,
      thumbnailUrl,
      socialLinks,
      socials,
      errors,
      processingRequest,
      processingDelete,
      deleteModalOpen,
      deletionEnteredName,
      isPrivate,
      email,
    } = this.state;
    return (
      <KeyboardAwareScrollView enableAutomaticScroll={!deleteModalOpen}>
        <Form>
          <Form.ImagePicker
            text="Upload Club Image"
            image={thumbnailUrl}
            type="club"
            onImagePicked={(value) => updateStateAndClearErrors(this, 'thumbnailUrl', value)}
            error={errors.thumbnailUrl}
          />
          <View style={{ display: 'flex', alignItems: 'center' }}>
            <Heading
              size="xl"
              style={{
                paddingBottom: '2%',
                paddingTop: '5%',
                textAlign: 'center',
              }}
            >
              {name}
            </Heading>
          </View>
          <View
            style={{ display: 'flex', alignItems: 'right', marginBottom: '2%' }}
          >
            <View
              style={{
                display: 'flex',
                alignItems: 'right',
                flexDirection: 'row',
              }}
            >
              <Text fontSize="lg" bold marginTop="2%">
                Private
              </Text>
              <CheckBox
                style={{
                  marginTop: '3.5%',
                  marginLeft: '1.5%',
                  transform: [{ scale: 1.2 }],
                }}
                color={Colors.red}
                onValueChange={(value) => updateStateAndClearErrors(this, 'isPrivate', value)}
                value={isPrivate}
              />
            </View>
            {isPrivate && (
              <>
                <Form.Text
                  placeholder="Add Member By Email"
                  value={email}
                  onChange={(value) => updateStateAndClearErrors(this, 'email', value)}
                  error={errors.email}
                />
                <Form.Button text="Add" onPress={this.AddMemberHandler} />
              </>
            )}
          </View>
          {socialLinks.map((socialLink, index) => (
            <View
              key={socialLink.social._id}
              style={{
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                width: '100%',
              }}
            >
              <Form.Select
                value={socialLink.social._id}
                onChange={(value) => this.updateSocialLinks(value, socialLink.link, index)}
                options={[
                  ...socials.filter(
                    (s) => !socialLinks
                      .map((sl) => sl.social._id)
                      .includes(s.value) || s.value === socialLink.social._id,
                  ),
                ]}
                placeholder="Platform"
                style={{ width: '30%', marginRight: 10 }}
              />
              <Form.Text
                value={socialLink.link}
                onChange={(value) => this.updateSocialLinks(socialLink.social._id, value, index)}
                placeholder="Link"
                error={
                  errors?.socialLinks?.length > index
                    ? errors.socialLinks[index]
                    : null
                }
                style={{ flex: 1, marginRight: 10 }}
              />
              <Pressable
                style={{ width: '5%' }}
                onPress={() => this.removeSocialLink(socialLink.social._id)}
                hitSlop={20}
              >
                <Ionicons name="remove-circle-outline" size={20} color="red" />
              </Pressable>
            </View>
          ))}
          <Form.Button
            style={{
              backgroundColor: colors.grayScale2,
              opacity: socialLinks.length < socials.length ? 1 : 0.3,
            }}
            textStyle={{ color: colors.grayScale10 }}
            text={
              processingRequest
                ? 'Editing...'
                : socialLinks.length < socials.length
                  ? 'Add Social Link'
                  : 'All Socials Added'
            }
            onPress={
              !processingRequest && socialLinks.length < socials.length
                ? this.addSocialLink
                : () => {}
            }
          />
          <Form.TextArea
            placeholder="Description"
            value={description}
            onChange={(value) => updateStateAndClearErrors(this, 'description', value)}
            error={errors.description}
          />
          <Form.Button
            style={{ backgroundColor: Colors.green }}
            text="Sync events with google calendar"
            onPress={() => navigation.navigate('Google Calendar', {
              club,
              navigation,
            })}
          />
          <Form.Button
            text={processingRequest ? 'Editing...' : 'Save'}
            onPress={this.editClub}
            isLoading={processingRequest}
          />
          <Form.Button
            style={{ backgroundColor: Colors.red }}
            text={processingDelete ? 'Deleting...' : 'Delete Club'}
            onPress={() => this.openModal()}
            isLoading={processingDelete}
          />
          <Modal withInput isOpen={deleteModalOpen}>
            <View style={{ backgroundColor: '#efeeee', padding: 20, borderRadius: 5 }}>
              <Text style={{
                fontSize: 20,
                textAlign: 'center',
                marginTop: 'auto',
                marginBottom: 5,
                fontWeight: 'bold',
              }}
              >
                Confirm Deletion
              </Text>
              <Text style={{ marginBottom: 20 }}>Please input your club&apos;s name to confirm its deletion</Text>
              <TextInput
                style={{ backgroundColor: 'white', padding: 10, borderRadius: 10 }}
                placeholder="Club's Name"
                placeholderTextColor="gray"
                onChangeText={(text) => this.setState({ deletionEnteredName: text })}
                value={deletionEnteredName}
              />
              <View style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
                marginTop: 20,
              }}
              >
                <Pressable
                  style={({ pressed }) => [{
                    paddingVertical: 5,
                    paddingHorizontal: 10,
                    borderWidth: 1,
                    borderColor: colors.grayScale8,
                    borderRadius: 5,
                    backgroundColor: pressed ? colors.grayScale6 : Colors.mutedWhite,
                  }]}
                  onPress={() => this.closeModal()}
                >
                  <Text style={{
                    fontWeight: 'bold',
                  }}
                  >
                    Cancel
                  </Text>
                </Pressable>
                <Pressable
                  style={({ pressed }) => [{
                    paddingVertical: 5,
                    paddingHorizontal: 10,
                    borderRadius: 5,
                    backgroundColor: pressed ? '#9e0230' : Colors.red,
                  }]}
                  onPress={() => this.handleDelete()}
                >
                  <Text style={{
                    color: Colors.white,
                    fontWeight: 'bold',
                  }}
                  >
                    Confirm
                  </Text>
                </Pressable>
              </View>
            </View>
          </Modal>
        </Form>
      </KeyboardAwareScrollView>
    );
  }
}
