import React, { Component } from 'react';
import {
  Platform, StyleSheet, Pressable, View,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Ionicons } from '@expo/vector-icons';
import { DateTime } from 'luxon';
import EventsApi from '../api/EventsApi';
import ImageUploadAPI from '../api/ImageUploadApi';
import Form from '../components/Form';
import colors from '../util/colors';
import { updateStateAndClearErrors } from '../util/formUtil';
import Validator from '../components/form/validation/Validator';
import { UserContext } from '../util/UserContext';

const styles = StyleSheet.create({
  newLinkButton: {
    backgroundColor: colors.grayScale2,
  },
  newLinkText: {
    color: colors.grayScale10,
  },
});

export default class EventCreation extends Component {
  static contextType = UserContext;

  validator = new Validator({
    name: [Validator.required(), Validator.minLength(3)],
    location: [Validator.validLocation()],
    facebookLink: [Validator.validFacebookUrl()],
    description: [Validator.required()],
    image: [],
    links: [Validator.itemsRequireFields(['name', 'link']), Validator.validLinks()],
  });

  constructor(props) {
    super(props);

    this.state = {
      name: '',
      date: DateTime.local(),
      location: {
        placeId: '',
        description: '',
        room: '',
        googleMapsUrl: '',
      },
      links: [],
      description: '',
      image: null,
      errors: {},
      processingRequest: false,
    };
  }

  updateLinks = (newLink, index) => {
    const { links } = this.state;

    const newLinks = [...links];
    newLinks[index] = newLink;

    updateStateAndClearErrors(this, 'links', newLinks);
  }

  createEvent = async () => {
    const validationResult = this.validator.validate(this.state);
    const { setMessage } = this.context;

    if (!validationResult.valid) {
      this.setState({ errors: validationResult.errors });

      return;
    }

    const {
      name,
      description,
      links,
      location,
      date,
      image,
    } = this.state;
    const { route, navigation } = this.props;

    const { club } = route.params;

    const eventObj = {
      club: club._id,
      name,
      description,
      links,
      date: date.toISO(),
      location,
    };

    if (image !== null) {
      const uploadedImage = await ImageUploadAPI.upload('event', image);
      eventObj.image = uploadedImage;
    }

    // set loading state to true
    this.setState({ processingRequest: true });

    await EventsApi.create(eventObj)
      .then((createEventResponse) => {
        // Handle event creation error.
        if (createEventResponse.error) {
          alert(`Unable to create event:\n ${createEventResponse.error}`);
        } else {
          setMessage('Event Created');
          navigation.pop();
        }
      })
      .catch((error) => {
        alert('Unexpected error occured. Please try again.');
        console.error('Unexpected error:', error);
      })
      .finally(() => {
        // set loading state back to false
        this.setState({ processingRequest: false });
      });
  };

  setShowDatePicker = () => {
    this.setState({ showDatePicker: true });
  };

  setShowTimePicker = () => {
    this.setState({ showTimePicker: true });
  };

  render() {
    const {
      name, date, location, links, description, errors, image, processingRequest,
    } = this.state;

    return (
      <KeyboardAwareScrollView
        resetScrollToCoords={{ x: 0, y: 0 }}
        style={{ width: '95%', alignSelf: 'center' }}
        keyboardShouldPersistTaps="handled"
      >
        <Form>
          <Form.Text
            placeholder="Name"
            value={name}
            onChange={(value) => updateStateAndClearErrors(this, 'name', value)}
            error={errors.name}
          />
          <Form.Date
            value={date}
            onChange={(value) => updateStateAndClearErrors(this, 'date', value)}
          />
          <Form.LocationPicker
            placeholder="Location"
            value={location.description}
            onChange={(value) => updateStateAndClearErrors(this, 'location', {
              ...location,
              placeId: value.placeId,
              description: value.description,
              googleMapsUrl: value.googleMapsUrl,
            })}
            error={errors.location}
          />
          <Form.Text
            placeholder="Room"
            value={location.room}
            onChange={(value) => updateStateAndClearErrors(this, 'location', { ...location, room: value })}
          />
          <Form.TextArea
            placeholder="Description"
            value={description}
            onChange={(value) => updateStateAndClearErrors(this, 'description', value)}
            error={errors.description}
          />
          {links.map((externalLink, index) => (
            <View
              key={externalLink._id}
              style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}
            >
              <Ionicons
                name="link-outline"
                size={20}
                color={colors.grayScale8}
                style={{ marginRight: 8 }}
              />
              <Form.Text
                value={externalLink.name}
                style={{ flex: 1, marginRight: 10 }}
                onChange={(value) => this.updateLinks(
                  {
                    link: externalLink.link,
                    name: value,
                  },
                  index,
                )}
                error={errors?.links?.name?.length > index ? errors.links.name[index] : null}
                placeholder="Link Name"
              />
              <Form.Text
                value={externalLink.link}
                style={{ flex: 1, marginRight: 10 }}
                onChange={(value) => this.updateLinks(
                  {
                    link: value,
                    name: externalLink.name,
                  },
                  index,
                )}
                error={
                  // Check for whether no link is given (Required error)
                  errors?.links?.link?.length > index ? errors.links.link[index]
                    // Check for whether link is valid (Invalid error)
                    : errors?.links?.length > index ? errors.links[index]
                      : null
                }
                placeholder="URL"
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType={Platform.OS === 'ios' ? 'url' : 'default'}
              />
              <Pressable
                style={{ width: '5%' }}
                hitSlop={20}
                onPress={() => {
                  updateStateAndClearErrors(this, 'links',
                    links.filter((otherLink) => externalLink !== otherLink));
                }}
              >
                <Ionicons
                  name="remove-circle-outline"
                  size={20}
                  color="red"
                />
              </Pressable>
            </View>
          ))}
          <Form.Button
            text="Add External Link"
            style={styles.newLinkButton}
            textStyle={styles.newLinkText}
            onPress={() => {
              updateStateAndClearErrors(this, 'links',
                [...(links || []), {
                  link: '', name: '',
                }]);
            }}
          />
          <Form.AttachmentImagePicker
            image={image}
            type="event"
            onImagePicked={(value) => updateStateAndClearErrors(this, 'image', value)}
            error={errors.image}
          />
          <Form.Button
            text={processingRequest ? 'Creating Event...' : 'Create Event'}
            onPress={this.createEvent}
            isLoading={processingRequest}
          />
        </Form>
      </KeyboardAwareScrollView>
    );
  }
}
