import React, { Component } from 'react';
import {
  Alert, Platform, StyleSheet, Pressable, View,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Ionicons } from '@expo/vector-icons';
import Form from '../components/Form';
import StandardColors from '../constants/Colors';
import colors from '../util/colors';
import { updateStateAndClearErrors } from '../util/formUtil';
import Validator from '../components/form/validation/Validator';
import EventsApi from '../api/EventsApi';
import ImageUploadAPI from '../api/ImageUploadApi';
import { UserContext } from '../util/UserContext';

const styles = StyleSheet.create({
  deleteButton: {
    backgroundColor: StandardColors.redFade,
  },
  updateButton: {
    backgroundColor: StandardColors.blue,
  },
  newLinkButton: {
    backgroundColor: colors.grayScale2,
  },
  newLinkText: {
    color: colors.grayScale10,
  },
});

export default class EditEvent extends Component {
  static contextType = UserContext;

  validator = new Validator({
    name: [Validator.required(), Validator.minLength(3)],
    location: [Validator.validLocation()],
    facebookLink: [Validator.validFacebookUrl('Must be valid facebook url')],
    description: [Validator.required()],
    image: [],
    links: [Validator.itemsRequireFields(['name', 'link']), Validator.validLinks()],
  });

  constructor(props) {
    super(props);

    this.state = {
      id: '',
      name: '',
      date: null,
      location: {
        placeId: '',
        description: '',
        room: '',
        googleMapsUrl: '',
      },
      links: [],
      description: '',
      image: null,
      processingRequest: { status: false, message: '' },
      processingDelete: { status: false, message: '' },
      errors: {},
    };
  }

  componentDidMount() {
    const { route } = this.props;
    const { event } = route.params;
    const {
      _id: id, name, date, location, links, description, image,
    } = event;

    this.setState({
      id,
      name,
      date,
      location,
      links,
      description,
      image,
    });
  }

  updateLinks = (newLink, index) => {
    const { links } = this.state;

    const newLinks = [...links];
    newLinks[index] = newLink;

    updateStateAndClearErrors(this, 'links', newLinks);
  }

  editEvent = async () => {
    const validationResult = this.validator.validate(this.state);
    const { setMessage } = this.context;
    const { route } = this.props;
    const { event } = route.params;

    if (!validationResult.valid) {
      this.setState({
        processingRequest: { status: false, message: '' },
        errors: validationResult.errors,
      });

      return;
    }

    this.setState({
      processingRequest: { status: true, message: 'Updating...' },
    });

    const {
      id, name, date, location, links, description, image,
    } = this.state;

    const updatedEventObj = {
      name,
      date,
      location,
      links,
      description,
    };

    if (image !== event.image) {
      const uploadedImage = await ImageUploadAPI.upload('event', image);
      updatedEventObj.image = uploadedImage;
    }

    const updateEventResponse = await EventsApi.update(
      id,
      updatedEventObj,
    );

    if (updateEventResponse.error) {
      // eslint-disable-next-line no-alert
      alert('Unable to update event');
      // eslint-disable-next-line no-console
      console.log(updateEventResponse.error);

      this.setState({
        processingRequest: { status: false, message: '' },
      });
    } else {
      setMessage('Event updated.');
      const { navigation } = this.props;
      navigation.pop(2);

      this.setState({
        processingRequest: { status: true, message: 'Saved!' },
      });
    }
  };

  deleteConfirmation = () => Alert.alert(
    'Delete Event?',
    'This action cannot be undone.',
    [
      {
        text: 'Cancel',
        style: 'cancel',
      },
      { text: 'OK', onPress: () => this.deleteEvent() },
    ],
    { cancelable: false },
  );

  deleteEvent = async () => {
    this.setState({
      processingDelete: { status: true, message: 'Deleting...' },
    });

    const { id } = this.state;
    const deleteEventResponse = await EventsApi.delete(id);
    if (deleteEventResponse.error) {
      // eslint-disable-next-line no-alert
      alert('Unable to delete event');
      // eslint-disable-next-line no-console
      console.log(deleteEventResponse.error);

      this.setState({
        processingDelete: { status: false, message: '' },
      });

      return;
    }

    this.setState({
      processingDelete: { status: false, message: '' },
    });

    // Pop the edit event screen and the event screen off the stack
    const { navigation } = this.props;
    navigation.pop(2);
  };

  render() {
    const {
      errors,
      name,
      date,
      location,
      links,
      description,
      image,
      processingRequest,
      processingDelete,
    } = this.state;

    return (
      <KeyboardAwareScrollView
        resetScrollToCoords={{ x: 0, y: 0 }}
        style={{ width: '95%', alignSelf: 'center' }}
        keyboardShouldPersistTaps="handled"
      >
        <Form>
          <Form.Text
            value={name}
            onChange={(value) => updateStateAndClearErrors(this, 'name', value)}
            placeholder="Event Name"
            error={errors.name}
          />
          <Form.Date
            value={date}
            onChange={(value) => updateStateAndClearErrors(this, 'date', value)}
          />
          <Form.LocationPicker
            placeholder="Location"
            value={location.description}
            onChange={(value) => updateStateAndClearErrors(this, 'location', {
              ...location,
              placeId: value.placeId,
              description: value.description,
              googleMapsUrl: value.googleMapsUrl,
            })}
            error={errors.location}
          />
          <Form.Text
            placeholder="Room"
            value={location.room}
            onChange={(value) => updateStateAndClearErrors(this, 'location', { ...location, room: value })}
          />
          <Form.TextArea
            value={description}
            onChange={(value) => updateStateAndClearErrors(this, 'description', value)}
            placeholder="Description"
            error={errors.description}
          />
          {links?.map((externalLink, index) => (
            <View
              key={externalLink._id}
              style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}
            >
              <Ionicons
                name="link-outline"
                size={20}
                color={colors.grayScale8}
                style={{ marginRight: 8 }}
              />
              <Form.Text
                value={externalLink.name}
                style={{ flex: 1, marginRight: 10 }}
                onChange={(value) => this.updateLinks(
                  {
                    link: externalLink.link,
                    name: value,
                  },
                  index,
                )}
                error={errors?.links?.name?.length > index ? errors.links.name[index] : null}
                placeholder="Link Name"
              />
              <Form.Text
                value={externalLink.link}
                style={{ flex: 1, marginRight: 10 }}
                onChange={(value) => this.updateLinks(
                  {
                    link: value,
                    name: externalLink.name,
                  },
                  index,
                )}
                error={
                  // Check for whether no link is given (Required error)
                  errors?.links?.link?.length > index ? errors.links.link[index]
                    // Check for whether link is valid (Invalid error)
                    : errors?.links?.length > index ? errors.links[index]
                      : null
                }
                placeholder="URL"
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType={Platform.OS === 'ios' ? 'url' : 'default'}
              />
              <Pressable
                style={{ width: '5%' }}
                hitSlop={20}
                onPress={() => {
                  updateStateAndClearErrors(this, 'links',
                    links.filter((otherLink) => externalLink !== otherLink));
                }}
              >
                <Ionicons
                  name="remove-circle-outline"
                  size={20}
                  color="red"
                />
              </Pressable>
            </View>
          ))}
          <Form.Button
            text="Add External Link"
            style={styles.newLinkButton}
            textStyle={styles.newLinkText}
            onPress={() => {
              updateStateAndClearErrors(this, 'links',
                [...(links || []), {
                  link: '', name: '',
                }]);
            }}
          />
          <Form.AttachmentImagePicker
            image={image}
            type="event"
            onImagePicked={(value) => updateStateAndClearErrors(this, 'image', value)}
            error={errors.image}
          />
          <Form.Button
            text={processingRequest.status ? processingRequest.message : 'Update'}
            style={styles.updateButton}
            onPress={() => this.editEvent()}
            isLoading={processingRequest.status}
          />
          <Form.Button
            text={processingDelete.status ? processingDelete.message : 'Delete'}
            style={styles.deleteButton}
            onPress={this.deleteConfirmation}
            isLoading={processingDelete.status}
          />
        </Form>
      </KeyboardAwareScrollView>
    );
  }
}
