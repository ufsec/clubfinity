import React, { Component } from 'react';
import {
  TouchableOpacity,
  ActivityIndicator,
  View,
  FlatList,
  ScrollView,
  StyleSheet,
} from 'react-native';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import {
  Box, Input, HStack, Text, Image,
} from 'native-base';
import CustomRefresh from '../components/CustomRefresh';
import ClubsApi from '../api/ClubsApi';
import EventsApi from '../api/EventsApi';
import { formatToTime } from '../util/dateUtil';
import colors from '../util/colors';
import defaultClubImage from '../assets/images/DefaultClubImage.png';
import { UserContext } from '../util/UserContext';

const { DateTime } = require('luxon'); // Remove once the placeholder announcement is removed

const placeholderAnnouncement = [
  {
    title: 'Announcement!',
    description: 'Big announcement today! I am happy to announce that our description takes up 3 lines.',
    date: DateTime.utc(2025, 3, 12, 0, 45),
    club: {
      name: 'Placeholder Club',
      isPrivate: false,
      category: 'Research',
      _id: '99cb91bdc3464f14678934ca',
      thumbnailUrl: 'https://i.ibb.co/F4rHdKN/sec-club-img.jpg',
    },
  },
]; // PLACEHOLDER FOR NOW, BECAUSE THERE IS CURRENTLY NO WAY TO GET ALL ANNOUNCEMENTS! FIX THIS!!
// Note: When this is fixed, ../components/CustomRefresh.js also needs to be modified

const searchFilters = [
  {
    name: 'Computer Science',
    icon: 'code-working-outline',
  },
  {
    name: 'Sports',
    icon: 'basketball-outline',
  },
  {
    name: 'Engineering',
    icon: 'build-outline',
  },
  {
    name: 'Art',
    icon: 'brush-outline',
  },
  {
    name: 'Research',
    icon: 'flask-outline',
  },
  {
    name: 'Business',
    icon: 'bar-chart-outline',
  },
  {
    name: 'Health',
    icon: 'medkit-outline',
  },
  {
    name: 'Journalism',
    icon: 'newspaper-outline',
  },
  {
    name: 'Liberal Arts',
    icon: 'library-outline',
  },
  {
    name: 'Cultural',
    icon: 'earth-outline',
  },
  {
    name: 'Honor Society',
    icon: 'school-outline',
  },
  {
    name: 'Media',
    icon: 'videocam-outline',
  },
  {
    name: 'Professional/Career',
    icon: 'briefcase-outline',
  },
  {
    name: 'Religious/Spiritual',
    icon: 'book-outline',
  },
  {
    name: 'Student Government',
    icon: 'people-outline',
  },
];

const featured = [
  '99cb91bdc3464f14678934ca', // Software Engineering Club
  '99ce7614c3464f14678934ca', // SHPE
  '99ce91b233464f14678934ca', // Open Source Club
];

const style = StyleSheet.create({
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    paddingTop: '1%',
  },
  search: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start',
    width: '90%',
    marginLeft: '5%',
    marginTop: '3%',
    marginBottom: '4%',
    height: 35,
    backgroundColor: 'transparent',
    borderBottomWidth: 0.9,
    borderColor: '#000000',
  },
  searchBar: {
    fontSize: 14,
  },
  categories: {
    height: 136,
    marginHorizontal: '3%',
  },
  category: {
    width: 75,
    height: 80,
    paddingTop: '5%',
  },
  categoryBox: {
    boxShadow: '0 0 4 rgba(0, 0, 0, 0.3)',
    borderRadius: 50,
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    width: 55,
    height: 55,
    margin: 3,
  },
  categoryText: {
    color: 'black',
    fontSize: 11,
    fontWeight: 600,
    textAlign: 'center',
    lineHeight: 15,
  },
  clubs: {
    height: 220,
    marginHorizontal: '3%',
  },
  club: {
    width: 185,
    height: 185,
    alignItems: 'center',
    justifyContent: 'center',
  },
  clubBox: {
    height: 170,
    width: 170,
    borderRadius: 15,
    overflow: 'hidden',
    boxShadow: '0 0 4 rgba(0, 0, 0, 0.3)',
  },
  clubTextBox: {
    position: 'absolute',
    width: '100%',
    height: 50,
    bottom: 0,
    backgroundColor: 'rgba(255, 255, 255, 0.9)',
    justifyContent: 'center',
  },
  clubName: {
    fontWeight: 'bold',
    marginHorizontal: 9,
    fontSize: 12,
    lineHeight: 13,
  },
  clubCategory: {
    marginHorizontal: 9,
    fontSize: 9,
    lineHeight: 13,
  },
  events: {
    height: 165,
    marginHorizontal: '3%',
  },
  event: {
    width: 220,
    height: 135,
    alignItems: 'center',
    justifyContent: 'center',
  },
  eventArea: {
    height: '83%',
    width: '93%',
  },
  eventBox: {
    height: '100%',
    width: '100%',
    backgroundColor: colors.grayScale0,
    borderRadius: 15,
    position: 'relative',
    boxShadow: '0 0 4 rgba(0, 0, 0, 0.3)',
  },
  eventInfo: {
    marginHorizontal: 13,
    top: 10,
  },
  eventTitle: {
    color: 'black',
    fontSize: 14,
    fontWeight: 600,
    lineHeight: 17,
  },
  eventClub: {
    color: colors.grayScale6,
    fontSize: 13,
    fontWeight: 600,
    lineHeight: 17,
  },
  eventDate: {
    marginHorizontal: 13,
    position: 'absolute',
    bottom: 9,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  eventDateText: {
    marginLeft: 6,
    lineHeight: 23,
    fontSize: 13,
    fontWeight: 600,
  },
  announcements: {
    height: 175,
    marginHorizontal: '3%',
  },
  announcement: {
    width: 330,
    height: 130,
    alignItems: 'center',
    justifyContent: 'center',
  },
  announcementArea: {
    width: '95%',
    height: '83%',
  },
  announcementBox: {
    height: '100%',
    width: '100%',
    backgroundColor: colors.grayScale0,
    borderRadius: 15,
    position: 'relative',
    boxShadow: '0 0 4 rgba(0, 0, 0, 0.3)',
  },
  announcementHeader: {
    marginHorizontal: 13,
    top: 11,
    flexDirection: 'row',
  },
  announcementClub: {
    color: 'black',
    fontSize: 14,
    fontWeight: 600,
    lineHeight: 17,
    width: '60%',
  },
  announcementDate: {
    color: colors.grayScale6,
    fontSize: 13,
    lineHeight: 17,
    textAlign: 'right',
    width: '40%',
  },
  announcementLower: {
    marginHorizontal: 15,
    marginTop: 19,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  announcementIcon: {
    borderRadius: 35,
  },
  announcementDescription: {
    marginLeft: 15,
    lineHeight: 18,
    fontSize: 13,
    fontWeight: 600,
    width: '70%',
  },
});

export default class DiscoverScr extends Component {
  static contextType = UserContext;

  constructor(props) {
    super(props);
    this.state = {
      category: '',
      searchText: '',
      clubs: [],
      events: [],
      announcements: [],
      allClubs: [],
      allEvents: [],
      allAnnouncements: [],
      isLoading: true,
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    navigation.addListener('willFocus', this.onFocus);
    this.onFocus();
  }

  changeHandler = (newState) => {
    // applies proper filtering on refresh
    const filteredClubs = this.filterClubs(newState.allClubs);
    const filteredEvents = this.filterEvents(newState.allEvents);
    const filteredAnnouncements = placeholderAnnouncement;

    const featuredClubs = this.filterFeatured(filteredClubs);

    this.setState({
      allClubs: filteredClubs,
      clubs: featuredClubs,
      allEvents: filteredEvents,
      events: filteredEvents,
      allAnnouncements: filteredAnnouncements,
      announcements: filteredAnnouncements,
      category: '',
    });
  };

  filterClubs = (clubData) => {
    // get users's clubs
    const { user } = this.context;
    const userClubs = user.clubs.map((club) => club._id);

    // filter out all private clubs that the user is not in
    const filteredClubs = clubData.filter(
      (club) => !club.isPrivate || userClubs.includes(club._id),
    );

    return filteredClubs;
  };

  filterEvents = (eventData) => {
    // get users's clubs
    const { user } = this.context;
    const userClubs = user.clubs.map((club) => club._id);

    // filter out events from hidden clubs and events that have already happened
    const filteredEvents = eventData.filter(
      (event) => event.club && (
        !event.club.isPrivate || userClubs.includes(event.club._id)
      ) && event.date > Date.now(),
    );

    // sort events from earliest to latest
    const sortedEvents = filteredEvents.sort((a, b) => a.date - b.date);

    return sortedEvents;
  };

  filterAnnouncements = (announcementData) => {
    // get users's clubs
    const { user } = this.context;
    const userClubs = user.clubs.map((club) => club._id);

    // filter out announcements from hidden clubs
    const filteredAnnouncements = announcementData.filter(
      (announcement) => announcement.club && (
        !announcement.club.isPrivate || userClubs.includes(announcement.club._id)
      ),
    );

    // sort announcements from latest to earliest
    const sortedAnnouncements = filteredAnnouncements.sort((a, b) => b.date - a.date);

    return sortedAnnouncements;
  };

  filterFeatured = (clubsData, category) => {
    // for page without searching, show featured clubs in the chosen category
    const filteredClubs = clubsData.filter(
      (club) => (featured.includes(club._id)) && (!category || club.category === category),
    );

    return filteredClubs;
  }

  filterCategory = (itemsData, category) => {
    // for page without searching, show events/announcements in the chosen category
    const filteredItems = itemsData.filter(
      (item) => (!category || item?.club.category === category),
    );

    return filteredItems;
  }

  onFocus = async () => {
    try {
      const clubData = await ClubsApi.getAllClubs();
      const eventData = await EventsApi.getAllEvents();
      const announcementData = placeholderAnnouncement;

      const filteredClubs = await this.filterClubs(clubData);
      const filteredEvents = await this.filterEvents(eventData);
      const filteredAnnouncements = await this.filterAnnouncements(announcementData);

      const featuredClubs = this.filterFeatured(filteredClubs);
      this.setState({
        allClubs: filteredClubs,
        clubs: featuredClubs,
        allEvents: filteredEvents,
        events: filteredEvents,
        allAnnouncements: filteredAnnouncements,
        announcements: filteredAnnouncements,
        isLoading: false,
      });
    } catch (error) {
      this.setState({ isLoading: false });
    }
  };

  handlePickerChange = async (value) => {
    const {
      allClubs, allEvents, allAnnouncements, category,
    } = this.state;
    if (category === value) {
      this.setState({
        category: '',
        clubs: this.filterFeatured(allClubs, ''),
        events: this.filterCategory(allEvents, ''),
        announcements: this.filterCategory(allAnnouncements, ''),
      });
    } else {
      this.setState({
        category: value,
        clubs: this.filterFeatured(allClubs, value),
        events: this.filterCategory(allEvents, value),
        announcements: this.filterCategory(allAnnouncements, value),
      });
    }
  };

  handleSelect = (item, type) => {
    const { navigation } = this.props;
    const routeMap = {
      club: { route: 'Club', param: 'club' },
      event: { route: 'Event', param: 'event' },
      announcement: { route: 'Announcement', param: 'announcement' },
    };

    navigation.navigate(routeMap[type].route, {
      [routeMap[type].param]: item,
    });
  };

  handleSearch = (text) => {
    // for page with searching, show clubs that contain the search text
    const {
      allClubs, allEvents, allAnnouncements, category,
    } = this.state;
    this.setState({
      clubs: (text) ? allClubs.filter(
        (club) => club.name.toLowerCase().includes(text.toLowerCase()),
      ) : this.filterFeatured(allClubs, category),
    });
    // show events that contain the search text or are in clubs containing the search text
    this.setState({
      events: (text) ? allEvents.filter(
        (event) => event.name.toLowerCase().includes(text.toLowerCase())
        || event.club.name.toLowerCase().includes(text.toLowerCase()),
      ) : this.filterCategory(allEvents, category),
    });
    // show announcements that contain the search text or are in clubs containing the search text
    this.setState({
      announcements: (text) ? allAnnouncements.filter(
        (announcement) => announcement.title.toLowerCase().includes(text.toLowerCase())
        || announcement.club.name.toLowerCase().includes(text.toLowerCase())
        || announcement.description.toLowerCase().includes(text.toLowerCase()),
      ) : this.filterCategory(allAnnouncements, category),
    });
  };

  render() {
    const {
      isLoading, searchText, category, clubs, events, announcements,
    } = this.state;

    const filter = (text) => {
      this.setState({
        searchText: text,
      });
      this.handleSearch(text);
    };

    if (isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator />
        </View>
      );
    }

    return (
      <ScrollView
        style={{ backgroundColor: '#f5f6fa', flex: 1 }}
        refreshControl={(
          <CustomRefresh
            changeHandler={this.changeHandler}
            reqs={{
              screen: 'DiscoverScr',
              searchText,
            }}
          />
        )}
      >
        <HStack
          rounded
          searchBar
          style={{
            flexDirection: 'column',
            borderBottomWidth: 0,
          }}
        >
          <Box style={style.search}>
            <Input
              placeholder="Discover Clubs"
              onChangeText={filter}
              value={searchText}
              autoCorrect={false}
              variant="unstyled"
              style={style.searchBar}
              InputLeftElement={(
                <Ionicons
                  name="search"
                  size={20}
                  style={{
                    marginLeft: '2%',
                  }}
                  color={colors.grayScale8}
                />
              )}
              InputRightElement={
                searchText !== '' ? (
                  <TouchableOpacity onPress={() => filter('')}>
                    <MaterialIcons
                      name="clear"
                      size={20}
                      style={{
                        marginTop: '1%',
                        marginRight: '3%',
                      }}
                      color={colors.grayScale8}
                    />
                  </TouchableOpacity>
                ) : null
              }
            />
          </Box>
        </HStack>
        {!searchText && (
        <View style={style.categories}>
          <Text style={style.title}>Categories</Text>
          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            data={searchFilters}
            renderItem={({ item }) => {
              const selected = category === item.name;
              return (
                <View style={style.category}>
                  <TouchableOpacity
                    onPress={() => this.handlePickerChange(item.name)}
                  >
                    <Box style={[style.categoryBox, {
                      backgroundColor: selected ? colors.primary0 : colors.grayScale0,
                    }]}
                    >
                      <Ionicons
                        name={item.icon}
                        size={31}
                        color={selected ? colors.grayScale0 : colors.grayscale12}
                      />
                    </Box>
                    <Text style={style.categoryText}>
                      {item.name}
                    </Text>
                  </TouchableOpacity>
                </View>
              );
            }}
          />
        </View>
        )}
        {clubs.length > 0 && (
        <View style={style.clubs}>
          <Text style={style.title}>{searchText ? 'Clubs' : 'Featured'}</Text>
          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            data={clubs}
            renderItem={({ item }) => (
              <View
                style={style.club}
              >
                <TouchableOpacity
                  onPress={() => this.handleSelect(item, 'club')}
                >
                  <Box style={style.clubBox}>
                    <Image
                      size="170"
                      source={item?.thumbnailUrl ? { uri: item.thumbnailUrl } : defaultClubImage}
                      alt="react-native"
                      key={Date.now()}
                    />
                    <Box style={style.clubTextBox}>
                      <Text
                        style={style.clubName}
                        numberOfLines={2}
                      >
                        {item.name}
                      </Text>
                      <Text
                        style={style.clubCategory}
                        numberOfLines={1}
                      >
                        {item.category}
                      </Text>
                    </Box>
                  </Box>
                </TouchableOpacity>
              </View>
            )}
          />
        </View>
        )}
        {events.length > 0 && (
        <View style={style.events}>
          <Text style={style.title}>Upcoming Events</Text>
          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            data={events}
            renderItem={({ item }) => (
              <View style={style.event}>
                <TouchableOpacity
                  style={style.eventArea}
                  onPress={() => this.handleSelect(item, 'event')}
                >
                  <Box style={style.eventBox}>
                    <View style={style.eventInfo}>
                      <Text
                        style={style.eventTitle}
                        numberOfLines={2}
                      >
                        {item.name}
                      </Text>
                      <Text
                        style={style.eventClub}
                        numberOfLines={2}
                      >
                        {item.club.name}
                      </Text>
                    </View>
                    <View style={style.eventDate}>
                      <Ionicons
                        name="calendar-clear-outline"
                        size={23}
                      />
                      <Text
                        style={style.eventDateText}
                        numberOfLines={1}
                      >
                        {`${item.date.monthShort} ${item.date.day} | ${formatToTime(item.date)}`}
                      </Text>
                    </View>
                  </Box>
                </TouchableOpacity>
              </View>
            )}
          />
        </View>
        )}
        {announcements.length > 0 && (
        <View style={style.announcements}>
          <Text style={style.title}>New Announcements</Text>
          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            data={announcements}
            renderItem={({ item }) => (
              <View style={style.announcement}>
                <TouchableOpacity
                  style={style.announcementArea}
                  onPress={() => this.handleSelect(item, 'announcement')}
                >
                  <Box style={style.announcementBox}>
                    <View style={style.announcementHeader}>
                      <Text
                        style={style.announcementClub}
                        numberOfLines={1}
                      >
                        {item.club.name}
                      </Text>
                      <Text
                        style={style.announcementDate}
                        numberOfLines={1}
                      >
                        {`${item.date.monthShort} ${item.date.day} | ${formatToTime(item.date)}`}
                      </Text>
                    </View>
                    <View style={style.announcementLower}>
                      <Image
                        size="60"
                        source={item?.club.thumbnailUrl ? { uri: item.club.thumbnailUrl } : defaultClubImage}
                        alt="react-native"
                        key={Date.now()} // Allows images to update immediately
                        style={style.announcementIcon}
                      />
                      <Text
                        style={style.announcementDescription}
                        numberOfLines={3}
                      >
                        {item.description}
                      </Text>
                    </View>
                  </Box>
                </TouchableOpacity>
              </View>
            )}
          />
        </View>
        )}
      </ScrollView>
    );
  }
}
