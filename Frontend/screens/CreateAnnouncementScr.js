/* eslint-disable linebreak-style */
import React, { Component } from 'react';
import { Center, Container } from 'native-base';
import { DateTime } from 'luxon';
import AnnouncementsApi from '../api/AnnouncementsApi';
import Form from '../components/Form';
import Validator from '../components/form/validation/Validator';
import { updateStateAndClearErrors } from '../util/formUtil';
import { UserContext } from '../util/UserContext';

export default class CreateAnnouncementScr extends Component {
  static contextType = UserContext;

  validator = new Validator({
    title: [Validator.required()],
    description: [Validator.required()],
  });

  constructor(props) {
    super(props);
    this.state = {
      title: '',
      description: '',
      errors: {},
      processingRequest: false,
    };
  }

  handleCreateAnnouncement = async () => {
    const { route, navigation } = this.props;
    const { title, description } = this.state;
    const { club } = route.params;

    const { setMessage } = this.context;

    const announcementObj = {
      title,
      description,
      date: DateTime.local(),
      club: club._id,
    };

    const validationResults = this.validator.validate(this.state);

    if (!validationResults.valid) {
      this.setState({ errors: validationResults.errors });
      return;
    }
    // set loading state to true
    this.setState({ processingRequest: true });

    await AnnouncementsApi.create(announcementObj)
      .then((createAnnouncementResponse) => {
        // error passed through API call
        if (createAnnouncementResponse.error) {
          alert(`Error:\n ${createAnnouncementResponse.error}`);
        } else {
          setMessage('Announcement created.');
          navigation.pop();
        }
      })
      .catch((error) => {
        // any other errors
        alert('Unexpected error occured. Please try again.');
        console.error('Unexpected error:', error);
      })
      .finally(() => {
        // set loading state back to false
        this.setState({ processingRequest: false });
      });
  };

  render() {
    const {
      title, description, errors, processingRequest,
    } = this.state;
    return (
      <Center>
        <Container>
          <Form>
            <Form.Text
              placeholder="Title"
              value={title}
              onChange={(value) => updateStateAndClearErrors(this, 'title', value)}
              error={errors.title}
            />
            <Form.TextArea
              placeholder="Description"
              value={description}
              onChange={(value) => updateStateAndClearErrors(this, 'description', value)}
              error={errors.description}
            />
            <Form.Button
              text={processingRequest ? 'Creating Announcement...' : 'Create Announcement'}
              onPress={this.handleCreateAnnouncement}
              isLoading={processingRequest}
            />
          </Form>
        </Container>
      </Center>
    );
  }
}
