import React from 'react';
import {
  View,
  SectionList,
  TouchableOpacity,
  Text,
} from 'react-native';
import {
  Input,
  Image,
  Avatar,
} from 'native-base';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import RenderHTML from 'react-native-render-html';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { UserContext } from '../util/UserContext';
import colors from '../util/colors';
import Layout from '../constants/Layout';
import defaultProfileImage from '../assets/images/DefaultUserImage.png';
import defaultClubImage from '../assets/images/DefaultClubImage.png';

const LINE_CHARACTER_REQ = 40;

export default class ProfileScr extends React.Component {
  static contextType = UserContext;

  constructor(props) {
    super(props);
    this.state = {
      searchText: '',
    };
  }

  filterFollowing = (followedClubs) => {
    const { searchText } = this.state;
    const text = searchText.toLowerCase();
    const filteredClubs = followedClubs.filter((club) => club.name.toLowerCase().includes(text));
    return filteredClubs;
  };

  renderSearch = () => {
    const { searchText } = this.state;
    return (
      <View
        style={{
          backgroundColor: 'transparent',
          marginHorizontal: '4%',
          marginTop: '1%',
          borderWidth: 0,
        }}
      >
        <View
          style={{
            position: 'absolute',
            alignSelf: 'center',
            borderBottomWidth: 0.5,
            borderColor: '#000000',
            bottom: 0,
            width: '90%',
          }}
        />
        <Input
          placeholder="Search following"
          onChangeText={(text) => this.setState({ searchText: text })}
          value={searchText}
          autoCorrect={false}
          variant="unstyled"
          InputLeftElement={(
            <Ionicons
              name="search"
              size={20}
              style={{
                marginLeft: '2%',
                marginTop: '1%',
              }}
              color={colors.grayScale8}
            />
          )}
          InputRightElement={
            (searchText !== '')
              ? (
                <TouchableOpacity onPress={() => this.setState({ searchText: '' })}>
                  <MaterialIcons
                    name="clear"
                    size={20}
                    style={{
                      marginTop: '1%',
                      marginRight: '5%',
                    }}
                    color={colors.grayScale8}
                  />
                </TouchableOpacity>
              )
              : null
          }
          style={{
            fontSize: 14,
            marginLeft: '2%',
            height: '100%',
            width: '100%',
          }}
        />
      </View>
    );
  };

  handleClubSelect = (club) => {
    const { navigation } = this.props;
    navigation.navigate('Club', { club });
  };

  renderClubListEmptyState = () => (
    <View
      style={{
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: '35%',
      }}
    >
      <Text style={{ color: colors.grayScale8 }}>
        You are not following or managing any clubs yet!
      </Text>
    </View>
  );

  renderSectionHeader = (section) => (
    <Text style={{
      fontSize: 16, paddingLeft: '4%', paddingTop: '1%', color: colors.grayScale7, backgroundColor: '#f5f6fa',
    }}
    >
      {section.title}
    </Text>
  )

  renderClubList = (clubs) => {
    const sectionListData = [];
    const { user } = this.context;

    const managingClubs = this.filterFollowing(clubs).filter(
      (item) => item.admins.some((admin) => admin._id === user._id),
    );
    if (managingClubs.length > 0) {
      sectionListData.push({ title: 'Managing', data: managingClubs });
    }

    const followingclubs = this.filterFollowing(clubs).filter(
      (following) => !managingClubs.some((managing) => managing._id === following._id),
    );
    if (followingclubs.length > 0) {
      sectionListData.push({ title: 'Following', data: followingclubs });
    }

    return (
      <KeyboardAwareScrollView>
        <SectionList
          sections={sectionListData}
          keyExtractor={(club) => club._id}
          style={{ paddingTop: '2%' }}
          ListEmptyComponent={this.renderClubListEmptyState}
          renderSectionHeader={({ section }) => this.renderSectionHeader(section)}
          renderItem={({ item }) => (
            <TouchableOpacity
              onPress={() => this.handleClubSelect(item)}
              style={{
                width: '95%',
                alignSelf: 'center',
              }}
            >
              <View
                style={{
                  width: '100%',
                  alignSelf: 'center',
                  display: 'flex',
                  flexDirection: 'row',
                }}
              >
                <View
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    width: '100%',
                    alignItems: 'center',
                    marginVertical: 5,
                    marginHorizontal: 2,
                    flexWrap: 'nowrap',
                    backgroundColor: 'white',
                    shadowColor: '#000',
                    shadowOffset: { width: 0, height: 2 },
                    shadowOpacity: 0.1,
                    shadowRadius: 1.5,
                    elevation: 3,
                    borderRadius: 5,
                  }}
                >
                  <View style={{ alignItems: 'center', padding: '4%' }}>
                    <Avatar
                      size="60"
                      source={item.thumbnailUrl ? { uri: item.thumbnailUrl } : defaultClubImage}
                      backgroundColor="white"
                      alignItems="center"
                    />
                  </View>
                  <View style={{ display: 'flex', flexDirection: 'column', paddingVertical: '2%' }}>
                    <View
                      style={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'flex-start',
                      }}
                    >
                      <Text style={{ fontSize: 16 }}>
                        {item.name}
                      </Text>
                      <Text style={{ fontSize: 12, color: colors.grayScale8 }}>
                        {item.category}
                      </Text>
                    </View>
                    <View style={{ paddingBottom: '2%', paddingTop: '8%' }}>
                      <Text
                        style={{ fontSize: 14, color: colors.grayScale8 }}
                      >
                        {item.description.length > LINE_CHARACTER_REQ
                        // eslint-disable-next-line max-len
                          ? (
                            <RenderHTML
                              source={{ html: `${item.description.substring(0, LINE_CHARACTER_REQ).trim()}...` }}
                              contentWidth={Layout.window.width}
                            />
                          )
                          : (
                            <RenderHTML
                              source={{ html: item.description.trim() }}
                              contentWidth={Layout.window.width}
                            />
                          )}
                      </Text>
                    </View>
                  </View>
                  <View style={{ marginLeft: 'auto' }}>
                    <Ionicons
                      name="chevron-forward-outline"
                      size={30}
                      style={{ paddingRight: '5%' }}
                    />
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          )}
        />
      </KeyboardAwareScrollView>
    );
  }

  render() {
    const { user } = this.context;

    return (
      <View
        style={{
          backgroundColor: '#f5f6fa',
          flex: 1,
          margin: 'auto',
        }}
      >
        {/* Profile Info */}
        <View
          style={{
            flex: 1,
            alignItems: 'center',
          }}
        >
          <View
            style={{
              width: '100%',
              alignItems: 'center',
              justifyContent: 'center',
              borderBottomRightRadius: 20,
              borderBottomLeftRadius: 20,
              backgroundColor: 'white',
              flexWrap: 'nowrap',
              shadowColor: '#000',
              shadowOffset: { width: 0, height: 2 },
              shadowOpacity: 0.1,
              shadowRadius: 1.5,
              elevation: 3,
            }}
          >
            <View style={{ paddingTop: '2%' }}>
              <Image
                size="120"
                source={user.profileImage ? { uri: user.profileImage } : defaultProfileImage}
                style={{ borderRadius: 63 }}
                alt="user profile image"
              />
            </View>
            <Text style={{
              color: '#000',
              fontSize: 27,
              paddingBottom: '2%',
              paddingTop: '5%',
            }}
            >
              {`${user.name.first} ${user.name.last}`}
            </Text>
            <Text style={{ paddingBottom: '5%', fontSize: 16 }}>{user.major}</Text>
          </View>
        </View>

        {/* Grid */}

        {this.renderSearch()}

        {this.renderClubList(user.clubs)}

      </View>
    );
  }
}
