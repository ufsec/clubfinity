import React, {
  useContext, useEffect, useState, useRef, useCallback,
} from 'react';
import {
  View, SafeAreaView, ScrollView, TouchableOpacity, StyleSheet, Linking, Modal,
} from 'react-native';
import {
  Button, Box, Text, Heading, Image, VStack, Flex, Center,
} from 'native-base';
import { Ionicons } from '@expo/vector-icons';
import * as Clipboard from 'expo-clipboard';
import BottomSheet, { BottomSheetBackdrop } from '@gorhom/bottom-sheet';
import { Portal } from '@gorhom/portal';
import RenderHTML from 'react-native-render-html';
import QRCode from 'react-native-qrcode-svg';
import { useNavigation } from '@react-navigation/native';
import { DateTime } from 'luxon';
import { DeepLinkingContext } from '../util/DeepLinkingContext';
import colors from '../util/colors';
import Layout from '../constants/Layout';
import { UserContext } from '../util/UserContext';
import UserApi from '../api/UserApi';
import ClubsApi from '../api/ClubsApi';
import saveImage from '../util/savePhotoToLocalStorage';
import AdminRow from '../components/AdminRow';
import CustomRefresh from '../components/CustomRefresh';
import defaultClubImage from '../assets/images/DefaultClubImage.png';
import { getHeaderRight } from '../navigation/helpers/headerRight';

const LINE_CHARACTER_REQ = 30;
const styles = StyleSheet.create({
  clubView: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  adminButton: {
    marginTop: '4%',
    alignSelf: 'center',
    backgroundColor: colors.secondary0,
    width: '85%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  clubScrCard: {
    width: '85%',
    marginTop: '10%',
    borderTopColor: colors.primary0,
    borderTopWidth: 10,
    borderRadius: 3,
    padding: '4%',
    borderWidth: 1,
    borderColor: 'lightgray',
  },
  noAnnouncementsEventsMessage: {
    alignSelf: 'center',
    opacity: 0.7,
    marginBottom: '5%',
    fontSize: 14,
  },
  listTextWrapper: {
    width: '100%',
    marginLeft: '0%',
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    padding: '2%',
    paddingTop: '2%',
    alignItems: 'center',
  },
  fullWidth: {
    width: '100%',
  },
  iconContainer: {
    paddingTop: '1.5%',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginHorizontal: 10,
  },
  iconCircle: {
    width: 60,
    height: 60,
    borderRadius: 30,
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 2,
    borderColor: '#D3D3D3',
  },
  iconText: {
    marginTop: 5,
    fontSize: 12,
    textAlign: 'center',
    flexWrap: 'wrap',
    width: 75,
  },
  modalContainer: {
    flex: 1,
    paddingTop: '50%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  qrCodeBox: {
    backgroundColor: '#FFFFFF',
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    width: 270,
    height: 270,
  },
  doneButton: {
    marginTop: '35%',
    backgroundColor: colors.primary0,
    paddingHorizontal: '25%',
    paddingVertical: '4%',
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },
  doneButtonText: {
    color: 'white',
    fontSize: 18,
  },
  qrCodeText: {
    marginTop: 10,
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

const ClubScr = ({ route }) => {
  const { club: clubParams } = route.params;

  const { user, setUser } = useContext(UserContext);
  const navigation = useNavigation();

  const [isFollowing, setIsFollowing] = useState(false);
  const [isAdmin, setIsAdmin] = useState(false);
  const [isPrivate, setIsPrivate] = useState(false);
  const [club, setClub] = useState(clubParams);
  const [events, setEvents] = useState([]);
  const [announcements, setAnnouncements] = useState([]);
  const announcementsEmpty = announcements.length === 0;
  const eventsEmpty = events.length === 0;
  // Page refers to first page for announcements/events for pagination
  const page = 1;
  // Pagesize used to fetch first "x" documents (closest events,
  // or most recent announcements)
  const pageSize = 5;
  // Want to pull upcoming events
  const eventType = 'upcoming';

  // Bottom Sheet
  const [bottomSheet, setBottomSheet] = useState(false);
  const bottomSheetRef = useRef(null);
  const qrCodeRef = useRef(null);
  const [qrCodeVisible, setQRCodeVisible] = useState(false);
  // download QR Code does not currently work. We may remove it.
  const bottomSheetIcons = [
    { name: 'link-outline', text: 'Copy Link' },
    // { name: "download-outline", text: "Download QR Code" },
    { name: 'eye-outline', text: 'Show QR Code' },
  ];
  // check if the social media links have been set up
  if (club?.slackLink) {
    bottomSheetIcons.push({ name: 'logo-slack', text: 'Share to Slack' });
  }
  if (club?.facebookLink) {
    bottomSheetIcons.push({ name: 'logo-facebook', text: 'Share to Facebook' });
  }
  if (club?.instagramLink) {
    bottomSheetIcons.push({
      name: 'logo-instagram',
      text: 'Share to Instagram',
    });
  }

  const openBottomSheet = () => {
    setBottomSheet(true);
    bottomSheetRef.current?.expand();
  };

  const closeBottomSheet = () => bottomSheetRef.current.close();

  // Function to run on each render -> fetch latest events/annoucements
  const onFocus = async () => {
    // Get latest announcements and closest events to show
    const {
      events: closestEvents,
      announcements: recentAnnouncements,
    } = await ClubsApi.getPosts(clubParams._id, page, pageSize, eventType);
    setEvents(closestEvents);
    setAnnouncements(recentAnnouncements);
  };

  // Check if current user is an admin in the club param
  const checkIfAdmin = (clubObj) => {
    if (clubObj.admins.map((admin) => admin._id).includes(user._id)) {
      setIsAdmin(true);
    } else {
      setIsAdmin(false);
    }
  };

  const checkIfPrivate = (clubObj) => {
    setIsPrivate(clubObj.isPrivate);
  };

  useEffect(() => {
    // Set initial user state regarding club (following? / is admin?)
    user.clubs.forEach((c) => {
      if (c._id === clubParams._id) {
        setIsFollowing(true);
        checkIfAdmin(c);
      }
    });
    checkIfPrivate(clubParams);
    // Set up onFocus function to fetch new events/announcements on each new render
    navigation.addListener('willFocus', onFocus);
    navigation.addListener('focus', onFocus);
    onFocus();
  }, []);

  useEffect(() => {
    navigation.setOptions(
      getHeaderRight('ClubScr', navigation, openBottomSheet),
    );
  }, [navigation]);

  // Callback function passed to sub-screens to update club screen state
  const updateClubState = (updatedClub) => {
    checkIfAdmin(updatedClub);
    setClub(updatedClub);
    checkIfPrivate(updatedClub);

    const updatedUserClubs = user.clubs.map((userClub) => {
      if (userClub._id === updatedClub._id) {
        return updatedClub;
      }
      return userClub;
    });
    setUser({ ...user, clubs: updatedUserClubs });
  };

  // Handle follow/unfollow API call
  const handleFollowUpdate = async (apiCall, clubId) => {
    const authResponse = await apiCall(clubId);
    setUser(authResponse.data.data);
    return authResponse;
  };

  // Handle follow/unfollow user button action
  const followBtnHandler = async () => {
    if (!isFollowing) {
      const authResponse = handleFollowUpdate(UserApi.followClub, club?._id);
      if (!authResponse.error) {
        setIsFollowing(true);
      } else {
        console.log('todo: error handling');
      }
    } else {
      handleFollowUpdate(UserApi.unfollowClub, club?._id);
      setIsFollowing(false);
    }
  };

  // Handles any state updates coming from CustomRefresh compoenent (when pull down to refresh)
  const changeHandler = (newState) => {
    const {
      events: updatedEvents,
      announcements: updatedAnnouncements,
      isAdmin: isAdminUpdated,
      admins: updatedAdmins,
      tags: updatedTags,
      name: updatedName,
      facebookLink: updatedFacebookLink,
      googleCalendarId: updatedGoogleCalendarId,
      instagramLink: updatedInstagramLink,
      slackLink: updatedSlackLink,
      description: updatedDescription,
      category: updatedCategory,
      thumbnailUrl: updatedThumbnailUrl,
    } = newState;

    const updatedClub = {
      ...club,
      admins: updatedAdmins,
      tags: updatedTags,
      name: updatedName,
      facebookLink: updatedFacebookLink,
      googleCalendarId: updatedGoogleCalendarId,
      instagramLink: updatedInstagramLink,
      slackLink: updatedSlackLink,
      description: updatedDescription,
      category: updatedCategory,
      thumbnailUrl: updatedThumbnailUrl,
    };

    updateClubState(updatedClub);
    setEvents(updatedEvents);
    setAnnouncements(updatedAnnouncements.reverse());
    setIsAdmin(isAdminUpdated);
  };

  const renderSocialMediaLink = (link, icon, text) => {
    const { name, size, marginRight } = icon;

    return (
      <TouchableOpacity onPress={() => Linking.openURL(link)}>
        <Flex p="2" justify="center" align="center" direction="row">
          <Ionicons name={name} size={size} style={{ marginRight }} />
          <Text paddingLeft="5%" fontSize="md">
            {text}
          </Text>
        </Flex>
      </TouchableOpacity>
    );
  };

  const renderClubLink = (link) => (
    <Button
      onPress={async () => {
        await Clipboard.setStringAsync(link);
      }}
    >
      Copy
    </Button>
  );

  const { createUrl } = useContext(DeepLinkingContext);
  const renderConnectCard = () => {
    if (!club?.slackLink && !club?.facebookLink && !club?.instagramLink) {
      return null;
    }

    // Create url from club's name (ex: "Puppy Club" -> "/club/puppy_club")
    const rawClubName = club.name;
    const nameForUrl = rawClubName.toLowerCase().replace(/ /g, '_');
    const redirectUrl = createUrl('club', nameForUrl);

    return (
      <Box style={styles.clubScrCard}>
        <Box py="2">
          <Text fontSize="lg" bold>
            Connect
          </Text>
        </Box>
        <VStack alignItems="flex-start">
          {club.socialLinks.map((socialLink) => renderSocialMediaLink(
            socialLink.link,
            {
              name: `logo-${socialLink.social.name.toLowerCase()}`,
              size: 27,
              marginRight: '4%',
            },
            socialLink.social.name,
          ))}
        </VStack>
        {renderClubLink(redirectUrl)}
        <Center margin={3}>
          <QRCode
            value={redirectUrl}
            getRef={(ref) => (qrCodeRef.current = ref)}
          />
        </Center>
      </Box>
    );
  };

  const renderBackdrop = useCallback(
    (props) => (
      <BottomSheetBackdrop
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...props}
        disappearsOnIndex={-1}
        appearsOnIndex={0}
        opacity={0.5}
      />
    ),
    [],
  );

  const renderQRCode = (redirectUrl) => (
    <Modal
      visible={qrCodeVisible}
      transparent
      animationType="fade"
      onRequestClose={() => setQRCodeVisible(false)}
    >
      <View style={styles.modalContainer}>
        <View style={styles.qrCodeBox}>
          <QRCode
            value={createUrl(redirectUrl)}
            size={200}
            getRef={(ref) => (qrCodeRef.current = ref)}
          />
          <Text style={styles.qrCodeText}>{club?.name}</Text>
        </View>
        <TouchableOpacity
          style={styles.doneButton}
          onPress={() => setQRCodeVisible(false)}
        >
          <Text style={styles.doneButtonText}>Done</Text>
        </TouchableOpacity>
      </View>
    </Modal>
  );

  async function handleIconPress(iconName) {
    const rawClubName = club.name;
    const nameForUrl = rawClubName.toLowerCase().replace(/ /g, '_');
    const redirectUrl = createUrl('club', nameForUrl);
    switch (iconName) {
      case 'link-outline': {
        await Clipboard.setStringAsync(redirectUrl);
        closeBottomSheet();
        break;
      }
      case 'download-outline': {
        if (qrCodeRef.current) {
          // must close bottom sheet here because it will wait for qrCodeUri
          closeBottomSheet();
          try {
            const qrCodeBase64Data = await new Promise((resolve, reject) => {
              qrCodeRef.current.toDataURL((data) => {
                if (data) {
                  resolve(`data:image/png;base64,${data}`);
                } else {
                  reject(new Error('QR code generation error'));
                }
              });
            });
            await saveImage(qrCodeBase64Data, `${club?.name}_QR_Code`);
          } catch (error) {
            console.error('error saving QR code:', error);
          }
        } else {
          console.error('QR code reference is not available');
        }
        break;
      }
      case 'eye-outline':
        closeBottomSheet(redirectUrl);
        setQRCodeVisible(true);
        break;
      case 'logo-instagram':
        Linking.openURL(club?.instagramLink);
        break;
      case 'logo-facebook':
        Linking.openURL(club?.facebookLink);
        break;
      case 'logo-slack':
        Linking.openURL(club?.slackLink);
        break;
      default:
        console.log('Error: Icon not found');
    }
  }

  const renderIconButtons = () => bottomSheetIcons.map((icon) => (
    <TouchableOpacity
      key={icon.name}
      onPress={() => handleIconPress(icon.name)}
    >
      <View style={styles.iconContainer}>
        <View style={styles.iconCircle}>
          <Ionicons name={icon.name} size={30} color="black" />
        </View>
        <Text style={styles.iconText}>{icon.text}</Text>
      </View>
    </TouchableOpacity>
  ));
  // TODO: Fix navigation back to the ClubScr from this stack

  const canAccessClub = !isPrivate || isAdmin || isFollowing;

  return (
    <>
      <SafeAreaView style={{ flex: 1 }}>
        <ScrollView
          showsVerticalScrollIndicator
          refreshControl={(
            <CustomRefresh
              changeHandler={changeHandler}
              reqs={{
                screen: 'ClubScr',
                club,
                user,
                pageSize,
                eventType,
              }}
            />
          )}
        >
          {canAccessClub ? (
            <View style={styles.clubView}>
              <View style={{ paddingTop: '10%' }}>
                <Image
                  size="40"
                  borderRadius={100}
                  source={
                    club?.thumbnailUrl
                      ? { uri: club.thumbnailUrl }
                      : defaultClubImage
                  }
                  alt="Club Thumbnail"
                />
              </View>
              <Heading
                size="xl"
                textAlign="center"
                px="4"
                style={{ paddingBottom: '2%', paddingTop: '5%' }}
              >
                {club?.name}
              </Heading>
              <Text style={{ paddingBottom: '5%' }}>{club?.category}</Text>

              {isAdmin ? (
                <>
                  <Button
                    style={styles.adminButton}
                    onPress={() => navigation.navigate('Edit Club', {
                      club,
                      updateClubState,
                    })}
                  >
                    <Text>Manage</Text>
                  </Button>

                  <Button
                    style={styles.adminButton}
                    onPress={() => navigation.navigate('Create Announcement', { club })}
                  >
                    <Text>Make an announcement</Text>
                  </Button>

                  <Button
                    style={styles.adminButton}
                    onPress={() => navigation.navigate('Create Event', { club })}
                  >
                    <Text>Create a new event</Text>
                  </Button>
                </>
              ) : (
                <Button
                  style={{
                    alignSelf: 'center',
                    backgroundColor: isFollowing
                      ? colors.success
                      : colors.secondary0,
                    width: '85%',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                  onPress={followBtnHandler}
                >
                  {isFollowing ? <Text>Following</Text> : <Text>Follow</Text>}
                </Button>
              )}

              {renderConnectCard()}

              {/**
               * Purpose
               */}
              <Box border="1" py="4" style={styles.clubScrCard}>
                <VStack space="4">
                  <Box header>
                    <Text fontSize="lg" bold>
                      Purpose
                    </Text>
                  </Box>
                  <Box px="0">
                    <Box>
                      {club?.description && (
                        <RenderHTML
                          source={{
                            html: club.description.substring(
                              0,
                              LINE_CHARACTER_REQ,
                            ),
                          }}
                          contentWidth={Layout.window.width}
                        />
                      )}
                    </Box>
                  </Box>
                </VStack>
              </Box>

              {/**
               * Events
               */}
              <Box border="1" style={styles.clubScrCard}>
                <Box py="4" style={{ justifyContent: 'space-between' }}>
                  <Text fontSize="lg" bold alignSelf="flex-start">
                    Upcoming Events
                  </Text>

                  <TouchableOpacity
                    onPress={() => navigation.navigate('Events', {
                      clubId: club?._id,
                      isAdmin,
                    })}
                  >
                    <Text
                      style={{ alignSelf: 'flex-start', color: colors.link }}
                    >
                      View all
                    </Text>
                  </TouchableOpacity>
                </Box>
                <Box style={{ paddingHorizontal: '0%' }}>
                  <VStack>
                    <Box style={{ paddingHorizontal: '0%', width: '100%' }}>
                      {!eventsEmpty ? (
                        <Box>
                          {[...events].splice(0, 10).map((item) => (
                            <TouchableOpacity
                              onPress={() => navigation.navigate('Event', {
                                event: item,
                                isAdmin,
                              })}
                              id={item._id}
                              key={item._id}
                            >
                              <Box
                                borderTopWidth="1"
                                borderColor="gray.200"
                                style={styles.listTextWrapper}
                                width="100%"
                                flex={1}
                              >
                                <Text bold numberOfLines={1} flex="3">
                                  {item.name}
                                </Text>
                                <Text fontSize="xs" alignSelf="center">
                                  {DateTime.fromISO(item.date).toFormat(
                                    'MMM dd yyyy',
                                  )}
                                </Text>
                                <Ionicons
                                  name="chevron-forward-outline"
                                  size={30}
                                />
                              </Box>
                            </TouchableOpacity>
                          ))}
                        </Box>
                      ) : (
                        <Text style={styles.noAnnouncementsEventsMessage}>
                          There are no events for this club.
                        </Text>
                      )}
                    </Box>
                  </VStack>
                </Box>
              </Box>

              {/**
               * Announcements
               */}
              <Box style={styles.clubScrCard}>
                <VStack>
                  <Box py="4" style={{ justifyContent: 'space-between' }}>
                    <Text fontSize="lg" bold alignSelf="flex-start">
                      Announcements
                    </Text>

                    {!announcementsEmpty && (
                      <TouchableOpacity
                        onPress={() => navigation.navigate('Announcements', {
                          clubId: club?._id,
                          isAdmin,
                        })}
                      >
                        <Text
                          style={{
                            alignSelf: 'flex-start',
                            color: colors.link,
                          }}
                        >
                          View all
                        </Text>
                      </TouchableOpacity>
                    )}
                  </Box>
                  <Box style={{ paddingHorizontal: '0%' }}>
                    <Box style={{ paddingHorizontal: '0%', width: '100%' }}>
                      {!announcementsEmpty ? (
                        <Box>
                          {[...announcements].splice(0, 5).map((item) => (
                            <TouchableOpacity
                              onPress={() => navigation.navigate('Announcement', {
                                isAdmin,
                                announcement: item,
                              })}
                              id={item._id}
                              key={item._id}
                            >
                              <Box
                                borderTopWidth="1"
                                borderColor="gray.200"
                                style={styles.listTextWrapper}
                                width="100%"
                                flex={1}
                              >
                                <VStack width="90%">
                                  <Text bold numberOfLines={1} flex="1">
                                    {item.title}
                                  </Text>
                                  <Box
                                    marginLeft="3%"
                                    maxHeight="35px"
                                    overflow="hidden"
                                  >
                                    <RenderHTML
                                      source={{ html: item.description }}
                                      contentWidth={Layout.window.width}
                                    />
                                  </Box>
                                </VStack>
                                <Ionicons
                                  name="chevron-forward-outline"
                                  size={30}
                                />
                              </Box>
                            </TouchableOpacity>
                          ))}
                        </Box>
                      ) : (
                        <Text style={styles.noAnnouncementsEventsMessage}>
                          There are no announcements for this club.
                        </Text>
                      )}
                    </Box>
                  </Box>
                </VStack>
              </Box>

              {/**
               * Admins
               */}
              <Box py="4" style={styles.clubScrCard}>
                <VStack>
                  <Box style={{ justifyContent: 'space-between' }}>
                    <Text fontSize="lg" bold alignSelf="flex-start">
                      Admins
                    </Text>
                    <TouchableOpacity
                      onPress={() => navigation.navigate('Admins', { club, updateClubState })}
                    >
                      <Text
                        style={{ alignSelf: 'flex-start', color: colors.link }}
                      >
                        View all
                      </Text>
                    </TouchableOpacity>
                  </Box>
                  <Box style={{ paddingHorizontal: '0%' }}>
                    <Box
                      borderTopWidth="1"
                      borderColor="gray.300"
                      paddingHorizontal="0%"
                      width="100%"
                    >
                      {club?.admins
                        && club.admins.map((item) => (
                          <AdminRow
                            admin={{
                              name: `${item.name.first} ${item.name.last}`,
                              year: item.year,
                              major: item.major,
                              profileImage: item.profileImage,
                            }}
                            handler={() => {}}
                            id={item._id}
                            key={item._id}
                          />
                        ))}
                    </Box>
                  </Box>
                </VStack>
              </Box>
            </View>
          ) : (
            <Text fontSize="lg">
              This club is private. Please ask a club admin to add you as a
              member.
            </Text>
          )}
        </ScrollView>

        {/* Share Button Bottom Sheet */}
        {bottomSheet && canAccessClub ? (
          <Portal>
            <BottomSheet
              ref={bottomSheetRef}
              index={0}
              snapPoints={['25%']}
              enablePanDownToClose
              backdropComponent={renderBackdrop}
              backgroundStyle={{ backgroundColor: '#FAF9F6' }}
              enableContentPanningGesture={false}
            >
              <View style={{ flex: 1 }}>
                <Text fontSize="lg" bold marginLeft="3%">
                  Share Club
                </Text>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                  {renderIconButtons()}
                </ScrollView>
              </View>
            </BottomSheet>
          </Portal>
        ) : null}
        {/* QR Code Modal */}
        {qrCodeVisible && renderQRCode('club')}
      </SafeAreaView>
    </>
  );
};

export default ClubScr;
