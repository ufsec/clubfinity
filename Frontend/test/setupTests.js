// This file allows contains any functions/classes that we want to override
// during our tests because they are causing issues with jest.

// eslint-disable-next-line import/no-extraneous-dependencies
import { jest } from '@jest/globals';

const mockReactNative = require('react-native');

// Apparently there is an issue with this module with jest. So, we mock it
// for now and use ScrollView instead (until we upgrade the package).
// https://github.com/APSL/react-native-keyboard-aware-scroll-view/issues/493
jest.mock('react-native-keyboard-aware-scroll-view', () => {
  const KeyboardAwareScrollView = mockReactNative.ScrollView;
  return { KeyboardAwareScrollView };
});

// We use `registerRootComponent` to define the entry point in our app.
// However, it causes issues with jest.
jest.mock('expo', () => ({
  registerRootComponent: () => { /* Do nothing */ },
}));

// This is giving issues with jest, and we don't need it for these
// unit tests since you don't run them on a phone/emulator.
// https://stackoverflow.com/questions/76903168/mocking-libraries-in-jest
jest.mock('react-native/Libraries/LogBox/LogBox', () => ({
  __esModule: true,
  default: {
    ignoreLogs: () => { /* Do nothing */ },
    ignoreAllLogs: () => { /* Do nothing */ },
  },
}));

// Giving issues on SigninScr.test.js from TextAreaInput.js component,
// so mocked to avoisd the issue.
jest.mock('react-native-webview', () => ({
  WebView: () => null,
  RNCWebViewModule: {
    getEnforcing: jest.fn().mockReturnValue(null),
  },
}));
