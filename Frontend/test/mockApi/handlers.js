import { http, HttpResponse } from 'msw';

const handlers = [
  http.post('http://mock/auth/login', async ({ request }) => {
    const { email, password } = await request.json();
    let res;
    let statusCode;
    let errorMessage;

    // This first if statement has the correct credentials.
    if (email === 'correctEmail' && password === 'correctPassword') {
      res = {
        token: 'mockToken',
        user: 'mockUser',
      };
      statusCode = 200;
      return HttpResponse.json(res, { status: statusCode });
    }

    // Anything other than the correct credentials throws an error.
    if (email === '' || password === '') {
      errorMessage = 'Missing credentials';
    } else if (email !== 'correctEmail') {
      errorMessage = 'User not found with that email.';
    } else if (password !== 'correctPassword') {
      errorMessage = 'Incorrect password.';
    } else {
      errorMessage = 'Issue with testing format.';
    }
    res = {
      error: errorMessage,
      user: false,
    };
    statusCode = 400;

    return HttpResponse.json(res, { status: statusCode });
  }),
];

export default handlers;
