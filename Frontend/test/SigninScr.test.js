import React from 'react';
import {
  render, screen, fireEvent, waitFor,
} from '@testing-library/react-native';
// eslint-disable-next-line import/no-extraneous-dependencies
import {
  jest, describe, it, beforeEach, afterEach, expect, afterAll, beforeAll,
} from '@jest/globals';
import { setupServer } from 'msw/node';
import SigninScr from '../screens/SigninScr';
import { UserProvider } from '../util/UserContext';
import { DeepLinkingProvider } from '../util/DeepLinkingContext';
import handlers from './mockApi/handlers';

// Mock the navigation object so we can detect
// whenever the `navigate` function is called.
const mockNavigation = {
  navigate: jest.fn(),
};

jest.mock('expo-asset', () => ({
  Asset: {
    fromModule: jest.fn(() => ({
      downloadAsync: jest.fn(),
    })),
  },
}));

jest.mock('expo-font', () => ({
  useFonts: jest.fn(),
  loadAsync: jest.fn(),
}));

jest.mock('@expo/vector-icons', () => ({
  Feather: 'Feather',
}));

jest.mock('expo-image-picker', () => ({
  requestMediaLibraryPermissionsAsync: jest.fn(),
  launchImageLibraryAsync: jest.fn(),
}));

// Set up a mock backend server so we can catch API calls.
const server = setupServer(...handlers);

describe('Sign In Screen', () => {
  // Start the mock server.
  beforeAll(() => server.listen());

  // Render the sign in screen before each test.
  beforeEach(() => {
    render(
      <UserProvider>
        <DeepLinkingProvider>
          <SigninScr navigation={mockNavigation} />
        </DeepLinkingProvider>
      </UserProvider>,
    );
  });

  // Reset and stop the server after all tests finish.
  afterAll(() => server.resetHandlers(), server.close());

  // Clear mock calls after each test. Otherwise, the mock
  // navigation object will store the call history for each test.
  afterEach(() => {
    mockNavigation.navigate.mockClear();
  });

  it('should render the initial texts correctly', async () => {
    screen.getByText('Forgot your password?');
    screen.getByText('Sign up');
    screen.getByText('Don\'t have an account yet?');
  });

  it('should show correct error messages on unsuccesful login attempts', async () => {
    // Test a user who inputs no email nor password.
    fireEvent.press(screen.getByText('Login'));
    await waitFor(() => {
      const errorMessage = screen.getByText('Missing credentials');
      expect(errorMessage).toBeTruthy();
    });

    // Test a user who inputs an email but no password.
    fireEvent.changeText(screen.getByPlaceholderText('Email'), 'correctEmail');
    fireEvent.changeText(screen.getByPlaceholderText('Password'), '');
    fireEvent.press(screen.getByText('Login'));
    await waitFor(() => {
      const errorMessage = screen.getByText('Missing credentials');
      expect(errorMessage).toBeTruthy();
    });

    // Clear the input.
    fireEvent.changeText(screen.getByPlaceholderText('Email'), '');

    // Test a user who inputs a correct password but no email.
    fireEvent.changeText(screen.getByPlaceholderText('Password'), 'correctPassword');
    fireEvent.press(screen.getByText('Login'));
    await waitFor(() => {
      const errorMessage = screen.getByText('Missing credentials');
      expect(errorMessage).toBeTruthy();
    });

    // Test a user who inputs an incorrect email.
    fireEvent.changeText(screen.getByPlaceholderText('Email'), 'wrongEmail');
    fireEvent.changeText(screen.getByPlaceholderText('Password'), 'correctPassword');
    fireEvent.press(screen.getByText('Login'));
    await waitFor(() => {
      const errorMessage = screen.getByText('User not found with that email.');
      expect(errorMessage).toBeTruthy();
    });

    // Test a user who inputs a correct email but incorrect password.
    fireEvent.changeText(screen.getByPlaceholderText('Email'), 'correctEmail');
    fireEvent.changeText(screen.getByPlaceholderText('Password'), 'wrongPassword');
    fireEvent.press(screen.getByText('Login'));
    await waitFor(() => {
      const errorMessage = screen.getByText('Incorrect password.');
      expect(errorMessage).toBeTruthy();
    });
  });

  it('should navigate home on a succesful login attempt', async () => {
    fireEvent.changeText(screen.getByPlaceholderText('Email'), 'correctEmail');
    fireEvent.changeText(screen.getByPlaceholderText('Password'), 'correctPassword');
    fireEvent.press(screen.getByText('Login'));
    await waitFor(() => {
      expect(mockNavigation.navigate).toHaveBeenCalledWith('AppStack');
    });
  });

  it('should navigate to ForgotPasswordScr when clicked', async () => {
    fireEvent.press(screen.getByText('Forgot your password?'));
    await waitFor(() => {
      expect(mockNavigation.navigate).toHaveBeenCalledWith('ForgotPassword');
    });
  });

  it('should navigate to SignupScr when clicked', async () => {
    fireEvent.press(screen.getByText('Sign up'));
    await waitFor(() => {
      expect(mockNavigation.navigate).toHaveBeenCalledWith('SignUp');
    });
  });
});
