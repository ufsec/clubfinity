// This file configures Babel, the JS compiler.
// Babel transpiles JS code for different environments.

module.exports = function (api) {
  // Caching is enabled to improve build performance.
  api.cache(true);
  return {
    // Using the 'babel-preset-expo' preset for Expo projects.
    presets: ['babel-preset-expo'],
    plugins: ['react-native-reanimated/plugin'],
  };
};
