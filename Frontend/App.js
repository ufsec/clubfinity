import React, { useState, useEffect } from 'react';
import 'react-native-get-random-values';
import { Platform, StatusBar, LogBox } from 'react-native';
import { registerRootComponent } from 'expo';
import { NativeBaseProvider } from 'native-base';
import { GestureHandlerRootView } from 'react-native-gesture-handler';
import { BottomSheetModalProvider } from '@gorhom/bottom-sheet';
import * as Font from 'expo-font';
// eslint-disable-next-line camelcase
import { useFonts, Roboto_100Thin, Roboto_500Medium } from '@expo-google-fonts/roboto';
import { Asset } from 'expo-asset';
import * as SplashScreen from 'expo-splash-screen';
import { PortalProvider } from '@gorhom/portal';
import SplashImage from './assets/splash.png';
import { UserProvider } from './util/UserContext';
import { DeepLinkingProvider } from './util/DeepLinkingContext';
import { FullscreenMediaProvider } from './util/FullscreenMediaContext';
import AppNavigator from './navigation/AppNavigator';
// import Snackbar from './components/Snackbar';

LogBox.ignoreAllLogs(true);
SplashScreen.preventAutoHideAsync();

export default function App() {
  const [isLoadingComplete, setIsLoadingComplete] = useState(false);

  const cacheResources = async () => {
    const images = [SplashImage];
    const cacheImages = images.map((image) => Asset.fromModule(image).downloadAsync());
    return Promise.all(cacheImages);
  };

  useEffect(() => {
    async function prepare() {
      await Font.loadAsync({
        Roboto_100Thin,
        Roboto_500Medium,
      });
      await cacheResources();
      setIsLoadingComplete(true);
      SplashScreen.hideAsync();
    }
    prepare();
  }, []);

  const [fontsLoaded] = useFonts({
    Roboto_100Thin,
    Roboto_500Medium,
  });

  if (!isLoadingComplete || !fontsLoaded) {
    return null;
  }

  return (
    <GestureHandlerRootView style={{ flex: 1 }}>
      <NativeBaseProvider>
        <PortalProvider>
          {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
          <FullscreenMediaProvider>
            <GestureHandlerRootView style={{ flex: 1 }}>
              <BottomSheetModalProvider>
                <UserProvider>
                  <DeepLinkingProvider>
                    <AppNavigator />
                    {/* <Snackbar /> */}
                  </DeepLinkingProvider>
                </UserProvider>
              </BottomSheetModalProvider>
            </GestureHandlerRootView>
          </FullscreenMediaProvider>
        </PortalProvider>
      </NativeBaseProvider>
    </GestureHandlerRootView>
  );
}

registerRootComponent(App);
