import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import colors from '../util/colors';

const AlertCard = ({ alert, dismissAlert }) => {
  AlertCard.propTypes = {
    alert: PropTypes.shape({
      _id: PropTypes.string,
      type: PropTypes.string,
      title: PropTypes.string,
      body: PropTypes.string,
    }).isRequired,
    dismissAlert: PropTypes.func.isRequired,
  };

  return (
    <View
      key={alert._id}
      style={{
        backgroundColor: colors.grayScale1,
        padding: 10,
        marginHorizontal: 10,
        marginBottom: 10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor:
          alert.type.toLowerCase() === 'info'
            ? colors.info
            : alert.type.toLowerCase() === 'warning'
              ? colors.warning
              : colors.error,
        borderStyle: 'solid',
        position: 'relative',
      }}
    >
      <TouchableOpacity
        onPress={() => dismissAlert(alert._id)}
        style={{
          position: 'absolute',
          top: 5,
          right: 5,
          zIndex: 1,
        }}
      >
        <Ionicons name="close" size={24} color={colors.grayScale11} />
      </TouchableOpacity>
      <Text
        style={{
          fontSize: 15,
          color: colors.grayScale11,
        }}
      >
        {alert.title}
      </Text>
      <Text
        style={{
          fontSize: 13,
          color: colors.grayScale8,
        }}
      >
        {alert.body}
      </Text>
    </View>
  );
};

export default AlertCard;
