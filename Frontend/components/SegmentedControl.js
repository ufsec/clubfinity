import { useState, React } from 'react';
import {
  View, Text, TouchableOpacity, Animated,
} from 'react-native';
import PropTypes from 'prop-types';

// To use component, basically pass a parentHandler from parent of this component
// Returns 0 if left button is active, 1 if right button is active

const SegmentedControl = ({ parentHandler, optionOne, optionTwo }) => {
  const [active, setActive] = useState(0);
  // xTabOne is left tab, xTabTwo is right tab
  const [xTabOne, setxTabOne] = useState(0);
  const [xTabTwo, setxTabTwo] = useState(0);
  const [translateX, setTranslateX] = useState(new Animated.Value(0));

  // Slide animation handler when button clicked
  const handleSlide = (type) => {
    Animated.spring(translateX, {
      toValue: type,
      duration: 100,
      useNativeDriver: true,
    }).start();

    // translateX is updated through Animation
    // (Only using useState to update component)
    setTranslateX(translateX);
  };

  return (
    <View>
      <View style={{ width: '90%', marginLeft: 'auto', marginRight: 'auto' }}>
        <View
          style={{
            flexDirection: 'row',
            marginTop: 20,
            marginBottom: 20,
            height: 36,
            position: 'relative',
          }}
        >
          <Animated.View
            style={{
              position: 'absolute',
              width: '50%',
              height: '100%',
              top: 0,
              left: 0,
              backgroundColor: '#206ff5',
              borderRadius: 4,
              transform: [
                {
                  translateX,
                },
              ],
            }}
          />
          <TouchableOpacity
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              borderWidth: 1,
              borderColor: '#206ff5',
              borderRadius: 4,
              borderRightWidth: 0,
              borderTopRightRadius: 0,
              borderBottomRightRadius: 0,
            }}
            onLayout={(event) => setxTabOne(event.nativeEvent.layout.x)}
            // When left button pressed, set corresponding side active, handle slide,
            // And send active number to parentHandler
            onPress={() => {
              setActive(0);
              handleSlide(xTabOne);
              parentHandler(0);
            }}
          >
            <Text style={{ color: active === 0 ? '#fff' : '#206ff5' }}>
              {optionOne}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              borderWidth: 1,
              borderColor: '#206ff5',
              borderRadius: 4,
              borderLeftRadius: 0,
              borderTopLeftRadius: 0,
              borderBottomLeftRadius: 0,
            }}
            onLayout={(event) => setxTabTwo(event.nativeEvent.layout.x)}
            // When right button pressed, set corresponding side active, handle slide,
            // And send active number to parentHandler
            onPress={() => {
              setActive(1);
              handleSlide(xTabTwo);
              parentHandler(1);
            }}
          >
            <Text style={{ color: active === 1 ? '#fff' : '#206ff5' }}>
              {optionTwo}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

SegmentedControl.propTypes = {
  parentHandler: PropTypes.func.isRequired,
  optionOne: PropTypes.string.isRequired,
  optionTwo: PropTypes.string.isRequired,
};

export default SegmentedControl;
