import {
  View, TouchableOpacity, StyleSheet, ActivityIndicator,
} from 'react-native';
import React, { useContext, useState, useEffect } from 'react';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { useVideoPlayer, VideoView } from 'expo-video';
import PropTypes from 'prop-types';
import { useEventListener } from 'expo';
import { FullscreenMediaContext } from '../util/FullscreenMediaContext';

const styles = StyleSheet.create({
  videoContainer: {
    aspectRatio: 1,
    overflow: 'hidden',
    borderRadius: 25,
    marginTop: '7%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#151515',
  },
  video: {
    flex: 1,
    aspectRatio: 1,
  },
  button: {
    position: 'absolute',
    backgroundColor: 'rgba(255, 255, 255, 0.1)',
    borderRadius: 25,
    top: 10,
    right: 10,
    padding: 7,
  },
});

export default function ExpandableVideo({ video, shouldPlay = true, style = null }) {
  const { expandVideo } = useContext(FullscreenMediaContext);
  const [loadingVideo, setLoadingVideo] = useState(true);

  const player = useVideoPlayer(video, (videoPlayer) => {
    videoPlayer.loop = true;
  });

  useEffect(() => {
    if (player) {
      if (shouldPlay) {
        player.play();
      } else {
        player.pause();
      }
    }
  }, [shouldPlay, player]);

  // detects when the video is ready to play
  useEventListener(player, 'statusChange', React.useCallback(({ status }) => {
    if (status === 'readyToPlay') {
      setLoadingVideo(false);
      if (shouldPlay && !player.playing) {
        player.play();
      }
    } else {
      setLoadingVideo(true);
    }
  }, [shouldPlay, player]));

  return (
    <View style={[styles.videoContainer, style]}>
      <VideoView
        style={styles.video}
        player={player}
        resizeMode="contain"
        nativeControls={false}
      />
      {/* Button to expand current video into "fullscreen mode" */}
      {!loadingVideo && (
        <TouchableOpacity style={styles.button} onPress={() => expandVideo(video, player)}>
          <MaterialCommunityIcons name="arrow-expand" size={24} color="gray" />
        </TouchableOpacity>
      )}
      {/* Loading indicator while video is loading in */}
      {loadingVideo && (
        <View style={{ position: 'absolute' }}>
          <ActivityIndicator color="white" />
        </View>
      )}
    </View>
  );
}

ExpandableVideo.propTypes = {
  video: PropTypes.string.isRequired,
  shouldPlay: PropTypes.bool,
  style: PropTypes.instanceOf(Object),
};
