import React from 'react';
import {
  TouchableOpacity, Text, StyleSheet, ActivityIndicator, View,
} from 'react-native';
import { ViewPropTypes, TextPropTypes } from 'deprecated-react-native-prop-types';
import PropTypes from 'prop-types';

import colors from '../../util/colors';

const styles = StyleSheet.create({
  button: {
    padding: 15,
    backgroundColor: colors.primary0,
    borderRadius: 8,
    minHeight: 42,
    elevation: 3,
  },
  text: {
    fontSize: 15,
    alignSelf: 'center',
    color: colors.grayScale0,
  },
});

const Button = (props) => {
  const {
    text, onPress, style, textStyle, isLoading,
  } = props;

  return (
    <TouchableOpacity
      style={[styles.button, style, isLoading && { opacity: 0.5 }]}
      onPress={onPress}
      disabled={isLoading}
    >
      {/* if loading, render loading spinner and text */}
      <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
        {isLoading && (
          <ActivityIndicator size="small" color={colors.grayScale0} style={{ marginRight: 10 }} />
        )}
        <Text style={{ ...styles.text, ...textStyle }}>{text}</Text>
      </View>
    </TouchableOpacity>
  );
};

Button.propTypes = {
  text: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  style: ViewPropTypes.style,
  textStyle: TextPropTypes.style,
  isLoading: PropTypes.bool,
};

Button.defaultProps = {
  style: {},
  textStyle: {},
  isLoading: false,
};

export default Button;
