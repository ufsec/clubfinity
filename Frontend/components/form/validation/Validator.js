import {
  isValidFacebookUrl, isValidInstagramUrl, isValidSlackUrl, isValidUrl,
} from '../../../util/validationUtil';

// Standard Email regex, see: https://emailregex.com/
// eslint-disable-next-line max-len
const EMAIL_REGEX = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export default class Validator {
  static required(errorMessage = 'Required') {
    return (value) => {
      if (value === null || value === '') {
        return errorMessage;
      }

      return null;
    };
  }

  static email(errorMessage = 'Invalid') {
    return (value) => {
      if (!EMAIL_REGEX.test(value)) {
        return errorMessage;
      }

      return null;
    };
  }

  static validUrl(errorMessage = 'Invalid') {
    return (value) => {
      if (!isValidUrl(value)) {
        return errorMessage;
      }

      return null;
    };
  }

  /**
   * validLinks() should be used for lists of the following format stored in state:
   * [
   *  {
   *    link: "https://ufl.edu", <-- This will be validated for each object
   *    ...
   *  },
   *  ...
   * ]
   *
   * If any errors are detected, a list of equal length will be returned containing
   * errors at the corresponding index (e.g. [null, "Invalid", null])
   * */
  static validLinks(errorMessage = 'Invalid') {
    return (linkObjects) => {
      const errors = new Array(linkObjects.length).fill(null);
      linkObjects.forEach((linkObject, index) => {
        if (!isValidUrl(linkObject.link)) {
          errors[index] = errorMessage;
        }
      });

      // If no errors were detected, return null rather than a list of nulls
      // Otherwise, the validator will treat the non-null value as an "invalid" result
      return errors.filter((error) => error != null).length > 0 ? errors : null;
    };
  }

  static itemsRequireFields(fields, errorMessage = 'Required') {
    // For a field that is an array of objects, check that every object has some input value for the given subfields
    // Errors accessible from errors.field.subfield[index]
    return (array) => {
      const errors = {};
      fields.forEach((field) => {
        errors[field] = new Array(array.length).fill(null);
      });

      let errorFound = false;
      array.forEach((item, index) => {
        fields.forEach((field) => {
          if (item[field] === null || item[field] === '') {
            errorFound = true;
            errors[field][index] = errorMessage;
          }
        });
      });

      return errorFound ? errors : null;
    };
  }

  static validFacebookUrl(errorMessage = 'Invalid') {
    return (value) => {
      if (value !== '' && !isValidFacebookUrl(value)) {
        return errorMessage;
      }

      return null;
    };
  }

  static validInstagramUrl(errorMessage = 'Invalid') {
    return (value) => {
      if (!isValidInstagramUrl(value)) {
        return errorMessage;
      }

      return null;
    };
  }

  static validSlackUrl(errorMessage = 'Invalid') {
    return (value) => {
      if (!isValidSlackUrl(value)) {
        return errorMessage;
      }

      return null;
    };
  }

  static validLocation(errorMessage = 'Invalid') {
    return (value) => {
      if (!value || !value.placeId || !value.description || !value.googleMapsUrl) {
        return errorMessage;
      }

      return null;
    };
  }

  static minLength(minLength, errorMessage = 'Too Short') {
    return (value) => {
      const { length } = value;
      if (length < minLength) {
        return errorMessage;
      }

      return null;
    };
  }

  static maxLength(maxLength, errorMessage = 'Too Long') {
    return (value) => {
      const { length } = value;
      if (length > maxLength) {
        return errorMessage;
      }

      return null;
    };
  }

  static regex(regex, errorMessage = 'Invalid') {
    return (value) => {
      if (!regex.test(value)) {
        return errorMessage;
      }

      return null;
    };
  }

  static numeric(errorMessage = 'Invalid') {
    return (value) => {
      if (Number.isNaN(Number(value))) {
        return errorMessage;
      }

      return null;
    };
  }

  static endsWith(suffix, errorMessage = 'Invalid') {
    return (value) => {
      // eslint-disable-next-line react/destructuring-assignment
      if (!value.endsWith(suffix)) {
        return errorMessage;
      }

      return null;
    };
  }

  static equalsField(field, errorMessage = 'Invalid') {
    return (value, formValue) => {
      // eslint-disable-next-line react/destructuring-assignment
      if (value !== formValue[field]) {
        return errorMessage;
      }

      return null;
    };
  }

  constructor(config) {
    this.config = config;
  }

  validate(formValue) {
    const errors = {};

    Object.keys(this.config).forEach((field) => {
      const validators = this.config[field];
      const value = formValue[field];

      validators.forEach((validator) => {
        const error = validator(value, formValue);

        if (error && !errors[field]) {
          errors[field] = error;
        }
      });
    });

    return { valid: Object.keys(errors).length === 0, errors };
  }
}
