import React from 'react';
import {
  Platform, StyleSheet, View,
} from 'react-native';
import { ViewPropTypes } from 'deprecated-react-native-prop-types';
import {
  Select,
} from 'native-base';
import PropTypes from 'prop-types';
import { Ionicons } from '@expo/vector-icons';
import colors from '../../util/colors';
import LabelAndErrorContainer from './LabelAndErrorContainer';

const styles = StyleSheet.create({
  selectInputContainer: {
    // This is required to avoid content jump when an error is added
    borderWidth: 1,
    borderColor: 'transparent',
    borderRadius: 10,
    backgroundColor: colors.grayScale1,
    width: '100%',
    paddingVertical: 6.5,
    paddingHorizontal: 3,
  },
  selectInput: {
    color: colors.inputText,
  },
  androidSelectContainer: {
    borderWidth: 1,
    borderColor: 'transparent',

    borderRadius: 10,
    backgroundColor: colors.grayScale1,
    paddingTop: 6.5,
    paddingBottom: 6.5,
    paddingLeft: 3,
    paddingRight: 3,
    width: '100%',
    color: colors.inputText,
  },
  errorInput: {
    borderColor: colors.error,
  },
  selectInputText: {
    color: colors.inputText,
  },
  selectInputPlaceholder: {
    color: colors.inputPlaceholder,
  },
  dropdownIcon: {
    paddingRight: '5%',
  },
});

export default class extends React.Component {
  static propTypes = {
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    options: PropTypes.array.isRequired,
    placeholder: PropTypes.string,
    label: PropTypes.string,
    style: ViewPropTypes.style,
    error: PropTypes.string,
  };

  static defaultProps = {
    placeholder: null,
    label: null,
    style: {},
    error: null,
  };

  renderAndroidPicker() {
    const {
      options, value, onChange, placeholder, error,
    } = this.props;

    const items = options.map((s) => (
      <Select.Item
        value={s.value}
        label={s.label}
        key={s.label}
      />
    ));

    // Since android always defaults to the first option in a dropdown, the placeholder must be added as an option
    // The given color allows it to appear as a placeholder with a lighter text color
    if (placeholder) {
      items.unshift(
        <Select.Item
          value=""
          label={placeholder}
          key={placeholder}
          color={colors.inputPlaceholder}
        />,
      );
    }

    // On Android, the picker must be wrapped in a view to give it a custom style since the picker style cannot
    // easily be changed and doing so hides the dropdown arrow icon
    return (
      <View style={{ ...styles.androidSelectContainer, ...(error ? styles.errorInput : {}) }}>
        <Select
          mode="dropdown"
          selectedValue={value}
          onValueChange={(newValue) => onChange(newValue)}
          borderWidth="0"
          dropdownIcon={(
            <Ionicons
              name="chevron-down-outline"
              size={20}
              style={styles.dropdownIcon}
            />
          )}
        >
          {items}
        </Select>
      </View>
    );
  }

  renderIOSPicker() {
    const {
      options, value, onChange, placeholder, error,
    } = this.props;

    const pickerItems = options.map((s) => (
      <Select.Item
        value={s.value}
        label={s.label}
        key={s.label}
      />
    ));

    return (
      <View style={styles.selectInputContainer}>
        <Select
          mode="dropdown"
          placeholder={placeholder}
          selectedValue={value}
          onValueChange={(newValue) => onChange(newValue)}
          style={{ ...styles.selectInput, ...(error ? styles.errorInput : {}) }}
          placeholderStyle={styles.selectInputPlaceholder}
          textStyle={styles.selectInputText}
          color={colors.inputPlaceholder}
          borderWidth="0"
          dropdownIcon={(
            <Ionicons
              name="chevron-down-outline"
              size={20}
              style={styles.dropdownIcon}
            />
          )}
        >
          {pickerItems}
        </Select>
      </View>
    );
  }

  renderPicker() {
    return Platform.OS === 'ios' ? this.renderIOSPicker() : this.renderAndroidPicker();
  }

  render() {
    const {
      label, style, error,
    } = this.props;

    return (
      <LabelAndErrorContainer
        style={style}
        label={label}
        error={error}
      >
        {this.renderPicker()}
      </LabelAndErrorContainer>
    );
  }
}
