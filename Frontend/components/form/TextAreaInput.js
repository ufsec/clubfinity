import React from 'react';
import {
  StyleSheet, Text,
} from 'react-native';
import PropTypes from 'prop-types';
import {
  actions,
  RichEditor,
  RichToolbar,
} from 'react-native-pell-rich-editor';
import { ViewPropTypes } from 'deprecated-react-native-prop-types';
import colors from '../../util/colors';
import newColors from '../../constants/Colors';
import LabelAndErrorContainer from './LabelAndErrorContainer';

const styles = StyleSheet.create({
  errorInput: {
    borderColor: colors.error,
    borderWidth: 1,
  },
  /** ***************************** */
  /* styles for html tags */
  a: {
    fontWeight: 'bold',
    color: 'purple',
  },
  div: {
    fontFamily: 'monospace',
  },
  p: {
    fontSize: 30,
  },
  /** **************************** */
  richContainer: {
    borderRadius: 10,
  },
  richEditor: {
    backgroundColor: colors.grayScale1,
    color: colors.inputText,
    placeholderColor: colors.inputPlaceholder,
    contentCSSText: 'padding: 15px 20px; font-size: 16px;',
  },
  richBar: {
    backgroundColor: 'transparent',
  },
});

export default class extends React.Component {
  static propTypes = {
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    placeholder: PropTypes.string,
    label: PropTypes.string,
    error: PropTypes.string,
    RichText: PropTypes.instanceOf(Object).isRequired,
    tintColor: PropTypes.string,
    style: ViewPropTypes.style,
  };

  static defaultProps = {
    placeholder: null,
    label: null,
    error: null,
    tintColor: null,
    style: {},
  };

  constructor(props) {
    super(props);
    this.state = {
      isFocused: false,
    };
  }

  handleFocus = () => {
    this.setState({ isFocused: true });
  };

  handleBlur = () => {
    this.setState({ isFocused: false });
  };

  render() {
    const {
      RichText,
      placeholder,
      value,
      onChange,
      label,
      error,
      style,
    } = this.props;

    const { isFocused } = this.state;

    return (
      <LabelAndErrorContainer
        style={style}
        label={label}
        error={error}
      >
        {isFocused && (
          <RichToolbar
            style={[styles.richBar]}
            editor={RichText}
            iconTint={newColors.blue}
            selectedIconTint={newColors.blueFade}
            disabledIconTint={newColors.blue}
            iconSize={30}
            iconMap={{
              [actions.heading1]: ({ tintColor }) => (
                <Text style={[styles.tib, { color: tintColor, fontSize: 20 }]}>H1</Text>
              ),
              // Add more actions as needed
            }}
          />
        )}
        <RichEditor
          androidHardwareAccelerationDisabled
          androidLayerType="software"
          containerStyle={{ ...styles.richContainer, ...(error ? styles.errorInput : {}) }}
          ref={RichText}
          editorStyle={styles.richEditor}
          placeholder={placeholder}
          initialContentHTML={value ?? ''}
          initialHeight={100}
          initialFocus={false}
          onChange={onChange}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
        />
      </LabelAndErrorContainer>
    );
  }
}
