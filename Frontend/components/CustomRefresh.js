import React from 'react';
import PropTypes from 'prop-types';
import { RefreshControl } from 'react-native';
import ClubsApi from '../api/ClubsApi';
import EventsApi from '../api/EventsApi';
import AnnouncementsApi from '../api/AnnouncementsApi';

export default class CustomRefresh extends React.Component {
  static propTypes = {
    changeHandler: PropTypes.func.isRequired,
    reqs: PropTypes.shape({
      screen: PropTypes.string,
      clubId: PropTypes.string,
      clubs: PropTypes.array,
      eventType: PropTypes.string,
      user: PropTypes.instanceOf(Object),
      club: PropTypes.instanceOf(Object),
      pageSize: PropTypes.number,
      searchType: PropTypes.string,
      searchText: PropTypes.string,
    }).isRequired,
    style: PropTypes.instanceOf(Object),
    children: PropTypes.oneOfType([
      PropTypes.element,
      PropTypes.arrayOf(PropTypes.element),
    ]),
  };

  static defaultProps = {
    style: null,
    children: null,
  };

  constructor(props) {
    super(props);
    this.onRefresh = this.onRefresh.bind(this);
    this.screensRefresh = {
      ClubScr: this.clubscrRefresh,
      DiscoverScr: this.discoverscrRefresh,
      HomeScr: this.homescrRefresh,
      AnnouncementList: this.announcementListRefresh,
      EventList: this.eventListRefresh,
    };
    this.state = {
      refreshing: false,
    };
  }

  homescrRefresh = async () => {
    const { reqs } = this.props;
    const { clubs, eventType, pageSize } = reqs;
    // Resets page to first page (0th)
    const homescrState = {
      isFollowingClubs: true,
      page: 1,
      events: [],
    };
    // If not following anyone,
    if (clubs.length === 0) {
      homescrState.events = [];
      homescrState.isFollowingClubs = false;
    } else {
      // Else, get events from followed clubs
      const { events, totalEvents } = await EventsApi.getFollowing(
        homescrState.page,
        pageSize,
        eventType,
      );
      homescrState.events = events;
      homescrState.totalEvents = totalEvents;
    }

    // Return state of events, page number, etc
    return homescrState;
  };

  discoverscrRefresh = async () => {
    const discoverscrState = {
      allClubs: await ClubsApi.getAllClubs(),
      allEvents: await EventsApi.getAllEvents(),
    };
    return discoverscrState;
  };

  clubscrRefresh = async () => {
    const { reqs } = this.props;
    const {
      club, user, pageSize, eventType,
    } = reqs;

    // Reset page for pagination to 1
    const page = 1;

    let clubscrState = {
      isAdmin: false,
    };

    const {
      admins,
      tags,
      name,
      facebookLink,
      googleCalendarId,
      instagramLink,
      slackLink,
      description,
      category,
      thumbnailUrl,
    } = await ClubsApi.getClub(club._id);

    if (admins.map((admin) => admin._id).includes(user._id)) {
      clubscrState = { ...clubscrState, isAdmin: true };
    }
    const { events, announcements } = await ClubsApi.getPosts(
      club._id,
      page,
      pageSize,
      eventType,
    );

    clubscrState = {
      ...clubscrState,
      events,
      announcements,
      admins,
      tags,
      name,
      facebookLink,
      googleCalendarId,
      instagramLink,
      slackLink,
      description,
      category,
      thumbnailUrl,
    };

    return clubscrState;
  };

  announcementListRefresh = async () => {
    // Reset page for pagination to 1
    const page = 1;
    const { reqs } = this.props;
    const { clubId, pageSize } = reqs;
    let announcementsState = {
      announcements: [],
    };
    const { announcements, totalAnnouncements } = await AnnouncementsApi.getForClub(clubId, page, pageSize);
    announcementsState = { announcements, totalAnnouncements, page };
    return announcementsState;
  };

  eventListRefresh = async () => {
    // Reset page for pagination to 1
    const page = 1;
    const { reqs } = this.props;
    const { clubId, pageSize, eventType } = reqs;
    // Get and return events on 1st page
    const { events, totalEvents } = await EventsApi.getForClub(
      clubId,
      page,
      pageSize,
      eventType,
    );
    const eventListState = { events, totalEvents, page };

    return eventListState;
  };

  inputChange = async () => {
    const { changeHandler, reqs } = this.props;
    const { screen } = reqs;
    const newState = await this.screensRefresh[screen]();
    changeHandler(newState);
  };

  onRefresh = () => {
    this.setState({ refreshing: true });
    this.inputChange().then(() => this.setState({ refreshing: false }));
  };

  render() {
    const {
      changeHandler, reqs, style, children,
    } = this.props;
    const { refreshing } = this.state;
    return (
      <RefreshControl
        changeHandler={changeHandler}
        reqs={reqs}
        style={style}
        children={children}
        onRefresh={this.onRefresh}
        refreshing={refreshing}
      />
    );
  }
}
