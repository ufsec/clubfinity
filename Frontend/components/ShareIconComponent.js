import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import PropTypes from 'prop-types';

const ICON_SIZE = 30;
const ICON_COLOR = 'white';

const ShareIconComponent = ({ onPress }) => (
  <TouchableOpacity onPress={onPress}>
    <Ionicons name="share-outline" size={ICON_SIZE} color={ICON_COLOR} />
  </TouchableOpacity>
);

ShareIconComponent.propTypes = {
  onPress: PropTypes.func.isRequired,
};

export default ShareIconComponent;
