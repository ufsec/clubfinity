import React, { useRef, useState } from 'react';
import {
  View, useWindowDimensions, Animated,
} from 'react-native';
import PropTypes from 'prop-types';
import { FlatList } from 'react-native-gesture-handler';

// Component used for the pagination indicators at the bottom of the Carousel
const Paginator = ({ data, scrollX }) => {
  const { width } = useWindowDimensions();

  return (
    <View style={{ flexDirection: 'row', height: 64 }}>
      {data.map((_, index) => {
        const inputRange = [(index - 1) * width, index * width, (index + 1) * width];

        const dotWidth = scrollX.interpolate({
          inputRange,
          outputRange: [10, 20, 10],
          extrapolate: 'clamp',
        });

        const opacity = scrollX.interpolate({
          inputRange,
          outputRange: [0.3, 1, 0.3],
          extrapolate: 'clamp',
        });

        return (
          <Animated.View
            style={{
              height: 10,
              borderRadius: 5,
              backgroundColor: '#2660ff',
              marginHorizontal: 8,
              width: dotWidth,
              opacity,
            }}
            // eslint-disable-next-line react/no-array-index-key
            key={index.toString()}
          />
        );
      })}
    </View>
  );
};

Paginator.propTypes = {
  data: PropTypes.array.isRequired,
  scrollX: PropTypes.instanceOf(Object).isRequired,
};

// Main Carousel component. Takes in a desired view for each page, and the data to be displayed
const Carousel = ({ ItemView, data }) => {
  const scrollX = useRef(new Animated.Value(0)).current;
  const slidesRef = useRef(null);
  const [visibleIndex, setVisibleIndex] = useState(-1);

  // Variable to determine percentage of visibility needed to switch the "viewed" index
  const viewConfig = useRef({ viewAreaCoveragePercentThreshold: 50 }).current;

  // Called whenever the viewable item is changed. Uses the above value as reference
  const onViewableItemsChanged = useRef(({ viewableItems }) => {
    if (viewableItems && viewableItems.length > 0) {
      setVisibleIndex(viewableItems[0].index);
    }
  }).current;

  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <View style={{ flex: 5 }}>
        <FlatList
          data={data}
          renderItem={({ item, index }) => (
            <ItemView item={item} shouldPlay={index === visibleIndex} />
          )}
          keyExtractor={(item) => item.id}
          horizontal
          showsHorizontalScrollIndicator={false}
          pagingEnabled
          bounces={false}
          onScroll={Animated.event([{ nativeEvent: { contentOffset: { x: scrollX } } }], {
            useNativeDriver: false,
          })}
          viewabilityConfig={viewConfig}
          onViewableItemsChanged={onViewableItemsChanged}
          scrollEventThrottle={32}
          ref={slidesRef}
        />
      </View>
      <Paginator data={data} scrollX={scrollX} />
    </View>
  );
};

Carousel.propTypes = {
  ItemView: PropTypes.elementType.isRequired,
  data: PropTypes.array.isRequired,
};

export default Carousel;
