import {
  View, TouchableOpacity, StyleSheet, ActivityIndicator, Image,
} from 'react-native';
import React, { useContext, useState } from 'react';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import PropTypes from 'prop-types';
import { FullscreenMediaContext } from '../util/FullscreenMediaContext';

const styles = StyleSheet.create({
  imageContainer: {
    flex: 1,
    aspectRatio: 1,
    overflow: 'hidden',
    borderRadius: 25,
    marginTop: '7%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#f8f8f8',
  },
  image: {
    flex: 1,
    aspectRatio: 1,
  },
  button: {
    position: 'absolute',
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
    borderRadius: 25,
    top: 10,
    right: 10,
    padding: 7,
  },
});

// Component which allows displayed images to be viewed in the "fullscreen" mode (via FullscreenMediaContext)
export default function ExpandableImage({ image = null, style }) {
  const { expandImage } = useContext(FullscreenMediaContext);
  const [loadingImage, setLoadingImage] = useState(true);

  return (
    <View style={[styles.imageContainer, style]}>
      <Image
        source={{ uri: image }}
        style={styles.image}
        onLoad={() => setLoadingImage(false)}
        resizeMode="contain"
      />
      {/* Button to expand current image into "fullscreen mode" */}
      {!loadingImage && (
      <TouchableOpacity style={styles.button} onPress={() => expandImage(image)}>
        <MaterialCommunityIcons name="arrow-expand" size={24} color="grey" />
      </TouchableOpacity>
      )}
      {/* Loading indicator while image is loading in */}
      {loadingImage && (
      <View style={{ position: 'absolute' }}>
        <ActivityIndicator color="white" />
      </View>
      )}
    </View>
  );
}

ExpandableImage.propTypes = {
  image: PropTypes.string.isRequired,
  style: PropTypes.instanceOf(Object),
};
