/* eslint-disable react/jsx-props-no-spreading,react/prop-types */
import React, { useEffect, useRef, useState } from 'react';
import {
  View, StyleSheet, Image, TouchableOpacity, ActivityIndicator,
} from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import { Feather } from '@expo/vector-icons';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import TextInput from './form/TextInput';
import SelectField from './form/SelectInput';
import TextAreaInput from './form/TextAreaInput';
import Button from './form/Button';
import DateInput from './form/date/DateInput';
import defaultClubImage from '../assets/images/DefaultClubImage.png';
import defaultProfileImage from '../assets/images/DefaultUserImage.png';
import colors from '../util/colors';
import EventsApi from '../api/EventsApi';

const styles = StyleSheet.create({
  container: {
    paddingLeft: '5%',
    paddingRight: '5%',
    paddingTop: 10,
  },
  item: {
    marginTop: 11,
    marginBottom: 11,
  },
  imageContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
  imageViewContainer: {
    height: 120,
    width: 120,
    borderRadius: 60,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    height: 120,
    width: 120,
    position: 'absolute',
  },
  imageOverlay: {
    backgroundColor: 'rgba(0,0,0, 0.3)',
    ...StyleSheet.absoluteFill,
  },
  attachmentImage: {
    width: '100%',
    aspectRatio: 1,
    objectFit: 'cover',
    backgroundColor: 'black',
    borderRadius: 5,
  },
  attachmentImageContainer: {
    width: 'auto',
    borderRadius: 5,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  attachmentImageButton: {
    borderRadius: 10,
    marginVertical: 25,
    backgroundColor: colors.grayScale2,
  },
  editIcon: {
    height: 52,
    width: 52,
    position: 'absolute',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    borderRadius: 50,
    top: '-3%',
    right: '-3%',
  },
  editIconInnerCircle: {
    height: 44,
    width: 44,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.primary0,
    borderRadius: 50,
  },
});

export default class Form extends React.Component {
  render() {
    return (
      <View style={styles.container} ref={(c) => this._root = c} {...this.props} />
    );
  }
}

Form.Text = (props) => {
  const { style, ...rest } = props;

  return (
    <TextInput style={{ ...styles.item, ...style }} {...rest} />
  );
};
Form.TextArea = (props) => {
  const RichText = useRef();

  const { style, ...rest } = props;
  return (
    <TextAreaInput
      style={{ ...styles.item, ...style }}
      RichText={RichText}
      {...rest}
    />
  );
};

Form.Select = (props) => {
  const { style, ...rest } = props;

  return (
    <SelectField style={{ ...styles.item, ...style }} {...rest} />
  );
};

Form.Date = (props) => {
  const { style, ...rest } = props;

  return (
    <DateInput style={{ ...styles.item, ...style }} {...rest} />
  );
};

Form.Button = (props) => {
  const { style, ...rest } = props;

  return (
    <Button style={{ ...styles.item, ...style }} {...rest} />
  );
};

Form.LocationPicker = (props) => {
  const { value, onChange, ...rest } = props;
  const locationRef = useRef();
  const [API_KEY, setData] = useState(null);

  useEffect(() => {
    if (value === undefined) {
      return;
    }
    locationRef.current?.setAddressText(value);
  }, [value]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await EventsApi.getGoogleAPIKey();
        setData(response.data.data.googleApiKey);
      } catch (error) {
        console.error('Error fetching Google API Key:', error.message);
      }
    };

    fetchData();
  }, []);

  const handlePlaceSelect = (data, details = null) => {
    const { description } = data;
    const { place_id: placeId, url: googleMapsUrl } = details;
    onChange({ placeId, description, googleMapsUrl });
  };

  return (
    <GooglePlacesAutocomplete
      ref={locationRef}
      styles={{
        ...styles.item,
        textInput: {
          borderWidth: 1,
          borderColor: 'transparent',
          borderRadius: 10,
          backgroundColor: colors.grayScale1,
          padding: 13,
          paddingLeft: 20,
          color: colors.inputText,
          fontSize: 16,
        },
      }}
      fetchDetails
      onPress={(data, details = null) => { handlePlaceSelect(data, details); }}
      {...rest}
      textInputProps={{
        placeholderTextColor: colors.inputPlaceholder,
      }}
      query={{
        key: API_KEY,
        language: 'en',
        components: 'country:us',
      }}
      disableScroll
    />
  );
};

Form.ImagePicker = (props) => {
  const { image, onImagePicked, type } = props;
  const [loading, setLoading] = useState(true);

  const pickImage = async () => {
    setLoading(true);

    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: false,
      aspect: [1, 1],
      quality: 1,
    });

    setLoading(false);

    if (!result.canceled) {
      onImagePicked(result.assets[0].uri);
    }
  };

  return (
    <View style={styles.imageContainer}>
      <TouchableOpacity style={styles.imageViewContainer} onPress={() => pickImage()}>
        <Image
          style={styles.image}
          source={image ? { uri: image } : type === 'user' ? defaultProfileImage : defaultClubImage}
          onLoadStart={() => { setLoading(true); }}
          onLoadEnd={() => { setLoading(false); }}
        />
        <View style={styles.imageOverlay} />
        {loading
          ? <ActivityIndicator size="small" color="white" />
          : <Feather name="camera" size={40} color="white" />}
      </TouchableOpacity>
    </View>
  );
};

Form.AttachmentImagePicker = (props) => {
  const {
    image, onImagePicked,
  } = props;
  const [loading, setLoading] = useState(true);

  const pickImage = async () => {
    setLoading(true);

    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: false,
      aspect: [1, 1],
      quality: 1,
    });

    setLoading(false);

    if (!result.canceled) {
      onImagePicked(result.assets[0].uri);
    }
  };

  return (
    <View style={styles.attachmentImageContainer}>
      {!image
        && (
        <Button
          text="Select Image"
          style={styles.attachmentImageButton}
          textStyle={{ color: colors.grayScale7 }}
          onPress={() => pickImage()}
        />
        )}
      {image
        && (
        <View style={{ marginVertical: 30, backgroundColor: 'white' }}>
          <TouchableOpacity
            style={{
              backgroundColor: 'black',
              width: '100%',
              aspectRatio: 1,
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 15,
            }}
            onPress={() => pickImage()}
          >
            <Image
              style={styles.attachmentImage}
              source={{ uri: image }}
              onLoadStart={() => { setLoading(true); }}
              onLoadEnd={() => { setLoading(false); }}
            />
            <View style={styles.editIcon}>
              <View style={styles.editIconInnerCircle}>
                <Feather name="edit-2" size={25} color={colors.grayScale3} />
              </View>
            </View>
            {loading && <ActivityIndicator style={{ position: 'absolute' }} size="small" color="white" />}

          </TouchableOpacity>
        </View>
        )}
    </View>

  );
};
