import React, { Component } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Text, Avatar } from 'native-base';
import PropTypes from 'prop-types';
import colors from '../util/colors';
import defaultProfileImage from '../assets/images/DefaultUserImage.png';

export default class AdminRow extends Component {
  static propTypes = {
    admin: PropTypes.shape({
      name: PropTypes.string,
      year: PropTypes.number,
      major: PropTypes.string,
      profileImage: PropTypes.string,
    }).isRequired,
    handler: PropTypes.func.isRequired,
  };

  static yearToString(year) {
    const date = new Date();
    const currentYear = date.getFullYear();

    // Based on assumption that graduations happen in the spring
    const isSpring = (date.getMonth() < 6) ? 1 : 0;
    switch (Number(year)) {
      case currentYear + 4 - isSpring:
        return '1st Year';
      case currentYear + 3 - isSpring:
        return '2nd Year';
      case currentYear + 2 - isSpring:
        return '3rd Year';
      case currentYear + 1 - isSpring:
        return '4th Year';
      case currentYear:
        return '5th Year';
      default:
        return `Class of ${year}`;
    }
  }

  render() {
    const {
      admin: {
        name, year, major, profileImage,
      },
      handler,
    } = this.props;

    return (
      <TouchableOpacity
        style={{
          width: '100%',
          marginTop: '3%',
        }}
        onPress={handler}
      >
        <View
          style={{
            width: '100%',
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            borderBottomColor: colors.grayScale9,
            borderBottomWidth: 0.3,
            paddingBottom: '2%',
          }}
        >
          <Avatar
            style={{ margin: '2%' }}
            source={profileImage ? { uri: profileImage } : defaultProfileImage}
            backgroundColor="white"
          />
          <View
            style={{
              marginLeft: '3%',
            }}
          >
            <Text>{name}</Text>
            <Text style={{ color: colors.grayScale9, fontSize: 14 }}>
              {AdminRow.yearToString(year)}
              &nbsp;&#183;&nbsp;
              {major}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}
