import {
  AlertDialog, Button, Input, Modal, Text,
} from 'native-base';
import {
  React, useContext, useState, useRef,
} from 'react';
import PropTypes from 'prop-types';
import { UserContext } from '../util/UserContext';
import ClubsAPI from '../api/ClubsApi';

const AddAdminButton = ({ club, setAdmins }) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const onClose = () => setIsOpen(false);
  const [error, setError] = useState('');
  const [value, setValue] = useState('');

  const cancelRef = useRef(null);

  const { user: { _id: userId } } = useContext(UserContext);
  const isAdmin = club?.admins?.filter((admin) => admin._id === userId).length > 0;

  if (!isAdmin) return null;

  const addAdmin = (email) => {
    ClubsAPI.addAdmin(club?._id, email).then((resp) => {
      if (resp.error) {
        if (resp.error === 'Id not found') setError('No user matching that email');
        else setError(resp.error);
        setIsOpen(true);
      } else {
        // console.log(resp);
        setAdmins(resp.admins);
      }
    });
  };

  return (
    <>
      <AlertDialog leastDestructiveRef={cancelRef} isOpen={isOpen} onClose={onClose}>
        <AlertDialog.Content>
          <AlertDialog.CloseButton />
          <AlertDialog.Header>Error</AlertDialog.Header>
          <AlertDialog.Body>
            <Text fontSize="lg">
              {error}
            </Text>
          </AlertDialog.Body>
          <AlertDialog.Footer>
            <Button.Group>
              <Button colorScheme="coolGray" onPress={onClose} ref={cancelRef}>
                Ok
              </Button>
            </Button.Group>
          </AlertDialog.Footer>
        </AlertDialog.Content>
      </AlertDialog>
      <Modal
        isOpen={modalVisible}
        onClose={setModalVisible}
        size="xl"
        avoidKeyboard
      >
        <Modal.Content maxH="212">
          <Modal.CloseButton />
          <Modal.Header>Add an Admin</Modal.Header>
          <Modal.Body>
            <Input
              value={value}
              onChangeText={setValue}
              placeholder="Input user's email address"
              w="100%"
              p="4"
            />
          </Modal.Body>
          <Modal.Footer>
            <Button.Group space={2}>
              <Button
                variant="ghost"
                colorScheme="blueGray"
                onPress={() => {
                  setModalVisible(false);
                  setValue('');
                }}
              >
                Cancel
              </Button>
              <Button
                onPress={() => {
                  setModalVisible(false);
                  addAdmin(value);
                  setValue('');
                }}
              >
                Add Admin
              </Button>
            </Button.Group>
          </Modal.Footer>
        </Modal.Content>
      </Modal>
      <Button size="lg" style={{ backgroundColor: '#206ff5' }} onPress={() => setModalVisible(!modalVisible)}>
        Add Admin
      </Button>
    </>
  );
};

AddAdminButton.propTypes = {
  setAdmins: PropTypes.func.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  club: PropTypes.object.isRequired,
};

export default AddAdminButton;
