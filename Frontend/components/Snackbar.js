import React, { useState, useEffect, useContext } from 'react';
import SnackBar from 'react-native-snackbar-component';
import { UserContext } from '../util/UserContext';

const SNACKBAR_TIME = 2000;

export default function Snackbar() {
  const userContext = useContext(UserContext);
  const { message } = userContext;
  const snackBarTime = SNACKBAR_TIME;

  const [visible, setVisible] = useState(false);

  const snackBarTimer = (time) => {
    if (message !== '') {
      setVisible(true);
      setTimeout(() => setVisible(false), time);
    }
  };

  useEffect(() => {
    snackBarTimer(snackBarTime);
  }, [message]);

  return <SnackBar visible={visible} textMessage={message} />;
}
