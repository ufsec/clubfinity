import React from 'react';
import {
  Text, View, StyleSheet, TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';
import { card } from '../assets/styles/stylesheet';

const style = StyleSheet.create({
  title: {
    fontWeight: '500',
  },

  container: {
    flex: 1,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    alignSelf: 'center',
    maxHeight: 65,
    margin: 0,
  },

  mainSection: {
    flex: 4,
  },

  subSection: {
    flex: 3,
    alignContent: 'flex-end',
  },
});

const AgendaCard = (props) => {
  const {
    onPress, clubName, eventName, location, date,
  } = props;
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={[card.container, style.container]}>
        <View style={style.mainSection}>
          <Text style={style.title}>{eventName}</Text>
          <Text style={card.bodyText}>{clubName}</Text>
        </View>
        <View style={style.subSection}>
          <Text style={card.agendaText}>{date.toLocaleString()}</Text>
          <Text style={card.agendaText} numberOfLines={1}>{location.description}</Text>
          {location.room && (
          <Text style={card.agendaText}>
            Room:
            {' '}
            {location.room}
          </Text>
          )}
        </View>
      </View>
    </TouchableOpacity>
  );
};

AgendaCard.propTypes = {
  clubName: PropTypes.string.isRequired,
  eventName: PropTypes.string.isRequired,
  location: PropTypes.shape({
    placeId: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    room: PropTypes.string,
  }).isRequired,
  date: PropTypes.instanceOf(Date).isRequired,
  onPress: PropTypes.func.isRequired,
};

export default AgendaCard;
