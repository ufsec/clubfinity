import {
  Modal as RNModal, Platform, View, KeyboardAvoidingView,
} from 'react-native';
import React from 'react';
import PropTypes from 'prop-types';

const Modal = ({ isOpen, withInput = false, children }) => {
  const content = withInput ? (
    <View
      style={{
        flex: 1,
        paddingHorizontal: '8%',
        backgroundColor: 'rgba(0,0,0,0.4)',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      >
        {children}
      </KeyboardAvoidingView>
    </View>
  ) : (
    <View
      style={{
        flex: 1,
        paddingHorizontal: '8%',
        backgroundColor: 'rgba(0,0,0,0.4)',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      {children}
    </View>
  );

  return (
    <RNModal
      visible={isOpen}
      transparent
      animationType="fade"
      statusBarTranslucent
    >
      {content}
    </RNModal>
  );
};

Modal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  withInput: PropTypes.bool,
  children: PropTypes.node.isRequired,
};

export default Modal;
