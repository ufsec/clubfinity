import * as SecureStore from 'expo-secure-store';
import API from './BaseApi';

exports.getAllSocials = async () => {
  const bearerToken = await SecureStore.getItemAsync('userToken');
  const resp = await API.get('/api/socials', {
    headers: {
      Authorization: `Bearer ${bearerToken}`,
    },
  });
  return resp.data.data;
};
