import * as SecureStore from 'expo-secure-store';
import API from './BaseApi';

exports.getAlerts = async () => {
  const bearerToken = await SecureStore.getItemAsync('userToken');
  const resp = await API.get('/api/alerts', {
    headers: {
      Authorization: `Bearer ${bearerToken}`,
    },
  });
  return resp.data.data;
};

exports.getAlert = async (alertId) => {
  const bearerToken = await SecureStore.getItemAsync('userToken');
  const resp = await API.get(`/api/alerts/${alertId}`, {
    headers: {
      Authorization: `Bearer ${bearerToken}`,
    },
  });
  return resp.data.data;
};

module.exports = exports;
