import * as SecureStore from 'expo-secure-store';
import API from './BaseApi';

exports.submitBugReport = async (bugData) => {
  const bearerToken = await SecureStore.getItemAsync('userToken');
  try {
    const axiosResponse = await API.post('/api/tickets', {
      type: 'bug',
      bugData,
    },
    {
      headers: {
        Authorization: `Bearer ${bearerToken}`,
      },
    })
      .then(async (response) => (response.data.ok))
      .catch((error) => {
        if (error.response) {
          return { error: error.response.data.error };
        }
        return { error: 'Unable to create new ticket' };
      });
    return axiosResponse;
  } catch (error) {
    console.error(error);
    return false;
  }
};

exports.submitClub = async (
  clubName,
  clubCategory,
  clubDescription,
  tags,
  facebookLink,
  instagramLink,
  slackLink,
  thumbnailUrl,
  googleCalLink,
  isPrivate,
) => {
  const bearerToken = await SecureStore.getItemAsync('userToken');
  const clubData = {
    name: clubName,
    category: clubCategory,
    description: clubDescription,
    isPrivate,
    tags,
  };
  if (thumbnailUrl) {
    clubData.thumbnailUrl = thumbnailUrl;
  }
  if (facebookLink) {
    clubData.facebookLink = facebookLink;
  }
  if (instagramLink) {
    clubData.instagramLink = instagramLink;
  }
  if (slackLink) {
    clubData.slackLink = slackLink;
  }
  if (googleCalLink) {
    clubData.googleCalLink = googleCalLink;
  }
  try {
    const resp = await API.post('/api/tickets', {
      type: 'club-submission',
      clubData,
    },
    {
      headers: {
        Authorization: `Bearer ${bearerToken}`,
      },
    })
      .then(async (response) => (response.data.ok))
      .catch((error) => {
        if (error.response) {
          return { error: error.response.data.error };
        }
        return { error: 'Unable to create new ticket' };
      });
    return resp;
  } catch (error) {
    console.error(error);
    return false;
  }
};
