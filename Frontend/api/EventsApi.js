import * as SecureStore from 'expo-secure-store';
import API from './BaseApi';
import transformDate from '../util/transform';

// return all events
exports.getAllEvents = async () => {
  const bearerToken = await SecureStore.getItemAsync('userToken');
  const resp = await API.get('/api/events?filterBy=all', {
    headers: {
      Authorization: `Bearer ${bearerToken}`,
    },
  });
  return transformDate(resp.data.data);
};

// return event from id
exports.getEvent = async (eventId) => {
  const bearerToken = await SecureStore.getItemAsync('userToken');
  const res = await API.get(`/api/events/${eventId}`, {
    headers: {
      Authorization: `Bearer ${bearerToken}`,
    },
  })
    .then(async (response) => response)
    .catch((error) => {
      if (error) {
        return error;
      }
      return { error: 'Unable to get event' };
    });
  return res.data.data;
};

exports.getFollowing = async (page, pageSize, eventType) => {
  const bearerToken = await SecureStore.getItemAsync('userToken');
  const resp = await API.get(
    `/api/events?filterBy=userId&page=${page}&pageSize=${pageSize}&eventType=${eventType}`,
    {
      headers: {
        Authorization: `Bearer ${bearerToken}`,
      },
    },
  );

  // Return object containing events with updated dates and any extra meta data
  return {
    ...resp.data.data,
    events: transformDate(resp.data.data.events),
  };
};

exports.getInMonth = async (date) => {
  const bearerToken = await SecureStore.getItemAsync('userToken');
  const resp = await API.get(
    `/api/events?filterBy=month&date=${date.toISODate()}&filter=following`,
    {
      headers: {
        Authorization: `Bearer ${bearerToken}`,
      },
    },
  );

  return transformDate(resp.data.data);
};

exports.getForClub = async (clubId, page, pageSize, eventType) => {
  const bearerToken = await SecureStore.getItemAsync('userToken');
  const resp = await API.get(
    `/api/events?filterBy=club&clubId=${clubId}&page=${page}&pageSize=${pageSize}&eventType=${eventType}`,
    {
      headers: {
        Authorization: `Bearer ${bearerToken}`,
      },
    },
  );

  return {
    ...resp.data.data,
    events: transformDate(resp.data.data.events),
  };
};

exports.create = async (eventData) => {
  const bearerToken = await SecureStore.getItemAsync('userToken');
  const axiosResponse = await API.post('/api/events', eventData, {
    headers: {
      Authorization: `Bearer ${bearerToken}`,
    },
  }).then(async (response) => response)
    .catch((error) => ({
      error: error.response?.data.error || 'Unable to create event',
    }));
  return axiosResponse;
};

exports.update = async (eventId, event) => {
  const bearerToken = await SecureStore.getItemAsync('userToken');
  const axiosResponse = await API.put(`/api/events/${eventId}`, event, {
    headers: {
      Authorization: `Bearer ${bearerToken}`,
    },
  })
    .then(async (response) => response)
    .catch((error) => {
      if (error) {
        return error;
      }
      return { error: 'Unable to update event' };
    });
  return axiosResponse;
};

exports.updateUsersList = async (eventId, userListType, op) => {
  const bearerToken = await SecureStore.getItemAsync('userToken');
  const axiosResponse = await API.patch(
    `/api/events/${eventId}/${userListType}?op=${op}`, {},
    {
      headers: {
        Authorization: `Bearer ${bearerToken}`,
      },
    },
  );
  return axiosResponse;
};

exports.addGoingUser = async (eventId) => exports.updateUsersList(eventId, 'going-users', 'add');

exports.removeGoingUser = async (eventId) => exports.updateUsersList(eventId, 'going-users', 'remove');

exports.addInterestedUser = async (eventId) => exports.updateUsersList(eventId, 'interested-users', 'add');

exports.removeInterestedUser = async (eventId) => exports.updateUsersList(eventId, 'interested-users', 'remove');

exports.addUninterestedUser = async (eventId) => exports.updateUsersList(eventId, 'uninterested-users', 'add');

exports.removeUninterestedUser = async (eventId) => exports.updateUsersList(eventId, 'uninterested-users', 'remove');

exports.delete = async (eventId) => {
  const bearerToken = await SecureStore.getItemAsync('userToken');

  const axiosResponse = await API.delete(`/api/events/${eventId}`, {
    headers: {
      Authorization: `Bearer ${bearerToken}`,
    },
  }).catch(
    (error) => error || { error: 'Internal server error. Unable to delete event' },
  );

  return axiosResponse;
};

exports.search = async (name) => {
  const bearerToken = await SecureStore.getItemAsync('userToken');
  const resp = await API.get(
    `/api/events?filterBy=search&input=${name}`, {
      headers: {
        Authorization: `Bearer ${bearerToken}`,
      },
    },
  );
  return transformDate(resp.data.data);
};

exports.getGoogleAPIKey = async () => {
  const bearerToken = await SecureStore.getItemAsync('userToken');
  const resp = await API.get(
    '/api/events/retrieve-Google-API-Key', {
      headers: {
        Authorization: `Bearer ${bearerToken}`,
      },
    },
  );
  return resp;
};
