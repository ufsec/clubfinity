import * as SecureStore from 'expo-secure-store';
import API from './BaseApi';
import transformDate from '../util/transform';

exports.create = async (params) => {
  const bearerToken = await SecureStore.getItemAsync('userToken');
  const axiosResponse = await API.post('/api/announcements', params, {
    headers: {
      Authorization: `Bearer ${bearerToken}`,
    },
  }).then(async (response) => response)
    .catch((error) => ({
      error: error.response?.data.error || 'Unable to create announcement',
    }));
  return axiosResponse;
};

exports.getForClub = async (clubId, page, pageSize) => {
  const bearerToken = await SecureStore.getItemAsync('userToken');
  const resp = await API.get(
    `/api/announcements?filterBy=club&clubId=${clubId}&page=${page}&pageSize=${pageSize}`,
    {
      headers: {
        Authorization: `Bearer ${bearerToken}`,
      },
    },
  );

  // Return object containing announcements with updated dates and any extra meta data
  return { ...resp.data.data, announcements: transformDate(resp.data.data.announcements) };
};

exports.update = async (announcementId, params) => {
  const bearerToken = await SecureStore.getItemAsync('userToken');

  const resp = await API.put(`/api/announcements/${announcementId}`, params, {
    headers: {
      Authorization: `Bearer ${bearerToken}`,
    },
  })
    .then(async (response) => response)
    .catch((error) => {
      if (error) {
        return { error };
      }
      return { error: 'Unable to update announcement' };
    });
  return resp;
};

exports.delete = async (announcementId) => {
  const bearerToken = await SecureStore.getItemAsync('userToken');

  return API.delete(`/api/announcements/${announcementId}`, {
    headers: {
      Authorization: `Bearer ${bearerToken}`,
    },
  }).catch((error) => error || { error: 'Unable to delete announcement' });
};
