require('dotenv').config();

export default {
  name: 'Clubfinity',
  description: 'Platform for Gator Club events.',
  slug: 'Clubfinity',
  privacy: 'public',
  platforms: [
    'ios',
    'android',
  ],
  version: '1.0.36',
  orientation: 'portrait',
  icon: './assets/icon.png',
  splash: {
    image: './assets/splash.png',
    resizeMode: 'contain',
    backgroundColor: '#ffffff',
  },
  updates: {
    fallbackToCacheTimeout: 0,
  },
  assetBundlePatterns: [
    '**/*',
  ],
  android: {
    package: 'com.ufsec.Clubfinity',
    googleServicesFile: './google-services.json',
    useNextNotificationsApi: true,
    permissions: [],
    versionCode: 22,
  },
  ios: {
    supportsTablet: false,
    bundleIdentifier: 'com.ufsec.clubfinity',
  },
  // Expo Linking (for qr codes) requires a
  // build-time setting `scheme` for production
  // apps. If it's left blank, prod may crash.
  scheme: 'clubfinity-deep-linking',
  plugins: [
    'expo-asset',
    'expo-font',
    'expo-secure-store',
    'expo-video',
  ],
};
