import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import PropTypes from 'prop-types';
import generateStack from './stackHelper';
import colors from '../../util/colors';

const Stack = createNativeStackNavigator();

const ClubfinityStack = ({ name }) => (
  <Stack.Navigator screenOptions={{
    headerTintColor: 'white',
    headerTitleStyle: { color: 'white' },
    headerStyle: { backgroundColor: colors.primary0 },
  }}
  >
    {generateStack(Stack, name)}
  </Stack.Navigator>
);

ClubfinityStack.propTypes = {
  name: PropTypes.string.isRequired,
};

export default ClubfinityStack;
