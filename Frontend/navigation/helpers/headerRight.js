import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import ShareIconComponent from '../../components/ShareIconComponent';

const ICON_SIZE = 30;
const ICON_COLOR = 'white';

export const getHeaderRight = (pageName, navigation, openBottomSheet) => {
  if (pageName === 'Profile') {
    return {
      headerRight: () => <SettingsIconComponent navigation={navigation} />,
    };
  }
  if (pageName === 'ClubScr') {
    return {
      headerRight: () => <ShareIconComponent onPress={openBottomSheet} />,
    };
  }
  return {};
};

const SettingsIconComponent = ({ navigation }) => (
  <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
    <TouchableOpacity
      onPress={() => navigation.navigate('Settings')}
      style={{
        paddingTop: '2%',
        paddingRight: '2%',
        alignSelf: 'flex-end',
      }}
    >
      <Ionicons name="settings" size={ICON_SIZE} color={ICON_COLOR} />
    </TouchableOpacity>
  </View>
);

export default getHeaderRight;
