import React, { useEffect, useContext, useRef } from 'react';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Ionicons } from '@expo/vector-icons';

import * as Linking from 'expo-linking';
import SignupScr from '../screens/SignupScr';
import SigninScr from '../screens/SigninScr';
import ForgotPasswordScr from '../screens/ForgotPasswordScr';
import ForgotPasswordVerification from '../screens/ForgotPasswordVerification';
import EmailVerificationScr from '../screens/EmailVerificationScr';
import ClubfinityStack from './helpers/ClubfinityStack';
import { navigationRef } from './helpers/resetNavigationHelper';
import AuthScr from '../screens/AuthScr';
import { DeepLinkingContext } from '../util/DeepLinkingContext';
import { UserContext } from '../util/UserContext';

const Stack = createNativeStackNavigator();
const AuthNav = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const getOptions = (barLabel, iconName) => ({
  tabBarIcon: ({ color, size }) => (
    <Ionicons name={iconName} size={size} color={color} />
  ),
  tabBarLabel: barLabel,
});

const getTabScreenForStack = (stackName, iconName) => (
  <Tab.Screen
    options={getOptions(stackName, iconName)}
    name={`${stackName}Stack`}
    children={() => <ClubfinityStack name={stackName} />}
  />
);

const AuthStack = () => (
  <AuthNav.Navigator
    initialRouteName="SignIn"
    screenOptions={{
      headerShown: false,
    }}
  >
    <AuthNav.Screen name="SignIn" component={SigninScr} />
    <AuthNav.Screen name="SignUp" component={SignupScr} />
    <AuthNav.Screen name="ForgotPassword" component={ForgotPasswordScr} />
    <AuthNav.Screen
      name="ForgotPasswordVerification"
      component={ForgotPasswordVerification}
    />
    <AuthNav.Screen name="EmailVerification" component={EmailVerificationScr} />
  </AuthNav.Navigator>
);

const AppStack = () => (
  <Tab.Navigator
    initialRouteName="HomeStack"
    screenOptions={{
      headerShown: false,
    }}
  >
    {getTabScreenForStack('Home', 'home')}
    {getTabScreenForStack('Discover', 'search')}
    {getTabScreenForStack('Calendar', 'calendar')}
    {getTabScreenForStack('Profile', 'happy')}
  </Tab.Navigator>
);

export default function AppNavigator() {
  const MyTheme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      background: 'rgb(255, 255, 255)',
    },
  };

  // Event listeners that detect expo url events.
  // The recommended useUrl hook only recognizes changes
  // to the original url, so scanning the same qr code
  // twice would not trigger an event. An event listener
  // works better in this regard.
  const urlEventListener = useRef(null);
  const { user } = useContext(UserContext);
  const { setCurrentUrl, navigateToNewLinkingUrl } = useContext(DeepLinkingContext);

  // If user is signed in, listener should navigate to the current url.
  // If user signs out, do not navigate but set the url variable for later.
  useEffect(() => {
    if (urlEventListener.current != null) { urlEventListener.current.remove(); }

    if (user != null) {
      urlEventListener.current = Linking.addEventListener('url', (newUrl) => {
        navigateToNewLinkingUrl(navigationRef, newUrl.url);
      });
    } else {
      urlEventListener.current = Linking.addEventListener('url', (newUrl) => {
        setCurrentUrl(newUrl.url);
      });
    }
  }, [user]);

  return (
    <NavigationContainer theme={MyTheme} ref={navigationRef}>
      <Stack.Navigator
        initialRouteName="LoadState"
        screenOptions={{
          headerShown: false,
        }}
      >
        <Stack.Screen name="LoadState" component={AuthScr} />
        <Stack.Screen name="AppStack" component={AppStack} />
        <Stack.Screen name="AuthStack" component={AuthStack} options={{ animation: 'fade' }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
